setwd("/media/mauricio/TeraHDD2/Dropbox/Learning Lab/XageT")
setwd("C:/Users/Mauricio/Dropbox/Learning Lab/XageT/")
setwd("D:/Dropbox/Learning Lab/XageT/")


library(sp)
library(maptools)
library(rgdal)
library(dismo)
library(raster)  # grids, rasters
library(rasterVis)  # raster visualisation
library(rgeos)
library(RgoogleMaps)
library(choroplethr)
library(choroplethrMaps)
library(ggplot2)
library(mapproj)
library(gridExtra)
library(GISTools)
library(foreign)
library(maps)
library(ggmap)




AcademyList=read.csv("RawData/FinalRandomizationYr2.csv",stringsAsFactors=F)
AcademyList=AcademyList[,c("academyid","Treatment")]

AcademyList$academyid[AcademyList$academyid=="Mazeras-MBA"]="Mazeras-KLF"
AcademyCoords=read.csv("RawData/ListAcademies_2016.csv")
AcademyCoords=AcademyCoords[,c("Academy_Name","Latitude","Longitude1")] 
colnames(AcademyCoords)=c("academyid","lat","long")
AcademyCoords=AcademyCoords[complete.cases(AcademyCoords),]
AcademyList=merge(AcademyList,AcademyCoords, all.x=T, all.y=F)



AcademySP = SpatialPoints(AcademyList[,c("long","lat")],proj4string=CRS("+proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"))
AcademySP = SpatialPointsDataFrame(AcademySP,AcademyList)

Kenya0=readRDS("RawData/KEN_adm0.rds")
Kenya=readRDS("RawData/KEN_adm1.rds")
KenyaProj = spTransform(Kenya, CRS("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs"))
Kenya0Proj = spTransform(Kenya0, CRS("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs"))
AcademySPProj = spTransform(AcademySP, CRS("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs")) 

pdf("ResultsYr2/Graphs/MapRCT_Yr2.pdf")
par(mar=c(1,1,1,1))
plot(KenyaProj)
#gbmap <- gmap(KenyaProj, type = "hybrid",scale=2)
#plot(gbmap,add=T)
plot(Kenya0Proj,add=T,lwd=1)
plot(KenyaProj,add=T,lwd=1)
plot(AcademySPProj,add=T,cex=0.8,col=2*(AcademySPProj$Treatment+1),pch=0+2*(AcademySPProj$Treatment+1))
map.scale(extent(KenyaProj)@xmin+200000,extent(KenyaProj)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='black',sfcol='red')          
#north.arrow(extent(KenyaProj)@xmin+80000,extent(KenyaProj)@ymin+170000 ,len=1000*30, lab="N")
legend("topright",c("English","Math"),pch=c(2,4),col=c(2,4),bg="white")
#text(KenyaProj, KenyaProj$NAME_1,cex=0.7,col="darkblue")
dev.off()

pdf("ResultsYr2/Graphs/MapRCT_Yr2_bground.pdf")
par(mar=c(1,1,1,1))
plot(extent(KenyaProj))
gbmap <- gmap(KenyaProj, type = "roadmap",scale=2)
plot(gbmap,add=T)
#plot(Kenya0Proj,add=T,lwd=2)
#plot(KenyaProj,add=T,lwd=2)
plot(AcademySPProj,add=T,cex=1,col=2*(AcademySPProj$Treatment+1),pch=0+2*(AcademySPProj$Treatment+1))
map.scale(extent(KenyaProj)@xmin+200000,extent(KenyaProj)@ymin+50000,len=1000*400,units="KM",ndivs=4,subdiv=100,tcol='black',sfcol='red')          
#north.arrow(extent(KenyaProj)@xmin+80000,extent(KenyaProj)@ymin+170000 ,len=1000*30, lab="N")
legend("topright",c("English","Math"),pch=c(2,4),col=c(2,4),bg="white")
dev.off()

# gbmap <- gmap(KenyaProj, type = "roadmap",exp=0.8,zoom=6,rgb=T)
# gbmap_proj=projectRaster(gbmap,crs=CRS("+proj=utm +zone=37 +ellps=WGS84 +datum=WGS84 +units=m +no_defs +towgs84=0,0,0"))
# KenyaProj = spTransform(Kenya, CRS("+proj=utm +zone=37 +ellps=WGS84 +datum=WGS84 +units=m +no_defs +towgs84=0,0,0")) 
# AcademySPProj = spTransform(AcademySP, CRS("+proj=utm +zone=37 +ellps=WGS84 +datum=WGS84 +units=m +no_defs +towgs84=0,0,0")) 
# plotRGB(gbmap_proj)
# plot(KenyaProj,add=T)
# plot(AcademySPProj,add=T,pch=19,cex=0.5,col=2*(AcademySPProj$treat+1))
# map.scale(extent(KenyaProj)@xmin+200000,extent(KenyaProj)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='black',sfcol='red')          
# #north.arrow(extent(KenyaProj)@xmin+80000,extent(KenyaProj)@ymin+170000 ,len=1000*30, lab="N")
