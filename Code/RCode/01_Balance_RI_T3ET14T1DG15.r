rm(list=ls())
#install.packages(c("foreign","readstata13","permute","sampling","dplyr","plotrix"))

library(foreign)
library(readstata13)
library(permute)
library(sampling)
library(dplyr)
library(plotrix)


##First a function to calculate whether a NULL hypothesis is true or not..the idea is that under the null the distribution is "distribution+null_HO"
##If the statistic is on either the top tail or the bottom tail of the distribution, we reject H_0
##A value of 1 indicates we reject the NULL!!! BE CAREFUL ABOUT THIS!
##Other important things, level can be a vector!! Yai...
HypothesisTest=function(statistic,distribution,null_HO,level){
  TreatmentsUnderNull=distribution+null_HO
  ColasDist=matrix(quantile(TreatmentsUnderNull,probs=c(level/2,1-level/2),names=F),ncol=2)
  TrueFalse=(statistic<ColasDist[,1] | statistic>ColasDist[,2])
  return(TrueFalse)
}

SequencePValues=seq(0,1,0.001)
##Now we estimate the p_value of the statistic by finding the minimum level at which we reject the null hypothesis
Funpvalue=function(statistic,distribution,null_HO){
return(SequencePValues[min(which(HypothesisTest(statistic=statistic,distribution=distribution,null_HO=null_HO,SequencePValues)==1))])
}

##Finally, this function computes the confidence interval..how? well it tries different H_0 and keeps those for which the p-value is greater than 0.5
ConfidenceInterval=function(statistic,distribution){
LookFor=sort(c(seq(from=statistic,by=0.01,length.out=300),seq(from=statistic,by=-0.01,length.out=300)))
VectoPvalues=sapply(LookFor,Funpvalue,statistic=statistic,distribution=distribution)
return(range(LookFor[which(VectoPvalues>CI_Level)]))
}


#setwd("/media/mauricio/TeraHDD/Dropbox/Learning Lab/XageT")
setwd("C:/Users/Mauricio/Dropbox/Learning Lab/XageT")

DataCompleta=read.dta13("CreatedData/2015t1_FullyLinked_mainTE_own.dta")
AcademyList=read.csv("RawData/TreatmentStatus.csv")
AcademyList=AcademyList[,c("academyid","province","treat")]
AcademyList$academyid=as.character(AcademyList$academyid)
AcademyList$province=as.character(AcademyList$province)
set.seed(30081986)
num_reps=10000

CI_Level=0.05
CI_interval=c(CI_Level/2,1-CI_Level/2)
endline_vector=c("T1MT15","T1ET15","T2MT15","T2ET15")

if(sum(CI_interval)!=1) stop("You messed up buddy... CI doesn't add up to 1!")


Original=data.frame(do.call(rbind,by(AcademyList, AcademyList$province, sample_frac)),row.names=NULL)


TreatmentsPerm=replicate(num_reps,do.call(c, by(data.frame(AcademyList$treat), AcademyList$province, sample_frac)))
row.names(TreatmentsPerm)=NULL

controls=c("mthly_fees_c1","c.wagecat_D2","c.wagecat_D3")

controlspu_T3ET14=c("env_T3ET14_Z","math_T3ET14_Z","env_T3ET14_Z2","math_T3ET14_Z2","env_T3ET14_Z3","math_T3ET14_Z3")
controlsc1_T3ET14=c("read_T3ET14_Z","env_T3ET14_Z","math_T3ET14_Z","kusoma_T3ET14_Z","read_T3ET14_Z2","env_T3ET14_Z2","math_T3ET14_Z2","kusoma_T3ET14_Z2","read_T3ET14_Z3","env_T3ET14_Z3","math_T3ET14_Z3","kusoma_T3ET14_Z3")
controlsc5_T3ET14=c("lang_T3ET14_Z","math_T3ET14_Z","sci_T3ET14_Z","re_T3ET14_Z","ss_T3ET14_Z","swahili_T3ET14_Z","lang_T3ET14_Z2","math_T3ET14_Z2","sci_T3ET14_Z2","re_T3ET14_Z2","ss_T3ET14_Z2","swahili_T3ET14_Z2","lang_T3ET14_Z3","math_T3ET14_Z3","sci_T3ET14_Z3","re_T3ET14_Z3","ss_T3ET14_Z3","swahili_T3ET14_Z3")
controlsc6_T3ET14=c("lang_T3ET14_Z","math_T3ET14_Z","sci_T3ET14_Z","re_T3ET14_Z","ss_T3ET14_Z","swahili_T3ET14_Z","lang_T3ET14_Z2","math_T3ET14_Z2","sci_T3ET14_Z2","re_T3ET14_Z2","ss_T3ET14_Z2","swahili_T3ET14_Z2","lang_T3ET14_Z3","math_T3ET14_Z3","sci_T3ET14_Z3","re_T3ET14_Z3","ss_T3ET14_Z3","swahili_T3ET14_Z3")

controlspu_ET14DG15=c("env_T3ET14_Z","math_T3ET14_Z","env_T3ET14_Z2","math_T3ET14_Z2","env_T3ET14_Z3","math_T3ET14_Z3")
controlsc1_ET14DG15=c("sci_T1DG15_Z","ss_T1DG15_Z","eng_T1DG15_Z","sci_T1DG15_Z2","ss_T1DG15_Z2","eng_T1DG15_Z2","sci_T1DG15_Z3","ss_T1DG15_Z3","eng_T1DG15_Z3","read_T3ET14_Z","env_T3ET14_Z","math_T3ET14_Z","kusoma_T3ET14_Z","read_T3ET14_Z2","env_T3ET14_Z2","math_T3ET14_Z2","kusoma_T3ET14_Z2","read_T3ET14_Z3","env_T3ET14_Z3","math_T3ET14_Z3","kusoma_T3ET14_Z3")
controlsc5_ET14DG15=c("math_T1DG15_Z","sci_T1DG15_Z","ss_T1DG15_Z","eng_T1DG15_Z","swahili_T1DG15_Z","math_T1DG15_Z2","sci_T1DG15_Z2","ss_T1DG15_Z2","eng_T1DG15_Z2","swahili_T1DG15_Z2","math_T1DG15_Z3","sci_T1DG15_Z3","ss_T1DG15_Z3","eng_T1DG15_Z3","swahili_T1DG15_Z3","lang_T3ET14_Z","math_T3ET14_Z","sci_T3ET14_Z","re_T3ET14_Z","ss_T3ET14_Z","swahili_T3ET14_Z","lang_T3ET14_Z2","math_T3ET14_Z2","sci_T3ET14_Z2","re_T3ET14_Z2","ss_T3ET14_Z2","swahili_T3ET14_Z2","lang_T3ET14_Z3","math_T3ET14_Z3","sci_T3ET14_Z3","re_T3ET14_Z3","ss_T3ET14_Z3","swahili_T3ET14_Z3")
controlsc6_ET14DG15=c("math_T1DG15_Z","sci_T1DG15_Z","ss_T1DG15_Z","eng_T1DG15_Z","swahili_T1DG15_Z","math_T1DG15_Z2","sci_T1DG15_Z2","ss_T1DG15_Z2","eng_T1DG15_Z2","swahili_T1DG15_Z2","math_T1DG15_Z3","sci_T1DG15_Z3","ss_T1DG15_Z3","eng_T1DG15_Z3","swahili_T1DG15_Z3","lang_T3ET14_Z","math_T3ET14_Z","sci_T3ET14_Z","re_T3ET14_Z","ss_T3ET14_Z","swahili_T3ET14_Z","lang_T3ET14_Z2","math_T3ET14_Z2","sci_T3ET14_Z2","re_T3ET14_Z2","ss_T3ET14_Z2","swahili_T3ET14_Z2","lang_T3ET14_Z3","math_T3ET14_Z3","sci_T3ET14_Z3","re_T3ET14_Z3","ss_T3ET14_Z3","swahili_T3ET14_Z3")

controlspu_T1DG15=c("")
controlsc1_T1DG15=c("sci_T1DG15_Z","ss_T1DG15_Z","eng_T1DG15_Z","sci_T1DG15_Z2","ss_T1DG15_Z2","eng_T1DG15_Z2","sci_T1DG15_Z3","ss_T1DG15_Z3","eng_T1DG15_Z3")
controlsc5_T1DG15=c("math_T1DG15_Z","sci_T1DG15_Z","ss_T1DG15_Z","eng_T1DG15_Z","swahili_T1DG15_Z","math_T1DG15_Z2","sci_T1DG15_Z2","ss_T1DG15_Z2","eng_T1DG15_Z2","swahili_T1DG15_Z2","math_T1DG15_Z3","sci_T1DG15_Z3","ss_T1DG15_Z3","eng_T1DG15_Z3","swahili_T1DG15_Z3")
controlsc6_T1DG15=c("math_T1DG15_Z","sci_T1DG15_Z","ss_T1DG15_Z","eng_T1DG15_Z","swahili_T1DG15_Z","math_T1DG15_Z2","sci_T1DG15_Z2","ss_T1DG15_Z2","eng_T1DG15_Z2","swahili_T1DG15_Z2","math_T1DG15_Z3","sci_T1DG15_Z3","ss_T1DG15_Z3","eng_T1DG15_Z3","swahili_T1DG15_Z3")

controlspu_T1MT15=c("read_T1MT15_Z","kusoma_T1MT15_Z","env_T1MT15_Z","math_T1MT15_Z","read_T1MT15_Z2","kusoma_T1MT15_Z2","env_T1MT15_Z2","math_T1MT15_Z2","read_T1MT15_Z3","kusoma_T1MT15_Z3","env_T1MT15_Z3","math_T1MT15_Z3")
controlsc1_T1MT15=c("math_T1MT15_Z","read_T1MT15_Z","kusoma_T1MT15_Z","re_T1MT15_Z","sci_T1MT15_Z","ss_T1MT15_Z","math_T1MT15_Z2","read_T1MT15_Z2","kusoma_T1MT15_Z2","re_T1MT15_Z2","sci_T1MT15_Z2","ss_T1MT15_Z2","math_T1MT15_Z3","read_T1MT15_Z3","kusoma_T1MT15_Z3","re_T1MT15_Z3","sci_T1MT15_Z3","ss_T1MT15_Z3")
controlsc5_T1MT15=c("lang_T1MT15_Z","math_T1MT15_Z","sci_T1MT15_Z","re_T1MT15_Z","ss_T1MT15_Z","swahili_T1MT15_Z","lang_T1MT15_Z2","math_T1MT15_Z2","sci_T1MT15_Z2","re_T1MT15_Z2","ss_T1MT15_Z2","swahili_T1MT15_Z2","lang_T1MT15_Z3","math_T1MT15_Z3","sci_T1MT15_Z3","re_T1MT15_Z3","ss_T1MT15_Z3","swahili_T1MT15_Z3")
controlsc6_T1MT15=c("lang_T1MT15_Z","math_T1MT15_Z","sci_T1MT15_Z","re_T1MT15_Z","ss_T1MT15_Z","swahili_T1MT15_Z","lang_T1MT15_Z2","math_T1MT15_Z2","sci_T1MT15_Z2","re_T1MT15_Z2","ss_T1MT15_Z2","swahili_T1MT15_Z2","lang_T1MT15_Z3","math_T1MT15_Z3","sci_T1MT15_Z3","re_T1MT15_Z3","ss_T1MT15_Z3","swahili_T1MT15_Z3")




DataCompleta$dSTD1=(DataCompleta$gradename=="STD 1")
DataCompleta$dPU=(DataCompleta$gradename=="PREUNIT")
DataCompleta$dSTD5=(DataCompleta$gradename=="STD 5")
DataCompleta$dSTD6=(DataCompleta$gradename=="STD 6")

DataCompleta$c1=(DataCompleta$gradename=="STD 1")
DataCompleta$pu=(DataCompleta$gradename=="PREUNIT")
DataCompleta$c5=(DataCompleta$gradename=="STD 5")
DataCompleta$c6=(DataCompleta$gradename=="STD 6")

colnames(DataCompleta)=sub("percscore_","",colnames(DataCompleta))


for(baseline in c("ET14DG15")){
	for(var in get(paste0("controlspu_",baseline))){
		if(var!=""){
		  DataCompleta[,paste0("zo_",var,"_",baseline,"_pu")]=DataCompleta[,var]
		  DataCompleta[which(is.na(DataCompleta[,paste0("zo_",var,"_",baseline,"_pu")]) & DataCompleta$dSTD1==1),paste0("zo_",var,"_",baseline,"_pu")]=0
		}
	}
}

for(baseline in c("ET14DG15")){
	for(var in get(paste0("controlsc1_",baseline))){
		if(var!=""){
		  DataCompleta[,paste0("zo_",var,"_",baseline,"_c1")]=DataCompleta[,var]
		  DataCompleta[which(is.na(DataCompleta[,paste0("zo_",var,"_",baseline,"_c1")]) & DataCompleta$dPU==1),paste0("zo_",var,"_",baseline,"_c1")]=0
		}
	}
}



controlspu_ET14DG15_z0=colnames(DataCompleta)[which(grepl("zo_",colnames(DataCompleta))*grepl("_pu",colnames(DataCompleta))==1)]
controlsc1_ET14DG15_z0=colnames(DataCompleta)[which(grepl("zo_",colnames(DataCompleta))*grepl("_c1",colnames(DataCompleta))==1)]

BaseLineVars=unique(c(controlspu_ET14DG15_z0,controlsc1_ET14DG15_z0,controlspu_ET14DG15,controlsc1_ET14DG15,controlsc5_ET14DG15,controlsc6_ET14DG15))
EndLineVars=colnames(DataCompleta)[c(which(grepl("T1MT15_Z",colnames(DataCompleta))),which(grepl("T1ET15_Z",colnames(DataCompleta))),which(grepl("T2MT15_Z",colnames(DataCompleta))),which(grepl("T2ET15_Z",colnames(DataCompleta))))]





ResultsPermsTreat=list()
for(i in 1:num_reps){
   TratmentStatus=data.frame(TreatmentsPerm[,i],Original[,1])
   TratmentStatus[,1]=as.numeric(as.character(TratmentStatus[,1]))
   TratmentStatus[,2]=as.character(TratmentStatus[,2])
   colnames(TratmentStatus)=c("treat_perm","academyid")
   DataCompletaPerm=merge(DataCompleta[,c("academyid","pu","c1","c5","c6",EndLineVars,BaseLineVars)],TratmentStatus)
   
   ##Primero recorre todos los endlines
      for(endline in endline_vector){
      
      ##Ahora para  PU
	for(subject in c("math","read","kusoma","env")){	
	  reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat_perm+",paste0(controlspu_ET14DG15,collapse="+"))),data=DataCompletaPerm,subset=DataCompletaPerm[,"pu"]==1)
	  ResultsPermsTreat[[paste0("PU_",subject,"_",endline)]][i]=reg$coef["treat_perm"]
	}
	
	##Ahora para primero
	for(subject in c("math","read","kusoma","sci","ss","re")){	
	  reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat_perm+",paste0(controlsc1_ET14DG15,collapse="+"))),data=DataCompletaPerm,subset=DataCompletaPerm[,"c1"]==1)
	  ResultsPermsTreat[[paste0("C1_",subject,"_",endline)]][i]=reg$coef["treat_perm"]
	}
	
	##Ahora para primero y PU juntos
	for(subject in c("math","read","kusoma")){	
	  reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat_perm+pu+pu:(",paste0(controlspu_ET14DG15_z0,collapse="+"),")+c1:(",paste0(controlsc1_ET14DG15_z0,collapse="+"),")")),data=DataCompletaPerm,subset=(DataCompletaPerm[,"c1"]==1 | DataCompletaPerm[,"pu"]==1))
	  ResultsPermsTreat[[paste0("C1PU_",subject,"_",endline)]][i]=reg$coef["treat_perm"]
	}
	
	##Ahora para C5
	for(subject in c("math","lang","swahili","sci","ss","re")){	
	  reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat_perm+",paste0(controlsc5_ET14DG15,collapse="+"))),data=DataCompletaPerm,subset=DataCompletaPerm[,"c5"]==1)
	  ResultsPermsTreat[[paste0("C5_",subject,"_",endline)]][i]=reg$coef["treat_perm"]
	}
	
	##Ahora para C6
	for(subject in c("math","lang","swahili","sci","ss","re")){	
	  reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat_perm+",paste0(controlsc6_ET14DG15,collapse="+"))),data=DataCompletaPerm,subset=DataCompletaPerm[,"c6"]==1)
	  ResultsPermsTreat[[paste0("C6_",subject,"_",endline)]][i]=reg$coef["treat_perm"]
	}
	
	##Ahora para C5/C6
	for(subject in c("math","lang","swahili","sci","ss","re")){	
	  reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat_perm+c5+c5:(",paste0(controlsc5_ET14DG15,collapse="+"),")+c6:(",paste0(controlsc6_ET14DG15,collapse="+"),")")),data=DataCompletaPerm,subset=(DataCompletaPerm[,"c5"]==1 | DataCompletaPerm[,"c6"]==1))
	  ResultsPermsTreat[[paste0("C5C6_",subject,"_",endline)]][i]=reg$coef["treat_perm"]
	}
      
      
    }
      
}



ResultsRealTreat=list()

##Primero recorre todos los endlines
for(endline in endline_vector){
  
  ##Ahora para  PU
    for(subject in c("math","read","kusoma","env")){	
      reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat+",paste0(controlspu_ET14DG15,collapse="+"))),data=DataCompleta,subset=DataCompleta[,"pu"]==1)
      ResultsRealTreat[[paste0("PU_",subject,"_",endline)]]=reg$coef["treat"]
    }
    
    ##Ahora para primero
    for(subject in c("math","read","kusoma","sci","ss","re")){	
      reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat+",paste0(controlsc1_ET14DG15,collapse="+"))),data=DataCompleta,subset=DataCompleta[,"c1"]==1)
      ResultsRealTreat[[paste0("C1_",subject,"_",endline)]]=reg$coef["treat"]
    }
    
    ##Ahora para primero y PU juntos
    for(subject in c("math","read","kusoma")){	
      reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat+pu+pu:(",paste0(controlspu_ET14DG15_z0,collapse="+"),")+c1:(",paste0(controlsc1_ET14DG15_z0,collapse="+"),")")),data=DataCompleta,subset=(DataCompleta[,"c1"]==1 | DataCompleta[,"pu"]==1))
      ResultsRealTreat[[paste0("C1PU_",subject,"_",endline)]]=reg$coef["treat"]
    }
    
    ##Ahora para C5
    for(subject in c("math","lang","swahili","sci","ss","re")){	
      reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat+",paste0(controlsc5_ET14DG15,collapse="+"))),data=DataCompleta,subset=DataCompleta[,"c5"]==1)
      ResultsRealTreat[[paste0("C5_",subject,"_",endline)]]=reg$coef["treat"]
    }
    
    ##Ahora para C6
    for(subject in c("math","lang","swahili","sci","ss","re")){	
      reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat+",paste0(controlsc6_ET14DG15,collapse="+"))),data=DataCompleta,subset=DataCompleta[,"c6"]==1)
      ResultsRealTreat[[paste0("C6_",subject,"_",endline)]]=reg$coef["treat"]
    }
    
    ##Ahora para C5C6
    for(subject in c("math","lang","swahili","sci","ss","re")){	
      reg=lm(as.formula(paste0(subject,"_",endline,"_Z~treat+c5+c5:(",paste0(controlsc5_ET14DG15,collapse="+"),")+c6:(",paste0(controlsc6_ET14DG15,collapse="+"),")")),data=DataCompleta,subset=(DataCompleta[,"c5"]==1 | DataCompleta[,"c6"]==1))
      ResultsRealTreat[[paste0("C5C6_",subject,"_",endline)]]=reg$coef["treat"]
    }
   
}



save(num_reps,ResultsRealTreat,ResultsPermsTreat,file="CreatedData/PermutationTests_ET14DG15.RData")



 



##Primero recorre todos los endlines
for(endline in endline_vector){
  
  ##Ahora para  PU
    for(subject in c("math","read","kusoma","env")){
    	pdf(paste0("Results/Graphs/ET14DG15_",endline,"_TreatmentPerm_PU_",subject,".pdf"))
    	pvalue=round(Funpvalue(ResultsRealTreat[[paste0("PU_",subject,"_",endline)]],ResultsPermsTreat[[paste0("PU_",subject,"_",endline)]],0),digits=2)
	hist(ResultsPermsTreat[[paste0("PU_",subject,"_",endline)]],main=paste("Permutation test for treatment at",endline,"grade PU"),sub=paste0("p-value (H_0=Treat-Control=0)=",pvalue),xlab="Mean difference")
	abline(v=ResultsRealTreat[[paste0("PU_",subject,"_",endline)]],lwd=2,col=4)
	dev.off()
    }
    
    ##Ahora para primero
    for(subject in c("math","read","kusoma","sci","ss","re")){	
    	pdf(paste0("Results/Graphs/ET14DG15_",endline,"_TreatmentPerm_C1_",subject,".pdf"))
	pvalue=round(Funpvalue(ResultsRealTreat[[paste0("C1_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C1_",subject,"_",endline)]],0),digits=2)
	hist(ResultsPermsTreat[[paste0("C1_",subject,"_",endline)]],main=paste("Permutation test for treatment at",endline,"grade C1"),sub=paste0("p-value (H_0=Treat-Control=0)=",pvalue),xlab="Mean difference")
	abline(v=ResultsRealTreat[[paste0("C1_",subject,"_",endline)]],lwd=2,col=4)
	dev.off()
    }
    
    ##Ahora para primero y PU juntos
    for(subject in c("math","read","kusoma")){	
    	pdf(paste0("Results/Graphs/ET14DG15_",endline,"_TreatmentPerm_C1PU_",subject,".pdf"))
	pvalue=round(1-sum(abs(ResultsRealTreat[[paste0("C1PU_",subject,"_",endline)]])>ResultsPermsTreat[[paste0("C1PU_",subject,"_",endline)]])/num_reps,digits=2)
	hist(ResultsPermsTreat[[paste0("C1PU_",subject,"_",endline)]],main=paste("Permutation test for treatment at",endline,"grades PU/C1"),sub=paste0("p-value (H_0=Treat-Control=0)=",pvalue),xlab="Mean difference")
	abline(v=ResultsRealTreat[[paste0("C1PU_",subject,"_",endline)]],lwd=2,col=4)
	dev.off()
    }
    
    ##Ahora para C5
    for(subject in c("math","lang","swahili","sci","ss","re")){	
    	pdf(paste0("Results/Graphs/ET14DG15_",endline,"_TreatmentPerm_C5_",subject,".pdf"))
	pvalue=round(Funpvalue(ResultsRealTreat[[paste0("C5_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C5_",subject,"_",endline)]],0),digits=2)
	hist(ResultsPermsTreat[[paste0("C5_",subject,"_",endline)]],main=paste("Permutation test for treatment at",endline,"grade C5"),sub=paste0("p-value (H_0=Treat-Control=0)=",pvalue),xlab="Mean difference")
	abline(v=ResultsRealTreat[[paste0("C5_",subject,"_",endline)]],lwd=2,col=4)
	dev.off()
    }
    
    ##Ahora para C6
    for(subject in c("math","lang","swahili","sci","ss","re")){	
    	pdf(paste0("Results/Graphs/ET14DG15_",endline,"_TreatmentPerm_C6_",subject,".pdf"))
	pvalue=round(1-sum(abs(ResultsRealTreat[[paste0("C6_",subject,"_",endline)]])>ResultsPermsTreat[[paste0("C6_",subject,"_",endline)]])/num_reps,digits=2)
	hist(ResultsPermsTreat[[paste0("C6_",subject,"_",endline)]],main=paste("Permutation test for treatment at",endline,"grade C6"),sub=paste0("p-value (H_0=Treat-Control=0)=",pvalue),xlab="Mean difference")
	abline(v=ResultsRealTreat[[paste0("C6_",subject,"_",endline)]],lwd=2,col=4)
	dev.off()
    }
    
    ##Ahora para C5/C6 juntos
    for(subject in c("math","lang","swahili","sci","ss","re")){	
    	pdf(paste0("Results/Graphs/ET14DG15_",endline,"_TreatmentPerm_C5C6_",subject,".pdf"))
	pvalue=round(Funpvalue(ResultsRealTreat[[paste0("C5C6_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C5C6_",subject,"_",endline)]],0),digits=2)
	hist(ResultsPermsTreat[[paste0("C5C6_",subject,"_",endline)]],main=paste("Permutation test for treatment at",endline,"grades C5/C6"),sub=paste0("p-value (H_0=Treat-Control=0)=",pvalue),xlab="Mean difference")
	abline(v=ResultsRealTreat[[paste0("C5C6_",subject,"_",endline)]],lwd=2,col=4)
	dev.off()
	
    }
   
}

  
  



  ##Ahora para  PU
    for(subject in c("math","read","kusoma","env")){
    Estimates=NULL
    CI=matrix(NA,nrow=length(endline_vector),ncol=2)
      for(endline in endline_vector){
	  CI[which(endline_vector==endline),]=ConfidenceInterval(ResultsRealTreat[[paste0("PU_",subject,"_",endline)]],ResultsPermsTreat[[paste0("PU_",subject,"_",endline)]])
	  Estimates[which(endline_vector==endline)]=ResultsRealTreat[[paste0("PU_",subject,"_",endline)]]
	}
	
    	pdf(paste0("Results/Graphs/ET14DG15_Evolution_TreatmentPerm_PU_",subject,".pdf"))
	plotCI(1:length(endline_vector), Estimates, ui=CI[,2], li=CI[,1],pch=19,lwd=1,slty=5,sfrac=0.01,xaxt="n",xlab="",ylab="Treatment effect",main=paste("Evolution of treatment effect. Subject:",subject,"- Grade: PU"))
	axis(1, at=1:length(endline_vector),labels=endline_vector, las=2)
	abline(h=0,lwd=2)
	dev.off()
    }
    



      ##Ahora para  C1
    for(subject in c("math","read","kusoma","sci","ss","re")){
    Estimates=NULL
    CI=matrix(NA,nrow=length(endline_vector),ncol=2)

      for(endline in endline_vector){
	  CI[which(endline_vector==endline),]=ConfidenceInterval(ResultsRealTreat[[paste0("C1_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C1_",subject,"_",endline)]])
	  Estimates[which(endline_vector==endline)]=ResultsRealTreat[[paste0("C1_",subject,"_",endline)]]
	}
	
    	pdf(paste0("Results/Graphs/ET14DG15_Evolution_TreatmentPerm_C1_",subject,".pdf"))
	plotCI(1:length(endline_vector), Estimates, ui=CI[,2], li=CI[,1],pch=19,lwd=1,slty=5,sfrac=0.01,xaxt="n",xlab="",ylab="Treatment effect",main=paste("Evolution of treatment effect. Subject:",subject,"- Grade: C1"))
	axis(1, at=1:length(endline_vector),labels=endline_vector, las=2)
	abline(h=0,lwd=2)
	dev.off()
    }
    


    ##Ahora para primero y PU juntos
    for(subject in c("math","read","kusoma")){
    Estimates=NULL
    CI=matrix(NA,nrow=length(endline_vector),ncol=2)
      for(endline in endline_vector){
	  CI[which(endline_vector==endline),]=ConfidenceInterval(ResultsRealTreat[[paste0("C1PU_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C1PU_",subject,"_",endline)]])
	  Estimates[which(endline_vector==endline)]=ResultsRealTreat[[paste0("C1PU_",subject,"_",endline)]]
   }
	
    	pdf(paste0("Results/Graphs/ET14DG15_Evolution_TreatmentPerm_C1PU_",subject,".pdf"))
	plotCI(1:length(endline_vector), Estimates, ui=CI[,2], li=CI[,1],pch=19,lwd=1,slty=5,sfrac=0.01,xaxt="n",xlab="",ylab="Treatment effect",main=paste("Evolution of treatment effect. Subject:",subject,"- Grade: C1PU"))
	axis(1, at=1:length(endline_vector),labels=endline_vector, las=2)
	abline(h=0,lwd=2)
	dev.off()
    }
    

    ##Ahora para C5
    for(subject in c("math","lang","swahili","sci","ss","re")){	
    Estimates=NULL
    CI=matrix(NA,nrow=length(endline_vector),ncol=2)
    for(endline in endline_vector){
	  CI[which(endline_vector==endline),]=ConfidenceInterval(ResultsRealTreat[[paste0("C5_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C5_",subject,"_",endline)]])
	  Estimates[which(endline_vector==endline)]=ResultsRealTreat[[paste0("C5_",subject,"_",endline)]]
	}
	
    	pdf(paste0("Results/Graphs/ET14DG15_Evolution_TreatmentPerm_C5_",subject,".pdf"))
	plotCI(1:length(endline_vector), Estimates, ui=CI[,2], li=CI[,1],pch=19,lwd=1,slty=5,sfrac=0.01,xaxt="n",xlab="",ylab="Treatment effect",main=paste("Evolution of treatment effect. Subject:",subject,"- Grade: C5"))
	axis(1, at=1:length(endline_vector),labels=endline_vector, las=2)
	abline(h=0,lwd=2)
	dev.off()
    }



    ##Ahora para C6
    for(subject in c("math","lang","swahili","sci","ss","re")){
    Estimates=NULL
    CI=matrix(NA,nrow=length(endline_vector),ncol=2)
    for(endline in endline_vector){
	  CI[which(endline_vector==endline),]=ConfidenceInterval(ResultsRealTreat[[paste0("C6_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C6_",subject,"_",endline)]])
	  Estimates[which(endline_vector==endline)]=ResultsRealTreat[[paste0("C6_",subject,"_",endline)]]
	}
	
    	pdf(paste0("Results/Graphs/ET14DG15_Evolution_TreatmentPerm_C6_",subject,".pdf"))
	plotCI(1:length(endline_vector), Estimates, ui=CI[,2], li=CI[,1],pch=19,lwd=1,slty=5,sfrac=0.01,xaxt="n",xlab="",ylab="Treatment effect",main=paste("Evolution of treatment effect. Subject:",subject,"- Grade: C6"))
	axis(1, at=1:length(endline_vector),labels=endline_vector, las=2)
	abline(h=0,lwd=2)
	dev.off()
    }
    
    
        ##Ahora para C5C6
    for(subject in c("math","lang","swahili","sci","ss","re")){
    Estimates=NULL
    CI=matrix(NA,nrow=length(endline_vector),ncol=2)
    for(endline in endline_vector){
	  CI[which(endline_vector==endline),]=ConfidenceInterval(ResultsRealTreat[[paste0("C5C6_",subject,"_",endline)]],ResultsPermsTreat[[paste0("C5C6_",subject,"_",endline)]])
	  Estimates[which(endline_vector==endline)]=ResultsRealTreat[[paste0("C5C6_",subject,"_",endline)]]
	}
	
    	pdf(paste0("Results/Graphs/ET14DG15_Evolution_TreatmentPerm_C5C6_",subject,".pdf"))
	plotCI(1:length(endline_vector), Estimates, ui=CI[,2], li=CI[,1],pch=19,lwd=1,slty=5,sfrac=0.01,xaxt="n",xlab="",ylab="Treatment effect",main=paste("Evolution of treatment effect. Subject:",subject,"- Grade: C5C6"))
	axis(1, at=1:length(endline_vector),labels=endline_vector, las=2)
	abline(h=0,lwd=2)
	dev.off()
	
    }


