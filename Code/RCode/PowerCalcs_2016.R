   SampleSize=function(effect,size_alpha,power_beta,Csize,rho,P){
return((1/(P*(1-P)))*(1+(Csize-1)*rho)*((qnorm(1-size_alpha)+qnorm(power_beta))/(effect))^2)
}


MDE=function(N_Schools,students_School,sigma,size_alpha,power_beta,rho,P){
sqrt(rho+sigma*(1-rho)/students_School)*(qnorm(1-size_alpha/2)+qnorm(power_beta))/(sqrt(P*(1-P)*N_Schools))
}

SeqX=seq(0.1,0.5,0.01)
Attrition=0.9
alpha_pod=0.05
beta_pod=0.9
intra_class=0.3

##For early grades assume that R^2 between test score and tescore(-1) is 0.1
##For later grades assume that R^2 between test score and tescore(-1) is 0.2
##Safe to assume ICC is 0.3
EffectSize=sapply(SeqX,FUN=MDE,N_Schools=405*Attrition^2,students_School=15,sigma=1,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)

plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Conservative (attrition+ no covariates) assumptions and MDE",type="b")
abline(h=0.2)

EffectSize2=sapply(SeqX,FUN=MDE,N_Schools=405*Attrition^2,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)

plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Mid-Conservative (attrition+covariates) assumptions and MDE",type="b")
abline(h=0.2)

EffectSize3=sapply(SeqX,FUN=MDE,N_Schools=405,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)

plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Aggresive-Scenario (no attrition+covariates) assumptions and MDE",type="b")
abline(h=0.2)


plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Assumptions and MDE",type="l",col=1,,lwd=2,ylim=range(c(EffectSize,EffectSize2,EffectSize3),finite=T),sub="intra-cluster correlation=0.3, covariates R^2=20%, 20% (of schools) missing/round")
lines(SeqX,EffectSize2,col=2,lwd=2)
lines(SeqX,EffectSize3,col=3,lwd=2)
abline(h=0.2,col=4)
legend("topright",c("Attrition/No Covariates","Attrition/Covariates","No Attrition/Covariates"),pch=19,lty=1,col=c(1,2,3))



SeqX=seq(0.1,0.5,0.01)
Attrition=0.9

##For early grades assume that R^2 between test score and tescore(-1) is 0.1
##For later grades assume that R^2 between test score and tescore(-1) is 0.2
##Safe to assume ICC is 0.3
EffectSize=sapply(SeqX,FUN=MDE,N_Schools=405*Attrition^2,students_School=15,sigma=1,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)
EffectSize2=sapply(SeqX,FUN=MDE,N_Schools=405*Attrition^2,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)
EffectSize3=sapply(SeqX,FUN=MDE,N_Schools=405,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)
legend("bottomleft",c("alpha=0.05","beta=0.9","rho=0.3","R^2 cov=20%","Attrition=20%"))



plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Assumptions and MDE",type="l",col=1,,lwd=2,ylim=range(c(EffectSize,EffectSize2,EffectSize3),finite=T),sub="intra-cluster correlation=0.3, covariates R^2=20%, 20% (of schools) missing/round")
lines(SeqX,EffectSize2,col=2,lwd=2)
lines(SeqX,EffectSize3,col=3,lwd=2)
abline(h=0.2,col=4)
legend("topright",c("Attrition/No Covariates","Attrition/Covariates","No Attrition/Covariates"),pch=19,lty=1,col=c(1,2,3))



SeqX=seq(0.1,0.5,0.01)
Attrition=0.8

##For early grades assume that R^2 between test score and tescore(-1) is 0.1
##For later grades assume that R^2 between test score and tescore(-1) is 0.2
##Safe to assume ICC is 0.3
EffectSize=sapply(SeqX,FUN=MDE,N_Schools=405*Attrition^2,students_School=15,sigma=1,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)
EffectSize2=sapply(SeqX,FUN=MDE,N_Schools=405*Attrition^2,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)
EffectSize3=sapply(SeqX,FUN=MDE,N_Schools=405,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)


plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Assumptions and MDE",type="l",col=1,,lwd=2,ylim=range(c(EffectSize,EffectSize2,EffectSize3),finite=T))
lines(SeqX,EffectSize2,col=2,lwd=2)
lines(SeqX,EffectSize3,col=3,lwd=2)
abline(h=0.2,col=4)
legend("topright",c("Attrition/No Covariates","Attrition/Covariates","No Attrition/Covariates"),pch=19,lty=1,col=c(1,2,3))
legend("right",c("alpha=0.05","beta=0.9","rho=0.3","R^2 cov=20%","Attrition=20%"))



SeqX=seq(0.1,0.5,0.01)
Attrition=0.9
alpha_pod=0.1
beta_pod=0.8
intra_class=0.2
##For early grades assume that R^2 between test score and tescore(-1) is 0.1
##For later grades assume that R^2 between test score and tescore(-1) is 0.2
##Safe to assume ICC is 0.3
EffectSize=sapply(SeqX,FUN=MDE,N_Schools=372*Attrition^2,students_School=15,sigma=1,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)
EffectSize2=sapply(SeqX,FUN=MDE,N_Schools=372*Attrition^2,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)


plot(SeqX,EffectSize,xlab="Proportion of schools in control",ylab="MDE",main="Assumptions and MDE",type="l",col=1,,lwd=2,ylim=range(c(EffectSize,EffectSize2,EffectSize3),finite=T))
lines(SeqX,EffectSize2,col=2,lwd=2)
abline(h=0.2,col=4)
legend("topright",c("Attrition/No Covariates","Attrition/Covariates"),pch=19,lty=1,col=c(1,2,3))
legend("right",c("alpha=0.1","beta=0.8","rho=0.3","R^2 cov=20%","Attrition=20%"))

sapply(0.5,FUN=MDE,N_Schools=120*Attrition^2,students_School=15,sigma=0.7,size_alpha=alpha_pod,power_beta=beta_pod,rho=intra_class)


SampleSize=function(effect,size_alpha,power_beta,Csize,rho,P,sigma){
((rho+sigma*(1-rho)/Csize))*((qnorm(1-size_alpha/2)+qnorm(power_beta))^2)/(P*(1-P)*(effect^2))
}
MDE=function(N_Schools,students_School,sigma,size_alpha,power_beta,rho,P){
sqrt(rho+sigma*(1-rho)/students_School)*(qnorm(1-size_alpha/2)+qnorm(power_beta))/(sqrt(P*(1-P)*N_Schools))
}

SampleSize(0.2,0.1,0.9,100,0.3,0.25,0.7)

MDE(274,15,0.7,0.1,0.8,0.3,0.25)


fun1=function(x){
SampleSize(0.2,0.1,0.8,100,0.1,x,0.7)-26
}
plot(seq(0,0.5,0.01), sapply(seq(0,0.5,0.01),fun1),xlab="Control schools",main="Size to detect a 0.2 SD effect: Long Term",ylab="size")


  SampleSize2=function(effect,size_alpha,power_beta,Csize,rho,n_controls,sigma){
((rho+sigma*(1-rho)/Csize))*((qnorm(1-size_alpha/2)+qnorm(power_beta))^2)/(P*(1-P)*(effect^2))
}


MDE=function(N_Schools,students_School,sigma,size_alpha,power_beta,rho,P){
sqrt(rho+sigma*(1-rho)/students_School)*(qnorm(1-size_alpha/2)+qnorm(power_beta))/(sqrt(P*(1-P)*N_Schools))
}

MDE(120,30,0.7,0.05,0.8,0.1,0.5)

MDE(120,30,0.7,0.05,0.8,0.3,0.5)
MDE(120,30,0.7,0.05,0.9,0.3,0.5)
MDE(120,30,0.7,0.05,0.95,0.3,0.5)



