use "$basein\Fully_Linked_2015.dta",clear
drop *teach*
drop if flag_T1DG15_T3ET15 > 0



keep pupilid percent* academyid* grade*
foreach year in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
	foreach year2 in   T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
		replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
		replace academyid_`year2'="Mariakani-KLF" if academyid_`year2'=="Mariakani-MBA"
		replace academyid_`year2'="Ugunja-SYA" if academyid_`year2'=="Ugunja-KIS"
		drop if academyid_`year'!=academyid_`year2' &  academyid_`year'!="" &  academyid_`year2'!=""
	}
}

foreach year in  T3MT15 T2ET15 T2MT15 T1ET15 T1MT15 T1DG15 {
	replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
	replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
	replace academyid_T3ET15=academyid_`year' if academyid_T3ET15=="" & academyid_`year'!=""
}



rename academyid_T3ET15 academyid
drop academyid_*
drop if academyid==""
compress
preserve
keep *T3ET15 pupilid academyid grade*
save "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2\T3ET15_Data.dta", replace
restore
drop *_c7_* *_c8_*
rename *_ba_* *_BA_*
rename *_ns_* *_NS_*
rename *_pu_* *_PU_*
rename *_swahili_* *_kiswahili_*
rename *_create_* *_creative_*


foreach grade in PU NS BA c1 c2 c3 c4 c5 c6{
reg percentscore_math_`grade'_T3ET15 c.(percentscore_math_`grade'_T3MT15)##c.(percentscore_math_`grade'_T3MT15)
predict yhat, xb
replace percentscore_math_`grade'_T3ET15=yhat if percentscore_math_`grade'_T3ET15==.
drop yhat
}

foreach grade in PU NS BA c1 c2 c3 c4 c5 c6{
reg percentscore_math_`grade'_T3ET15 c.(percentscore_math_`grade'_T2ET15)##c.(percentscore_math_`grade'_T2ET15)
predict yhat, xb
replace percentscore_math_`grade'_T3ET15=yhat if percentscore_math_`grade'_T3ET15==.
drop yhat
}

foreach grade in PU NS BA c1 c2 c3 c4 c5 c6{
reg percentscore_math_`grade'_T3ET15 c.(percentscore_math_`grade'_T2MT15)##c.(percentscore_math_`grade'_T2MT15)
predict yhat, xb
replace percentscore_math_`grade'_T3ET15=yhat if percentscore_math_`grade'_T3ET15==.
drop yhat
}

foreach grade in PU NS BA c1 c2 c3 c4 c5 c6{
reg percentscore_math_`grade'_T3ET15 c.(percentscore_math_`grade'_T1ET15)##c.(percentscore_math_`grade'_T1ET15)
predict yhat, xb
replace percentscore_math_`grade'_T3ET15=yhat if percentscore_math_`grade'_T3ET15==.
drop yhat
}

foreach grade in PU NS BA c1 c2 c3 c4 c5 c6{
reg percentscore_math_`grade'_T3ET15 c.(percentscore_math_`grade'_T1MT15)##c.(percentscore_math_`grade'_T1MT15)
predict yhat, xb
replace percentscore_math_`grade'_T3ET15=yhat if percentscore_math_`grade'_T3ET15==.
drop yhat
}

collapse (mean) percent*, by(academyid)


***Now we drop academies that we dont really care about ****
merge 1:1 academyid using "$base_out\DropPTRMissingYR2.dta", keepus(DropPTRMissing)
drop _merge
merge 1:1 academyid using "$base_out\SchoolLeveLData.dta", keepus(treat)
drop if _merge==3
drop _merge
drop if DropPTRMissing==1





pca  percentscore_math_BA_T3ET15 percentscore_math_c1_T3ET15 percentscore_math_c2_T3ET15 percentscore_math_c3_T3ET15 percentscore_math_c4_T3ET15 percentscore_math_c5_T3ET15 percentscore_math_c6_T3ET15
predict Total, score 
keep academyid Total
save "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2\Score.dta", replace


