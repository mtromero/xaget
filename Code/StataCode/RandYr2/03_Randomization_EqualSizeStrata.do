set seed 7595009 /*winde card found around */

cd "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2"



import excel "All Academies for Charts 2015.09.17.xlsx", sheet("Raw") clear firstrow
rename Academy_Name academyid
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
keep academyid FormerProvince
merge 1:1 academyid  using "Score.dta", keepus(Total)
drop if _merge!=3
drop _merge

xtile Strata=Total, nq(7)


gen rand = runiform()
sort Strata rand
egen lrank = rank(_n), by(Strata)


gen Treatment=.
replace Treatment=0 if lrank<=7
replace Treatment=1 if lrank>7



tab Treatment
list academyid academyid if Treatment==0
tab Treatment Strata
tab Treatment FormerProvince



keep academyid Treatment FormerProvince Strata

save "FinalRandomization.dta", replace
export delimited using "FinalRandomization.csv", replace




capture matrix drop  resultados 
use "T3ET15_Data.dta", clear
*tabstat *math*, by(gradename_T3ET15) statistics(count)

merge m:1 academyid using "FinalRandomization.dta"
encode academyid, gen(Academy_Name2)
encode FormerProvince, gen(FormerProvince2)
rename percentscore_* *
foreach var of varlist dev_BA_T3ET15- read_PU_T3ET15{
qui reg `var' Treatment i.Strata, vce(cluster Academy_Name2)
qui test (_b[Treatment]== 0)
qui mat resultados = nullmat(resultados)\ r(p)
}
qui matrix list resultados
clear
svmat resultados
gen menor5=( resultados1<0.05)
gen menor10=( resultados1<0.1)
sum menor*

capture matrix drop  resultados 
use "T3ET15_Data.dta", clear
merge m:1 academyid using "FinalRandomization.dta"
encode academyid, gen(Academy_Name2)
encode FormerProvince, gen(FormerProvince2)
rename percentscore_* *
foreach var of varlist *math*{
reg `var' Treatment i.Strata, vce(cluster Academy_Name2)
qui test (_b[Treatment]== 0)
qui mat resultados = nullmat(resultados)\ r(p)
}
qui matrix list resultados
clear
svmat resultados
gen menor5=( resultados1<0.05)
gen menor10=( resultados1<0.1)
sum menor*

capture matrix drop  resultados 
use "T3ET15_Data.dta", clear
merge m:1 academyid using "FinalRandomization.dta"
encode academyid, gen(Academy_Name2)
encode FormerProvince, gen(FormerProvince2)
rename percentscore_* *
keep  pupilid dev_BA_T3ET15- read_PU_T3ET15 academyid Strata Treatment Academy_Name2
reshape long dev_BA_@ env_BA_@ lang_BA_@ math_BA_@ kuandika_c1_@ kusoma_c1_@ lang_c1_@ math_c1_@ re_c1_@ read_c1_@ sci_c1_@ ss_c1_@ kuandika_c2_@ kusoma_c2_@ lang_c2_@ math_c2_@ re_c2_@ read_c2_@ sci_c2_@ ss_c2_@ kuandika_c3_@ kusoma_c3_@ lang_c3_@ math_c3_@ re_c3_@ read_c3_@ sci_c3_@ ss_c3_@ comp_c4_@ insha_c4_@ kiswahili_c4_@ lang_c4_@ math_c4_@ re_c4_@ sci_c4_@ ss_c4_@ comp_c5_@ insha_c5_@ kiswahili_c5_@ lang_c5_@ math_c5_@ re_c5_@ sci_c5_@ ss_c5_@ comp_c6_@ insha_c6_@ kiswahili_c6_@ lang_c6_@ math_c6_@ re_c6_@ sci_c6_@ ss_c6_@ dev_NS_@ env_NS_@ lang_NS_@ math_NS_@ creative_PU_@ env_PU_@ kuandika_PU_@ kusoma_PU_@ lang_PU_@ math_PU_@ read_PU_@, i(pupilid) j(term) string

rename *_ *

reshape long dev_@ env_@ lang_@ math_@ kuandika_@ kusoma_@ re_@ read_@ ss_@ sci_@ insha_@ kiswahili_@ creative_@ comp_@, i(pupilid term) j(grade) string
rename *_ *
encode grade, gen(grade2)
foreach var of varlist lang- ss{
reg `var' Treatment i.Strata i.grade2, vce(cluster Academy_Name2)
qui test (_b[Treatment]== 0)
qui mat resultados = nullmat(resultados)\ r(p)
}
qui matrix list resultados
clear
svmat resultados
gen menor5=( resultados1<0.05)
gen menor10=( resultados1<0.1)
sum menor*
