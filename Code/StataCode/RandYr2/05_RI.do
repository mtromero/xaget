
qui forvalues i=0/1000{
scalar define seed_s= 7595009+`i'
set seed `=scalar(seed_s)' /*winde card found around */

cd "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2"

import excel "All Academies for Charts 2015.09.17.xlsx", sheet("Raw") clear firstrow
rename Academy_Name academyid
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
keep academyid FormerProvince
merge 1:1 academyid  using "Score.dta", keepus(Total)
drop if _merge!=3
drop _merge
preserve
drop if FormerProvince!="North Eastern"
gen NE=1
save "NE_Schools.dta", replace
restore
drop if FormerProvince=="North Eastern"

sort FormerProvince Total
bys FormerProvince: g dist_treat_score_rank=_n
bys FormerProvince: g dist_treat_score_Total=_N
gen StrataScore=1 if dist_treat_score_rank>=ceil(dist_treat_score_Total/2)
replace StrataScore=0 if dist_treat_score_rank<=floor(dist_treat_score_Total/2)
tab FormerProvince StrataScore
drop dist_treat_score_rank dist_treat_score_Total


gen rand = runiform()
sort FormerProvince StrataScore rand
egen lrank = rank(_n), by(FormerProvince StrataScore)
bys FormerProvince  StrataScore: gen lnumb = _N
gen position=lrank/lnumb


scalar define part=57/187

gen Treatment=.
replace Treatment=0 if position<scalar(part) 
replace Treatment=1 if position>=scalar(part)



tab Treatment
list academyid academyid if Treatment==0
tab Treatment FormerProvince
tab Treatment StrataScore

keep academyid Treatment
rename Treatment Treatment`i'
save "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2\Rand`i'.dta", replace
}

use "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2\Rand0.dta", clear
qui forvalues i=1/1000{
merge 1:1 academyid using "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2\Rand`i'.dta"
drop _merge
}
egen ProbTreatment=rowmean(Treatment1- Treatment1000)
drop Treatment1- Treatment1000

merge 1:1 academyid using "FinalRandomization.dta"
encode FormerProvince, gen(FormerProvince2)

reg ProbTreatment Treatment0 FormerProvince2## StrataScore
