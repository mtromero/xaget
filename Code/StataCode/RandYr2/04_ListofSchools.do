use "FinalRandomization.dta", clear
merge 1:1 academyid using "$base_out\DropPTRMissingYR2.dta", keepus(DropPTRMissing)
drop _merge
merge 1:1 academyid using "$base_out\SchoolLeveLData.dta", keepus(treat)
drop _merge
merge 1:1 academyid using "NE_Schools.dta", keepus(NE)
drop _merge

gen FullyTreated=1 if treat==1 | Treatment==1 | DropPTRMissing==1 | NE==1
gen PartiallyTreated=1 if treat==0 & DropPTRMissing==0
gen Controls=1 if Treatment==0
keep academyid Controls PartiallyTreated FullyTreated

save "FinalSchedule.dta", replace
export delimited using "FinalSchedule.csv", replace

