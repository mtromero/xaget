set seed 08301986

cd "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2"



import excel "All Academies for Charts 2015.09.17.xlsx", sheet("Raw") clear firstrow
rename Academy_Name academyid
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
keep academyid FormerProvince
sort academyid
merge 1:1 academyid using "$base_out\DropPTRMissingYR2.dta", keepus(DropPTRMissing)
drop _merge
merge 1:1 academyid using "$base_out\SchoolLeveLData.dta", keepus(treat)
drop if _merge==3
drop _merge
drop if DropPTRMissing==1




gen rand = runiform()
sort FormerProvince rand
egen lrank = rank(_n), by(FormerProvince)
bys FormerProvince: gen lnumb = _N
gen position=lrank/lnumb


scalar define part=54/190

gen Treatment=.
replace Treatment=0 if position<scalar(part) 
replace Treatment=1 if position>=scalar(part)
replace Treatment=0 if FormerProvince=="North Eastern" & lrank==1


tab Treatment
list academyid academyid if Treatment==0
tab Treatment FormerProvince



keep academyid FormerProvince Treatment

save "FinalRandomization.dta", replace
export delimited using "FinalRandomization.csv", replace




capture matrix drop  resultados 
use "T3ET15_Data.dta", clear
*tabstat *math*, by(gradename_T3ET15) statistics(count)

merge m:1 academyid using "FinalRandomization.dta"
encode academyid, gen(Academy_Name2)
encode FormerProvince, gen(FormerProvince2)
rename percentscore_* *
foreach var of varlist dev_BA_T3ET15- read_PU_T3ET15{
qui reg `var' Treatment i.FormerProvince2, vce(cluster Academy_Name2)
qui test (_b[Treatment]== 0)
qui mat resultados = nullmat(resultados)\ r(p)
}
qui matrix list resultados
clear
svmat resultados
gen menor5=( resultados1<0.05)
gen menor10=( resultados1<0.1)
sum menor*

capture matrix drop  resultados 
use "T3ET15_Data.dta", clear
merge m:1 academyid using "FinalRandomization.dta"
encode academyid, gen(Academy_Name2)
encode FormerProvince, gen(FormerProvince2)
rename percentscore_* *
foreach var of varlist *math*{
qui reg `var' Treatment i.FormerProvince2, vce(cluster Academy_Name2)
qui test (_b[Treatment]== 0)
qui mat resultados = nullmat(resultados)\ r(p)
}
qui matrix list resultados
clear
svmat resultados
gen menor5=( resultados1<0.05)
gen menor10=( resultados1<0.1)
sum menor*


