
*cd "C:\Users\Mauricio\Dropbox\Learning Lab\XageT\"
cd "/media/mauricio/HDD1/Dropbox/Learning Lab/XageT/"
import delimited "RandYr2/FinalRandomization.csv", bindquote(strict) stripquote(yes) clear 
keep academyid treatment
rename treatment treatmentRCTYr2
gen RCTYr2=1
saveold "FullList.dta",version(13) replace

import delimited "RandYr2/FinalSchedule.csv", bindquote(strict) stripquote(yes) clear 
rename fullytreated fullytreatedYr2
rename partiallytreated partiallytreatedYr2
rename controls controlsYr2
replace controlsYr2=0 if controlsYr2==.
replace partiallytreatedYr2=0 if partiallytreatedYr2==.
replace fullytreatedYr2=0 if fullytreatedYr2==.



merge 1:1 academyid using "FullList.dta"
replace RCTYr2=0 if RCTYr2==.
drop _merge
saveold "FullList.dta",version(13) replace

import delimited "RawData/TreatmentStatus.csv", bindquote(strict) stripquote(yes) clear 

replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
keep academyid treat
rename treat treatmentRCTYr1
gen RCTYr1=1
merge 1:1 academyid using "FullList.dta"
drop _merge
saveold "FullList.dta",version(13) replace

import excel "/media/mauricio/HDD1/Dropbox/Learning Lab/XageT/RawData/MM_academies_Schedule2016.xlsx", sheet("Sheet1") firstrow clear
keep Conc NameofCalendar
rename Conc academyid
rename NameofCalendar FinalCalendar
replace academyid="Kodiaga-SYA" if academyid=="KOdiaga-SYA"
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"

merge 1:1 academyid using "FullList.dta"
drop _merge
saveold "FullList.dta",version(13) replace
replace RCTYr1=0 if RCTYr1==.

gen treatmentYr1=1 if FinalCalendar=="Maths Mentoring"
replace treatmentYr1=0 if FinalCalendar=="Control"
compress
drop  FinalCalendar
saveold "FullList.dta",version(13) replace



