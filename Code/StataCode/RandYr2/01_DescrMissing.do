use "$basein\Fully_Linked_2015.dta",clear
drop *teach*
drop if flag_T1DG15_T3ET15 > 0
keep pupilid percent* academyid*
drop *_c7_* *_c8_*

foreach year in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
	foreach year2 in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
		replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
		replace academyid_`year2'="Mariakani-KLF" if academyid_`year2'=="Mariakani-MBA"
		replace academyid_`year2'="Ugunja-SYA" if academyid_`year2'=="Ugunja-KIS"
		drop if academyid_`year'!=academyid_`year2' &  academyid_`year'!="" &  academyid_`year2'!=""
	}
}

foreach year in  T3MT15 T2ET15 T2MT15 T1ET15 T1MT15 T1DG15 {
	replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
	replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
	replace academyid_T3ET15=academyid_`year' if academyid_T3ET15=="" & academyid_`year'!=""
}

rename academyid_T3ET15 academyid
drop academyid_*
drop if academyid==""
compress
collapse (percent) percent*, by(academyid)

rename *_ba_* *_BA_*
rename *_ns_* *_NS_*
rename *_pu_* *_PU_*
rename *_swahili_* *_kiswahili_*
rename *_create_* *_creative_*
drop *T1DG15*
reshape long percentscore_dev_BA_@ percentscore_env_BA_@ percentscore_lang_BA_@ percentscore_math_BA_@ percentscore_kuandika_c1_@ percentscore_kusoma_c1_@ percentscore_lang_c1_@ percentscore_math_c1_@ percentscore_re_c1_@ percentscore_read_c1_@ percentscore_sci_c1_@ percentscore_ss_c1_@ percentscore_kuandika_c2_@ percentscore_kusoma_c2_@ percentscore_lang_c2_@ percentscore_math_c2_@ percentscore_re_c2_@ percentscore_read_c2_@ percentscore_sci_c2_@ percentscore_ss_c2_@ percentscore_kuandika_c3_@ percentscore_kusoma_c3_@ percentscore_lang_c3_@ percentscore_math_c3_@ percentscore_re_c3_@ percentscore_read_c3_@ percentscore_sci_c3_@ percentscore_ss_c3_@ percentscore_comp_c4_@ percentscore_insha_c4_@ percentscore_kiswahili_c4_@ percentscore_lang_c4_@ percentscore_math_c4_@ percentscore_re_c4_@ percentscore_sci_c4_@ percentscore_ss_c4_@ percentscore_comp_c5_@ percentscore_insha_c5_@ percentscore_kiswahili_c5_@ percentscore_lang_c5_@ percentscore_math_c5_@ percentscore_re_c5_@ percentscore_sci_c5_@ percentscore_ss_c5_@ percentscore_comp_c6_@ percentscore_insha_c6_@ percentscore_kiswahili_c6_@ percentscore_lang_c6_@ percentscore_math_c6_@ percentscore_re_c6_@ percentscore_sci_c6_@ percentscore_ss_c6_@ percentscore_dev_NS_@ percentscore_env_NS_@ percentscore_lang_NS_@ percentscore_math_NS_@ percentscore_creative_PU_@ percentscore_env_PU_@ percentscore_kuandika_PU_@ percentscore_kusoma_PU_@ percentscore_lang_PU_@ percentscore_math_PU_@ percentscore_read_PU_@, i(academyid) j(term) string

rename *_ *

reshape long percentscore_dev_@ percentscore_env_@ percentscore_lang_@ percentscore_math_@ percentscore_kuandika_@ percentscore_kusoma_@ percentscore_re_@ percentscore_read_@ percentscore_ss_@ percentscore_sci_@ percentscore_insha_@ percentscore_kiswahili_@ percentscore_creative_@ percentscore_comp_@, i(academyid term) j(grade) string
rename *_ *

reshape long percentscore_@, i(academyid term grade) j(subject) string
rename *_ *

replace percentscore=1 if percentscore>0 & !missing(percentscore)

foreach subject in kusoma kiswahili lang dev sci re kuandika math creative insha comp ss env read{
foreach grade in c3 BA c2 c6 c4 PU c1 c5 NS {
sum percentscore if  grade=="`grade'" & subject=="`subject'" 
if r(N)==0{
drop if  grade=="`grade'" & subject=="`subject'" 
}
}
}

save "$base_out\MissingData.dta",replace

/*
use "$base_out\MissingData.dta", clear
collapse (max) percentscore, by(academyid grade term)
collapse (mean) percentscore, by(academyid grade)
gen DataComeplta=(percentscore==1)
collapse (mean) DataComeplta, by(academyid)
sort DataComeplta academyid
save "$base_out\DataCompleta_Grade.dta",replace

use "$base_out\MissingData.dta", clear
collapse (max) percentscore, by(academyid grade term)
collapse (mean) percentscore, by(academyid term)
gen DataComeplta=(percentscore==1)
collapse (mean) DataComeplta, by(academyid)
sort DataComeplta academyid
save "$base_out\DataCompleta_Period.dta",replac
*/

use "$base_out\MissingData.dta", clear
gen NonMissing=(percentscore==1)
gen Missing=(percentscore==0)
collapse (sum) Missing (mean) NonMissing=NonMissing, by(academyid)
save "$base_out\DataCompleta_Often.dta",replace

/*
gen DropMissing=(NonMissing<0.7)
keep academyid DropMissing
save "$base_out\DropMissing.dta",replace

use "$base_out\DropMissing.dta", clear
replace academyid="Mariakani-MBA" if academyid=="Mariakani-KLF"
merge 1:1 academyid using "$base_out\RandomizationXageT.dta", keepus(treat)
*/

import excel "$basein\ptr_2015.10.23.xlsx", sheet("Sheet1") firstrow clear
drop if academyid==""
drop if grade=="STD 8"
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
egen MeanPTR=rowmean(ptr*)  
collapse (mean) MeanPTR, by(academyid)
save "$base_out\PTR_Mean.dta", replace


use "$base_out\DataCompleta_Often.dta", clear
merge 1:1 academyid using "$base_out\PTR_Mean.dta"
sum NonMissing,d
gen DropMissing=(NonMissing<r(p5))
sum MeanPTR,d
gen DropPTR=(MeanPTR<r(p5))

gen DropPTRMissing=min(DropPTR+DropMissing,1)
sort academyid
save "$base_out\DropPTRMissingYR2.dta", replace
save "C:\Users\Mauricio\Documents\git\XageT\Code\StataCode\RandYr2\DropPTRMissingYR2.dta", replace
