* Code to randomize GB Maths Mentoring
* Last updated: 12.15.2014

use "C:\Users\Mauricio\Dropbox\Learning Lab\XageT\RawData\GB Maths Mentoring Final School Randomization Dataset_12.15.2014.dta", clear


* restrict to just experiment schools

drop if gb_experiment==0
replace gb_experiment=1 if academyid=="Litein-KCO"

* Generate random school number
		set seed 25123
		gen s_random = runiform() 
		sort s_random
		
* Randomizing within province blocks

	* 1) sort by province & then by randomon school number
	
		sort province s_random

/* Province	Freq.	85 percent	T	C
				
Central		32	27.2	28	4
Coast		32	27.2	28	4
Eastern		16	13.6	14	2
Nairobi		21	17.85	18	3
Nyanza		36	30.6	31	5
Rift Valley	46	39.1	40	6
Western		24	20.4	21	3
				
Total		207			180	27
*/

	
	* 3) Assign treatment and control based on predetermined proportions
	
	*gen count within province (I confirmed this bysort does not resort the data)
	bysort province: gen count = _n
	
	gen treat=0
	
	replace treat=1 if province=="Central" & count<=28
	replace treat=1 if province=="Coast" & count<=28
	replace treat=1 if province=="Eastern" & count<=14
	replace treat=1 if province=="Nairobi" & count<=18
	replace treat=1 if province=="Nyanza" & count<=31
	replace treat=1 if province=="Rift Valley" & count<=40
	replace treat=1 if province=="Western" & count<=21
	
		
* check balance within provinces

	tab province treat, row
	
* converts cohort into a stata date format
	
	gen open_date = date(cohort,"DMY")
	
	foreach v of varlist ptr_total_active fee_tier avg_sm_stdscore open_date {
	
		reg `v' treat  
		areg `v' treat, absorb(province) 
		}
		
