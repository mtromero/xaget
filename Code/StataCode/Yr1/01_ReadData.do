import excel "$basein\ptr_2015.10.23.xlsx", sheet("Sheet1") cellrange(A1:E7542) firstrow clear
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
drop if academyid==""
compress
save "$base_out\PTR.dta", replace


import excel "$basein\ses_info_2015.10.27.xlsx", sheet("Sheet1") firstrow clear
replace pupil_dob="" if pupil_dob=="N/A"
gen DOB = date(pupil_dob, "MDY")
format DOB %td
gen DOB_M=mofd(DOB)
format DOB_M %tm

drop pupilname_T3ET14 pupilname_T1DG15
drop academyid_T1DG15
drop gender_T1DG15
rename academyid_T3ET14 academyid
rename gender_T3ET14 gender
replace gender="F" if gender=="F "
replace gender="M" if gender=="M "
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
compress
save "$base_out\StudentSES.dta", replace

use "$base_out\StudentSES.dta", clear
drop if academyid==""
collapse (count) pupilid, by( academyid gradename_T3ET14)
encode gradename_T3ET14, gen(grade2)
reshape wide pupilid gradename_T3ET14 , i( academyid) j( grade2)
gen TTR_T3ET14_C1=pupilid4/ pupilid9
gen TTR_T3ET14_PU=pupilid3/ pupilid8
drop gradename_T3ET141- pupilid11
save "$base_out\TTR_T3ET14.dta", replace

use "$base_out\StudentSES.dta", clear
drop if academyid==""
drop if gradename_T1DG15==""
collapse (count) pupilid, by( academyid gradename_T1DG15)
encode gradename_T1DG15, gen(grade2)
reshape wide pupilid gradename_T1DG15 , i( academyid) j( grade2)
gen TTR_T1DG15_C1=pupilid4/ pupilid9
gen TTR_T1DG15_PU=pupilid3/ pupilid8
drop gradename_T1DG151- pupilid12
save "$base_out\TTR_T1DG15.dta", replace


import delimited "$basein\ListAcademies.csv", bindquote(strict) stripquote(yes) clear 
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
compress 
save "$base_out\ListAcademies.dta", replace

import delimited "$basein\TreatmentStatus.csv", bindquote(strict) stripquote(yes) clear 
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
compress
save "$base_out\RandomizationXageT.dta", replace

use "$base_out\ListAcademies.dta", clear
merge 1:1 academyid using "$base_out\RandomizationXageT.dta", keepus(treat)
drop if _merge==1
drop _merge

replace launchdate = launchdate + td(30dec1899)
format launchdate %td

gen DaysOpen=mdy(1,1,2015)- launchdate 
tab wagecategory, gen(wagecat_D)
tab religion, gen(religion_D)

replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
save "$base_out\SchoolLeveLData.dta", replace




use "$basein\Fully_Linked_2015.dta",clear
drop *teach*
merge 1:1 pupilid using "$basein\Fully_Linked_2015_T3ET14.dta", keepus(*T3ET14)
drop if flag_T1DG15_T3ET15 > 0
drop _merge

drop score* maxscore*
drop academycohort* firstname* middlenam* lastname* enrolleddate* merge_gradename* cohort* assessmenttype* merge_grade* pupilname*
drop flag*
drop merge*
drop clustername

rename percentscore* percscore*
compress

rename *create* *creative*
rename *kiswahili* *swahili*
rename *ssre* *ss*


foreach var in  dev env lang math kuandika kusoma re read sci ss insha comp creative eng swahili {
	foreach year in  T3ET14 T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		foreach grade in BA c1 c2 c3 c4 c5 c6 c7 c8 PU NS ba pu ns{
		capture gen percscore_`var'_`year'=percscore_`var'_`grade'_`year'
		capture replace percscore_`var'_`year'=percscore_`var'_`grade'_`year' if percscore_`var'_`year'==.
		capture drop percscore_`var'_`grade'_`year'
		}
	}
}






foreach var of varlist * {    
    qui count if missing(`var')      
    if (r(N)/_N) == 1 drop `var'    
}

compress
save "$base_out\StudentLevel_Complete.dta", replace



use "$base_out\StudentLevel_Complete.dta", clear

foreach year in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
	foreach year2 in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
		replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
		replace academyid_`year2'="Mariakani-KLF" if academyid_`year2'=="Mariakani-MBA"
		replace academyid_`year2'="Ugunja-SYA" if academyid_`year2'=="Ugunja-KIS"
		drop if academyid_`year'!=academyid_`year2' &  academyid_`year'!="" &  academyid_`year2'!=""
		drop if gradename_`year'!=gradename_`year2' &  gradename_`year'!="" &  gradename_`year2'!=""
	}
}

foreach year in  T3MT15 T2ET15 T2MT15 T1ET15 T1MT15 T1DG15 T3ET14 {
	replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
	replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
	replace academyid_T3ET15=academyid_`year' if academyid_T3ET15=="" & academyid_`year'!=""
}

foreach year in  T3MT15 T2ET15 T2MT15 T1ET15 T1MT15 T1DG15{
replace gradename_T3ET15=gradename_`year' if gradename_T3ET15=="" & gradename_`year'!=""
}

replace gradename_T3ET15="Nursery" if gradename_T3ET15=="" & gradename_T3ET14=="Baby Class"
replace gradename_T3ET15="PREUNIT" if gradename_T3ET15=="" & gradename_T3ET14=="Nursery"
replace gradename_T3ET15="STD 1" if gradename_T3ET15=="" & gradename_T3ET14=="PREUNIT"
replace gradename_T3ET15="STD 2" if gradename_T3ET15=="" & gradename_T3ET14=="STD 1"
replace gradename_T3ET15="STD 3" if gradename_T3ET15=="" & gradename_T3ET14=="STD 2"
replace gradename_T3ET15="STD 4" if gradename_T3ET15=="" & gradename_T3ET14=="STD 3"
replace gradename_T3ET15="STD 5" if gradename_T3ET15=="" & gradename_T3ET14=="STD 4"
replace gradename_T3ET15="STD 6" if gradename_T3ET15=="" & gradename_T3ET14=="STD 5"
replace gradename_T3ET15="STD 7" if gradename_T3ET15=="" & gradename_T3ET14=="STD 6"
replace gradename_T3ET15="STD 8" if gradename_T3ET15=="" & gradename_T3ET14=="STD 7"


 

*keep if gradename_T1ET15=="PREUNIT" | gradename_T1ET15=="STD 1" | gradename_T1ET15=="STD 5" | gradename_T1ET15=="STD 6"
rename academyid_T3ET15 academyid
rename gradename_T3ET15 gradename
drop if academyid==""
drop if gradename==""

merge m:1 academyid using "$base_out\SchoolLeveLData.dta", keepus(county former_province constituency launchdate streamtype wagecategory mthly_fees_preunit mthly_fees_c1 mthly_fees_c5 mthly_fees_c6 treat DaysOpen wagecat_D1 wagecat_D2 wagecat_D3 religion_D1 religion_D2)
drop _merge
drop if treat==.
/*
merge m:1 academyid using "$base_out\SchoolLevel_T1DG15.dta"
drop _merge

merge m:1 academyid using "$base_out\SchoolLevel_T3ET14.dta"
drop _merge
compress
*/
*drop if percscore_math_T1ET15==. | percscore_math_T3ET14==.
encode constituency, gen(constituency2)
encode former_province, gen(former_province2)
encode county, gen(county2)
encode gradename, gen(gradename2)
*drop percscore_Dev_Dev_T1ET15 percscore_Lan_Lan_T1ET15 percscore_kusoma_T3ET14 percscore_kuandika_T3ET14 percscore_read_T3ET14


foreach grd in "Baby Class" "Nursery" "PREUNIT" "STD 1" "STD 2" "STD 3" "STD 4" "STD 5" "STD 6" "STD 7" "STD 8"{
foreach var of varlist percscore*T3ET15 percscore*T3MT15 percscore*T2ET15  percscore*T2MT15 percscore*T1ET15 percscore*T3ET14 percscore*T1MT15 percscore*T1DG15{
qui sum `var' if treat==0 & gradename=="`grd'",d
capture gen `var'_Z=(`var'-r(mean))/r(sd) if gradename=="`grd'"
capture replace `var'_Z=(`var'-r(mean))/r(sd) if gradename=="`grd'"

capture gen `var'_Z_LC=(`var'-r(mean))/r(sd) if gradename=="`grd'" & `var'!=0
capture replace `var'_Z_LC=(`var'-r(mean))/r(sd) if gradename=="`grd'" & `var'!=0

capture gen `var'_Z_RC=(`var'-r(mean))/r(sd) if gradename=="`grd'" & `var'!=100
capture replace `var'_Z_RC=(`var'-r(mean))/r(sd) if gradename=="`grd'" & `var'!=100
}
}


foreach var of varlist percscore*T3ET15 percscore*T3MT15 percscore*T2ET15 percscore*T2MT15 percscore*T1ET15 percscore*T3ET14 percscore*T1MT15 percscore*T1DG15{
gen `var'_Z2=`var'_Z*`var'_Z
gen `var'_Z3=`var'_Z*`var'_Z*`var'_Z
}



lookfor sci
foreach var of varlist `r(varlist)'{
label var `var' Science
}
lookfor _re_
foreach var of varlist `r(varlist)'{
label var `var' RE
}
lookfor math
foreach var of varlist `r(varlist)'{
label var `var' Math
}
lookfor creative
foreach var of varlist `r(varlist)'{
label var `var' Creative
}

lookfor env
foreach var of varlist `r(varlist)'{
label var `var' "Environmental"
}

lookfor dev
foreach var of varlist `r(varlist)'{
label var `var' "Development"
}

lookfor lang
foreach var of varlist `r(varlist)'{
label var `var' "Language"
}

lookfor read
foreach var of varlist `r(varlist)'{
label var `var' "Reading"
}
 
lookfor kusoma
foreach var of varlist `r(varlist)'{
label var `var' "Kusoma"
}

lookfor kuandika
foreach var of varlist `r(varlist)'{
label var `var' "Kuandika"
}

lookfor _re_
foreach var of varlist `r(varlist)'{
label var `var' "RE"
}

lookfor sci
foreach var of varlist `r(varlist)'{
label var `var' "Science"
}

lookfor _ss_
foreach var of varlist `r(varlist)'{
label var `var' "S.S."
}

lookfor env
foreach var of varlist `r(varlist)'{
label var `var' "Enviromental"
}

lookfor creative
foreach var of varlist `r(varlist)'{
label var `var' "Creative"
}

lookfor comp
foreach var of varlist `r(varlist)'{
label var `var' "Composition"
}

lookfor swahili
foreach var of varlist `r(varlist)'{
label var `var' "Swahili"
}

lookfor insha
foreach var of varlist `r(varlist)'{
label var `var' "Insha"
}


lookfor eng
foreach var of varlist `r(varlist)'{
label var `var' "English (DG)"
}
  
 
label var treat Treatment


pca percscore_env_T3ET14 percscore_math_T3ET14 if gradename=="PREUNIT", components(1) 
predict IndexBaseline_PU if gradename=="PREUNIT", score

pca  percscore_read_T3ET14 percscore_env_T3ET14 percscore_math_T3ET14 percscore_kusoma_T3ET14 if gradename=="STD 1", components(1) 
predict IndexBaseline_STD1 if gradename=="STD 1", score

pca  percscore_read_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_kusoma_T3ET14 if gradename=="STD 2", components(1) 
predict IndexBaseline_STD2 if gradename=="STD 2", score

pca  percscore_read_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_kusoma_T3ET14 if gradename=="STD 3", components(1) 
predict IndexBaseline_STD3 if gradename=="STD 3", score

pca  percscore_read_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_kusoma_T3ET14 if gradename=="STD 4", components(1) 
predict IndexBaseline_STD4 if gradename=="STD 4", score

pca  percscore_lang_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_swahili_T3ET14 if gradename=="STD 5", components(1) 
predict IndexBaseline_STD5 if gradename=="STD 5", score

pca  percscore_lang_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_swahili_T3ET14 if gradename=="STD 6", components(1) 
predict IndexBaseline_STD6 if gradename=="STD 6", score

pca  percscore_lang_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_swahili_T3ET14 if gradename=="STD 7", components(1) 
predict IndexBaseline_STD7 if gradename=="STD 7", score

pca  percscore_lang_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_swahili_T3ET14 if gradename=="STD 8", components(1) 
predict IndexBaseline_STD8 if gradename=="STD 8", score

****SAME FOR DIAGNOSIS


gen IndexDiagn_PU=0

pca  percscore_sci_T1DG15_Z percscore_ss_T1DG15_Z percscore_eng_T1DG15_Z if gradename=="STD 1", components(1) 
predict IndexDiagn_STD1 if gradename=="STD 1", score

pca  percscore_math_T1DG15_Z percscore_sci_T1DG15_Z percscore_ss_T1DG15_Z percscore_eng_T1DG15_Z percscore_swahili_T1DG15_Z  if gradename=="STD 5", components(1) 
predict IndexDiagn_STD5 if gradename=="STD 5", score

pca  percscore_math_T1DG15_Z percscore_sci_T1DG15_Z percscore_ss_T1DG15_Z percscore_eng_T1DG15_Z percscore_swahili_T1DG15_Z if gradename=="STD 6", components(1) 
predict IndexDiagn_STD6 if gradename=="STD 6", score



****SAME FOR DIAGNOSIS and BASELINE


pca percscore_env_T3ET14 percscore_math_T3ET14 if gradename=="PREUNIT", components(1) 
predict IndexDGET_PU if gradename=="PREUNIT", score

pca  percscore_read_T3ET14 percscore_env_T3ET14 percscore_math_T3ET14 percscore_kusoma_T3ET14 percscore_sci_T1DG15_Z percscore_ss_T1DG15_Z percscore_eng_T1DG15_Z if gradename=="STD 1", components(1) 
predict IndexDGET_STD1 if gradename=="STD 1", score

pca  percscore_lang_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_swahili_T3ET14 percscore_math_T1DG15_Z percscore_sci_T1DG15_Z percscore_ss_T1DG15_Z percscore_eng_T1DG15_Z percscore_swahili_T1DG15_Z if gradename=="STD 5", components(1) 
predict IndexDGET_STD5 if gradename=="STD 5", score

pca  percscore_lang_T3ET14 percscore_math_T3ET14 percscore_sci_T3ET14 percscore_re_T3ET14 percscore_ss_T3ET14  percscore_swahili_T3ET14 percscore_math_T1DG15_Z percscore_sci_T1DG15_Z percscore_ss_T1DG15_Z percscore_eng_T1DG15_Z percscore_swahili_T1DG15_Z if gradename=="STD 6", components(1) 
predict IndexDGET_STD6 if gradename=="STD 6", score




***SAME FOR ENDLINE
pca percscore_env_T3ET15  percscore_math_T3ET15  percscore_kusoma_T3ET15 percscore_read_T3ET15   if gradename=="PREUNIT", components(1) 
predict IndexEndline_PU if gradename=="PREUNIT", score

pca  percscore_math_T3ET15  percscore_kusoma_T3ET15 percscore_re_T3ET15 percscore_read_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15 if gradename=="STD 1", components(1) 
predict IndexEndline_STD1 if gradename=="STD 1", score

pca  percscore_math_T3ET15  percscore_kusoma_T3ET15 percscore_re_T3ET15 percscore_read_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15 if gradename=="STD 2", components(1) 
predict IndexEndline_STD2 if gradename=="STD 2", score

pca  percscore_math_T3ET15  percscore_kusoma_T3ET15 percscore_re_T3ET15 percscore_read_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15 if gradename=="STD 3", components(1) 
predict IndexEndline_STD3 if gradename=="STD 3", score

pca  percscore_lang_T3ET14 percscore_math_T3ET15 percscore_re_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15   percscore_swahili_T3ET15 if gradename=="STD 4", components(1) 
predict IndexEndline_STD4 if gradename=="STD 4", score

pca  percscore_lang_T3ET14 percscore_math_T3ET15 percscore_re_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15   percscore_swahili_T3ET15 if gradename=="STD 5", components(1) 
predict IndexEndline_STD5 if gradename=="STD 5", score

pca percscore_lang_T3ET15 percscore_math_T3ET15 percscore_re_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15   percscore_swahili_T3ET15 if gradename=="STD 6", components(1) 
predict IndexEndline_STD6 if gradename=="STD 6", score

pca percscore_lang_T3ET15 percscore_math_T3ET15 percscore_re_T3ET15 percscore_sci_T3ET15 percscore_ss_T3ET15   percscore_swahili_T3ET15 if gradename=="STD 7", components(1) 
predict IndexEndline_STD7 if gradename=="STD 7", score

/*for some reason no one upload STD8 data*/
pca percscore_lang_T3MT15 percscore_math_T3MT15 percscore_re_T3MT15 percscore_sci_T3MT15 percscore_ss_T3MT15   percscore_swahili_T3MT15 if gradename=="STD 8", components(1) 
predict IndexEndline_STD8 if gradename=="STD 8", score


foreach grd in "PREUNIT" "STD 1" "STD 2" "STD 3" "STD 4" "STD 5" "STD 6" "STD 7" "STD 8"{
foreach var of varlist IndexBaseline* IndexEndline*{
sum `var' if treat==0 & gradename=="`grd'",d
capture replace `var'=(`var'-r(mean))/r(sd) if gradename=="`grd'"
}
}



egen IndexT3ET14=rowtotal(IndexBaseline_PU-IndexBaseline_STD8), missing
drop IndexBaseline_PU-IndexBaseline_STD8

egen IndexT1DG15=rowtotal(IndexDiagn_PU-IndexDiagn_STD6), missing
drop IndexDiagn_PU-IndexDiagn_STD6

egen IndexET14DG15=rowtotal(IndexDGET_PU-IndexDGET_STD6), missing
drop IndexDGET_PU-IndexDGET_STD6

egen IndexEndline=rowtotal(IndexEndline_PU-IndexEndline_STD8), missing
drop IndexEndline_PU-IndexEndline_STD8


*T3ET14 T1DG15 T1MT15 ET14DG15

label var IndexT3ET14 "Baseline Score (Index)"
label var IndexET14DG15 "Baseline Score (Index)"
label var IndexT1DG15 "Baseline Score (Index)"
label var IndexEndline "Endline Score (Index)"


rename percscore_* *

gen dSTD1=(gradename=="STD 1")
gen dSTD2=(gradename=="STD 2")
gen dSTD3=(gradename=="STD 3")
gen dSTD4=(gradename=="STD 4")
gen dSTD7=(gradename=="STD 7")
gen dPU=(gradename=="PREUNIT")
gen dSTD5=(gradename=="STD 5")
gen dSTD6=(gradename=="STD 6")

label var dSTD1 "STD 1"
label var dSTD5 "STD 5"
label var dSTD6 "STD 6"
label var dPU "PU"

/*
local controlspu_T3ET14 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_T3ET14 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_T3ET14 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_T3ET14 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
*/
local controlspu_ET14DG15 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_ET14DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3

/*
local controlspu_T1DG15 
local controlsc1_T1DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3
local controlsc5_T1DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3
local controlsc6_T1DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3

local controlspu_T1MT15 read_T1MT15_Z kusoma_T1MT15_Z env_T1MT15_Z math_T1MT15_Z read_T1MT15_Z2 kusoma_T1MT15_Z2 env_T1MT15_Z2 math_T1MT15_Z2 read_T1MT15_Z3 kusoma_T1MT15_Z3 env_T1MT15_Z3 math_T1MT15_Z3
local controlsc1_T1MT15 math_T1MT15_Z read_T1MT15_Z  kusoma_T1MT15_Z re_T1MT15_Z sci_T1MT15_Z ss_T1MT15_Z math_T1MT15_Z2 read_T1MT15_Z2  kusoma_T1MT15_Z2 re_T1MT15_Z2 sci_T1MT15_Z2 ss_T1MT15_Z2 math_T1MT15_Z3 read_T1MT15_Z3  kusoma_T1MT15_Z3 re_T1MT15_Z3 sci_T1MT15_Z3 ss_T1MT15_Z3
local controlsc5_T1MT15 lang_T1MT15_Z math_T1MT15_Z sci_T1MT15_Z re_T1MT15_Z ss_T1MT15_Z  swahili_T1MT15_Z lang_T1MT15_Z2 math_T1MT15_Z2 sci_T1MT15_Z2 re_T1MT15_Z2 ss_T1MT15_Z2 swahili_T1MT15_Z2 lang_T1MT15_Z3 math_T1MT15_Z3 sci_T1MT15_Z3 re_T1MT15_Z3 ss_T1MT15_Z3  swahili_T1MT15_Z3
local controlsc6_T1MT15 lang_T1MT15_Z math_T1MT15_Z sci_T1MT15_Z re_T1MT15_Z ss_T1MT15_Z  swahili_T1MT15_Z lang_T1MT15_Z2 math_T1MT15_Z2 sci_T1MT15_Z2 re_T1MT15_Z2 ss_T1MT15_Z2 swahili_T1MT15_Z2 lang_T1MT15_Z3 math_T1MT15_Z3 sci_T1MT15_Z3 re_T1MT15_Z3 ss_T1MT15_Z3  swahili_T1MT15_Z3
*/

foreach subject in math env{
	foreach Period in T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		gen `subject'_`Period'_Delta=`subject'_`Period'_Z-`subject'_T3ET14_Z if dPU==1
	}
}

foreach Period in T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
	replace math_`Period'_Delta=math_`Period'_Z-math_T3ET14_Z if dSTD1==1
	gen read_`Period'_Delta=read_`Period'_Z-read_T3ET14_Z if dSTD1==1
	gen kusoma_`Period'_Delta=kusoma_`Period'_Z-kusoma_T3ET14_Z if dSTD1==1
	gen sci_`Period'_Delta=sci_`Period'_Z-sci_T3ET14_Z if dSTD1==1
	gen ss_`Period'_Delta=ss_`Period'_Z-ss_T3ET14_Z if dSTD1==1
	gen re_`Period'_Delta=re_`Period'_Z-re_T3ET14_Z if dSTD1==1
	
}

foreach subject in math lang swahili  re sci ss{
	foreach Period in T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		capture gen `subject'_`Period'_Delta=`subject'_`Period'_Z-`subject'_T3ET14_Z if dSTD5==1 | dSTD6==1
		capture replace `subject'_`Period'_Delta=`subject'_`Period'_Z-`subject'_T3ET14_Z if dSTD5==1 | dSTD6==1
	}
}



*T3ET14 T1DG15
foreach baseline in ET14DG15{
	foreach var in `controlspu_`baseline''{
		capture gen zo_`var'_`baseline'_pu=`var'
		capture replace zo_`var'_`baseline'_pu=0 if zo_`var'_`baseline'_pu==. & dSTD1==1
	}
}

foreach baseline in ET14DG15{
	foreach var in `controlsc1_`baseline''{
		capture gen zo_`var'_`baseline'_c1=`var'
		capture replace zo_`var'_`baseline'_c1=0 if zo_`var'_`baseline'_c1==. & dPU==1
	}
}

foreach baseline in  ET14DG15{
	foreach var in `controlsc5_`baseline''{
		capture gen zo_`var'_`baseline'_c5=`var'
		capture replace zo_`var'_`baseline'_c5=0 if zo_`var'_`baseline'_c5==. & dSTD5==1
	}
}

foreach baseline in  ET14DG15{
	foreach var in `controlsc6_`baseline''{
		capture gen zo_`var'_`baseline'_c6=`var'
		capture replace zo_`var'_`baseline'_c6=0 if zo_`var'_`baseline'_c6==. & dSTD6==1
	}
}




compress
save "$base_out\2015t1_FullyLinked_mainTE_own.dta", replace

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear
merge 1:1 pupilid using "$base_out\StudentSES.dta", keepus(DOB_M gender status_T3ET14 status_T1DG15)
drop if _merge==2
drop _merge

label var DOB_M "Month of birth"

gen age=ym(2015,1)-DOB_M
replace age=age/12
label var age "Age"


label var gender Gender
gen male=(gender=="M") if !missing(gender)
label var male Male
 
save "$base_out\2015t1_FullyLinked_mainTE_own.dta", replace


use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear
collapse (mean) *_T3ET14_Z *_T1MT15_Z *_T1ET15_Z *_T2MT15_Z *_T2ET15_Z *_T3MT15_Z *_T3ET15_Z, by(academyid gradename)
rename *_Z *
foreach x of var dev_T3ET14- swahili_T3ET15 { 
	rename `x' mean_`x' 
}
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var mean_dev_T3ET14- mean_swahili_T3ET15 {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\Mean`grade'.dta", replace
restore
}

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear


collapse (median) *_T3ET14_Z *_T1MT15_Z *_T1ET15_Z *_T2MT15_Z *_T2ET15_Z *_T3MT15_Z *_T3ET15_Z, by(academyid gradename)
rename *_Z *
foreach x of var dev_T3ET14- swahili_T3ET15 { 
	rename `x' median_`x' 
}
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var median_dev_T3ET14- median_swahili_T3ET15 {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\Median`grade'.dta", replace
restore
}

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

collapse (p80) *_T3ET14_Z *_T1MT15_Z *_T1ET15_Z *_T2MT15_Z *_T2ET15_Z *_T3MT15_Z *_T3ET15_Z, by(academyid gradename)
rename *_Z *
foreach x of var dev_T3ET14- swahili_T3ET15 { 
	rename `x' p80_`x' 
}
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var p80_dev_T3ET14- p80_swahili_T3ET15 {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\p80`grade'.dta", replace
restore
}


use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

collapse (p20) *_T3ET14_Z *_T1MT15_Z *_T1ET15_Z *_T2MT15_Z *_T2ET15_Z *_T3MT15_Z *_T3ET15_Z, by(academyid gradename)
rename *_Z *
foreach x of var dev_T3ET14- swahili_T3ET15 { 
	rename `x' p20_`x' 
}
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var p20_dev_T3ET14- p20_swahili_T3ET15 {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\p20`grade'.dta", replace
restore
}

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear


collapse (sd) *_T3ET14_Z *_T1MT15_Z *_T1ET15_Z *_T2MT15_Z *_T2ET15_Z *_T3MT15_Z *_T3ET15_Z, by(academyid gradename)
rename *_Z *
foreach x of var dev_T3ET14- swahili_T3ET15 { 
	rename `x' sd_`x' 
}
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var sd_dev_T3ET14- sd_swahili_T3ET15 {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\Deviaton`grade'.dta", replace
restore
}

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear


collapse (mean) male , by(academyid gradename)
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var male {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\Male`grade'.dta", replace
restore
}


use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear


collapse (mean) age , by(academyid gradename)
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var age {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\Age`grade'.dta", replace
restore
}


use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear


/*
collapse (iqr) *_T3ET14_Z *_T1MT15_Z *_T1ET15_Z *_T2MT15_Z *_T2ET15_Z, by(academyid gradename)
rename *_Z *
foreach x of var dev_T3ET14- swahili_T2ET15 { 
	rename `x' iqr_`x' 
}
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
preserve
drop if gradename!="`grade'"
foreach x of var iqr_dev_T3ET14- iqr_swahili_T2ET15 {
	if (gradename=="PREUNIT") rename `x' `x'_pu
	if (gradename=="STD 1") rename `x' `x'_c1
	if (gradename=="STD 5") rename `x' `x'_c5
	if (gradename=="STD 6") rename `x' `x'_c6
} 
drop gradename
compress
save "$base_out\IQR`grade'.dta", replace
restore
}
*/

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear
foreach grade in "PREUNIT" "STD 1" "STD 5" "STD 6"{
merge m:1 academyid using "$base_out\Mean`grade'.dta"
drop _merge
merge m:1 academyid using "$base_out\Median`grade'.dta"
drop _merge
merge m:1 academyid using "$base_out\p80`grade'.dta"
drop _merge
merge m:1 academyid using "$base_out\p20`grade'.dta"
drop _merge
merge m:1 academyid using "$base_out\Deviaton`grade'.dta"
drop _merge
merge m:1 academyid using "$base_out\Male`grade'.dta"
drop _merge
merge m:1 academyid using "$base_out\Age`grade'.dta"
drop _merge
/*
merge m:1 academyid using "$base_out\IQR`grade'.dta"
drop _merge
*/
}

compress
save "$base_out\2015t1_FullyLinked_mainTE_own.dta", replace




use "$base_out\PTR.dta", clear
rename grade gradename_T3ET14
rename stream stream_T3ET14 
rename academyid academyid_T3ET14
drop ptr_T1DG15
collapse (mean) ptr_T3ET14, by(academyid_T3ET14 gradename_T3ET14)
merge 1:m academyid_T3ET14 gradename_T3ET14 using "$base_out\2015t1_FullyLinked_mainTE_own.dta"
drop if _merge==1
drop _merge
save "$base_out\2015t1_FullyLinked_mainTE_own.dta", replace

use "$base_out\PTR.dta", clear
rename grade gradename
rename stream stream_T1DG15
drop ptr_T3ET14
collapse (mean) ptr_T1DG15, by(academyid gradename)
merge 1:m academyid gradename using "$base_out\2015t1_FullyLinked_mainTE_own.dta"
drop if _merge==1
drop _merge

label var ptr_T1DG15 "PTR T1DG15"
label var ptr_T3ET14 "PTR T3ET14"
save "$base_out\2015t1_FullyLinked_mainTE_own.dta", replace





merge m:1 academyid using "$base_out\TTR_T3ET14.dta"
drop if _merge==2
drop _merge

merge m:1 academyid using "$base_out\TTR_T1DG15.dta"
drop if _merge==2
drop _merge

compress
save "$base_out\2015t1_FullyLinked_mainTE_own.dta", replace

