set more off
clear all

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 se_1 se_2 d_p
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
 reg `var' TD1 TD2 i.`strat_id' `if', nocons vce(cluster `clus_id')
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p'  = nullmat(`d_p'),r(p)
 matrix A=e(b)
 matrix B=e(V)
 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
 sum `var' if TD1==1 & e(sample)==1
 mat `mu_1' = nullmat(`mu_1'), round(r(mean),0.01)
 mat `se_1' = nullmat(`se_1'), r(sd)
 sum `var' if TD2==1 & e(sample)==1
 mat `mu_2' = nullmat(`mu_2'),round(r(mean),0.01)
 mat `se_2' = nullmat(`se_2'), r(sd)
}
foreach mat in mu_1 mu_2 mu_3  se_1 se_2 d_p {
 mat coln ``mat'' = `varlist'
}
 local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3  se_1 se_2  d_p {
 eret mat `mat' = ``mat''
}
end

use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

twoway (kdensity age if gradename=="PREUNIT", bwidth(0.3)) (kdensity age if gradename=="STD 1", bwidth(0.3)) ///
(kdensity age if gradename=="STD 5", bwidth(0.3)) (kdensity age if gradename=="STD 6", bwidth(0.3)), legend(order(1 "Pre-Unit" 2 "STD 1" 3 "STD 5" 4 "STD 6")) ///
title("Age distribution across grades") xtitle("Age") ytitle("Density")
graph export "$results\Graphs\AgeDistributionGrades.pdf", as(pdf) replace


*drop if academyid=="Emali-MUE" /* its the only control school in the easter province, so there is no variance here... */
reg math_T3ET14_Z treat  if  gradename=="PREUNIT", vce(cluster academyid)
encode academyid, gen(academyid2)

****************
****************
*** FIRST LETS TEST IF THE ENDLINE SCORE IS DIFFERENT FOR THOSE WITH AND WITHOUT BASELINE. 
****************
****************
gen baseline=!missing(IndexET14DG15)

*FIRST, A GRAPHIC ANALYSIS
/*
twoway (kdensity IndexEndline if treat==0 & IndexET14DG15!=., lcolor(blue) lpattern(solid) legend(lab(1 "Control: Baseline"))) ///
(kdensity IndexEndline if treat==0 & IndexET14DG15==.,lcolor(blue) lpattern(longdash) legend(lab(2 "Control: No baseline"))) ///
(kdensity IndexEndline if treat==1 & IndexET14DG15!=., lcolor(red) lpattern(solid) legend(lab(3 "Treatment: Baseline"))) ///
(kdensity IndexEndline if treat==1 & IndexET14DG15==.,lcolor(red) lpattern(longdash) legend(lab(4 "Treatment: No baseline"))) ///
, xtitle(Endline Score (PCA)) ytitle(Density) legend(order(1 2 3 4)) ///
title("Who has a baseline?", size(vsmall))
graph export "$results\Graphs\WhoBaseline_AllGrades.pdf", as(pdf) replace

foreach grd in "PREUNIT" "STD 1" "STD 2" "STD 3" "STD 4" "STD 5" "STD 6" "STD 7" "STD 8"{
twoway (kdensity IndexEndline if treat==0 & IndexET14DG15!=. & gradename=="`grd'", lcolor(blue) lpattern(solid) legend(lab(1 "Control: Baseline"))) ///
(kdensity IndexEndline if treat==0 & IndexET14DG15==. & gradename=="`grd'",lcolor(blue) lpattern(longdash) legend(lab(2 "Control: No baseline"))) ///
(kdensity IndexEndline if treat==1 & IndexET14DG15!=. & gradename=="`grd'", lcolor(red) lpattern(solid) legend(lab(3 "Treatment: Baseline"))) ///
(kdensity IndexEndline if treat==1 & IndexET14DG15==. & gradename=="`grd'",lcolor(red) lpattern(longdash) legend(lab(4 "Treatment: No baseline"))) ///
, xtitle(Endline Score (PCA)) ytitle(Density) legend(order(1 2 3 4)) ///
title("Who has a baseline?", size(vsmall))
graph export "$results\Graphs\WhoBaseline_Grade`grd'.pdf", as(pdf) replace
}

*NOW, A regression ANALYSIS

eststo clear
foreach grd in "PREUNIT" "STD 1" "STD 5" "STD 6"{
eststo: reg baseline treat i.former_province2 if gradename=="`grd'", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)
}
eststo: reg baseline treat i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" , vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg baseline treat i.former_province2 if  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg baseline treat i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" |  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)


esttab  using "$results\LatexCode\WhoBaseline_Quantity.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
indicate(  "Former Province F.E.= *former_province2" ) ///
mtitle("PREUNIT" "STD 1" "STD 5" "STD 6"  "Pupils" "Tutors""All") ///
star(* 0.10 ** 0.05 *** 0.01) ///
replace /// 
keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

*********************************************************************************
*Now lets see if its different by baseline
*********************************************************************************

eststo clear
foreach grd in "PREUNIT" "STD 1" "STD 5" "STD 6" {
eststo: reg baseline c.treat##c.IndexEndline i.former_province2 if gradename=="`grd'", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)
}
eststo: reg baseline c.treat##c.IndexEndline i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" , vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg baseline c.treat##c.IndexEndline i.former_province2 if  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg baseline c.treat##c.IndexEndline i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" |  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)


esttab  using "$results\LatexCode\WhoBaseline_Quantity_Endline.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
indicate(  "Former Province F.E.= *former_province2" ) ///
mtitle("PREUNIT" "STD 1" "STD 5" "STD 6" "Pupils" "Tutors" "All") ///
star(* 0.10 ** 0.05 *** 0.01) ///
replace /// 
keep(treat IndexEndline c.treat#c.IndexEndline) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



****************
****************
*** NOW LETS TEST FOR BALANCE AT BASELINE
****************
****************

**Picture/graphic analysis of this balance

twoway (kdensity IndexET14DG15 if treat==0, lcolor(blue) lpattern(solid) legend(lab(1 "Control"))) ///
(kdensity IndexET14DG15 if treat==1,lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
, xtitle(Baseline Score (PCA)) ytitle(Density) legend(order(1 2)) ///
title("Balance of baseline scores", size(vsmall))
graph export "$results\Graphs\Balance_AllGrades.pdf", as(pdf) replace




foreach grd in "PREUNIT" "STD 1" "STD 2" "STD 3" "STD 4" "STD 5" "STD 6" "STD 7" "STD 8"{
twoway (kdensity IndexET14DG15 if treat==0 & gradename=="`grd'", lcolor(blue) lpattern(solid) legend(lab(1 "Control"))) ///
(kdensity IndexET14DG15 if treat==1 & gradename=="`grd'",lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
, xtitle(Baseline Score (PCA)) ytitle(Density) legend(order(1 2)) ///
title("Balance of baseline scores for `grd'", size(vsmall))
graph export "$results\Graphs\Balance_Grade_`grd'.pdf", as(pdf) replace
}
*/

foreach period in T3ET14 T1DG15{
capture drop Tested`period'
gen Tested`period'=!missing(math_`period'_Z)
label var Tested`period' "Tested in `period'"
}

foreach period in   T1MT15 T1ET15 T2MT15 T2ET15 T3MT15{
capture drop Tested`period'
gen Tested`period'=!missing(math_`period'_Z) if !missing(math_T3ET14_Z)
label var Tested`period' "Tested in `period' (conditional on T3ET14)"
}

**Regresion analysis of this balance
eststo clear
xi: my_ptest   math_T3ET14_Z read_T3ET14_Z  kusoma_T3ET14_Z env_T3ET14_Z male age TestedT3ET14- TestedT3MT15 if gradename=="STD 1", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C1.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest    math_T3ET14_Z  env_T3ET14_Z  male age TestedT3ET14- TestedT3MT15  if gradename=="PREUNIT", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_PU.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest   math_T3ET14_Z  male age TestedT3ET14- TestedT3MT15  if gradename=="PREUNIT" |  gradename=="STD 1", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_PUC1.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 


eststo clear
xi: my_ptest  math_T3ET14_Z read_T3ET14_Z  kusoma_T3ET14_Z male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 2", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C2.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z read_T3ET14_Z  kusoma_T3ET14_Z male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 3", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C3.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z read_T3ET14_Z  kusoma_T3ET14_Z male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 4", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C4.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z lang_T3ET14_Z  swahili_T3ET14_Z  sci_T3ET14_Z  ss_T3ET14_Z  male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 5", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C5.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z lang_T3ET14_Z  swahili_T3ET14_Z  sci_T3ET14_Z  ss_T3ET14_Z  male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 6", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C6.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z lang_T3ET14_Z  swahili_T3ET14_Z  sci_T3ET14_Z  ss_T3ET14_Z  male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 7", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C7.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z lang_T3ET14_Z  swahili_T3ET14_Z  sci_T3ET14_Z  ss_T3ET14_Z  male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 8", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C8.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

eststo clear
xi: my_ptest  math_T3ET14_Z lang_T3ET14_Z  swahili_T3ET14_Z  sci_T3ET14_Z  ss_T3ET14_Z  male age  TestedT3ET14- TestedT3MT15  if gradename=="STD 5" |  gradename=="STD 6", by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\Balance_C5C6.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(%12.2fc)) mu_2(fmt(%12.2fc)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

/*
****************
****************
*** Now LETS TEST FOR DIFFERENTIAL ATTRITION... I.E. ARE THE STUDENTS WITH END-SCORES DIFFERENT? OR THE STUDENTS WITH BASELINE SCORES DIFFERENT?
****************
****************

*FIRST, A GRAPHIC ANALYSIS

twoway (kdensity IndexET14DG15 if treat==0 & IndexEndline!=., lcolor(blue) lpattern(solid) legend(lab(1 "Control: Non-Attrited"))) ///
(kdensity IndexET14DG15 if treat==0 & IndexEndline==.,lcolor(blue) lpattern(longdash) legend(lab(2 "Control: Attrited"))) ///
(kdensity IndexET14DG15 if treat==1 & IndexEndline!=., lcolor(red) lpattern(solid) legend(lab(3 "Treatment: Non-Attrited"))) ///
(kdensity IndexET14DG15 if treat==1 & IndexEndline==.,lcolor(red) lpattern(longdash) legend(lab(4 "Treatment: Attrited"))) ///
, xtitle(Baseline Score (PCA)) ytitle(Density) legend(order(1 2 3 4)) ///
title("Differential attrition", size(vsmall))
graph export "$results\Graphs\Attrition_AllGrades.pdf", as(pdf) replace

foreach grd in "PREUNIT" "STD 1" "STD 2" "STD 3" "STD 4" "STD 5" "STD 6" "STD 7" "STD 8"{
twoway (kdensity IndexET14DG15 if treat==0 & IndexEndline!=. & gradename=="`grd'", lcolor(blue) lpattern(solid) legend(lab(1 "Control: Non-Attrited"))) ///
(kdensity IndexET14DG15 if treat==0 & IndexEndline==. & gradename=="`grd'",lcolor(blue) lpattern(longdash) legend(lab(2 "Control: Attrited"))) ///
(kdensity IndexET14DG15 if treat==1 & IndexEndline!=. & gradename=="`grd'", lcolor(red) lpattern(solid) legend(lab(3 "Treatment: Non-Attrited"))) ///
(kdensity IndexET14DG15 if treat==1 & IndexEndline==. & gradename=="`grd'",lcolor(red) lpattern(longdash) legend(lab(4 "Treatment: Attrited"))) ///
, xtitle(Baseline Score (PCA)) ytitle(Density) legend(order(1 2 3 4)) ///
title("Differential attrition", size(vsmall))
graph export "$results\Graphs\Attrition_Grade_`grd'.pdf", as(pdf) replace
}


*NOW LETS DO A REGRESSION ANALYSIS: THE QUESTION IS, IS THERE ANY DIFFERENTIAL ATTRITION IN TREATMENT AND CONTROL SCHOOLS? IS THE QUANTITY DIFFERENT?
gen Attrion=missing(IndexEndline)

eststo clear
foreach grd in "PREUNIT" "STD 1" "STD 5" "STD 6"{
eststo: reg Attrion treat i.former_province2 if gradename=="`grd'", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)
}
eststo: reg Attrion treat i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" , vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg Attrion treat i.former_province2 if  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg Attrion treat i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" |  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)



esttab  using "$results\LatexCode\Attrition_Quantity.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
indicate(  "Former Province F.E.= *former_province2" ) ///
mtitle("PREUNIT" "STD 1" "STD 5" "STD 6"  "Pupils" "Tutors""All") ///
star(* 0.10 ** 0.05 *** 0.01) ///
replace /// 
keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

*********************************************************************************
*Now lets see if its different by baseline
*********************************************************************************

eststo clear
foreach grd in "PREUNIT" "STD 1" "STD 5" "STD 6" {
eststo: reg Attrion c.treat##c.IndexET14DG15 i.former_province2 if gradename=="`grd'", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)
}
eststo: reg Attrion c.treat##c.IndexET14DG15 i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" , vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg Attrion c.treat##c.IndexET14DG15 i.former_province2 if  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)

eststo: reg Attrion c.treat##c.IndexET14DG15 i.former_province2 if  gradename=="PREUNIT" |  gradename=="STD 1" |  gradename=="STD 5" |  gradename=="STD 6", vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)


esttab  using "$results\LatexCode\Attrition_Quantity_Baseline.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
indicate(  "Former Province F.E.= *former_province2" ) ///
mtitle("PREUNIT" "STD 1" "STD 5" "STD 6" "Pupils" "Tutors" "All") ///
star(* 0.10 ** 0.05 *** 0.01) ///
replace /// 
keep(treat IndexET14DG15 c.treat#c.IndexET14DG15) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

