set matsize 1000, perm
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M


local controlspu_ET14DG15 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_ET14DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3

gen mean_tutor=mean_math_T3ET14_c5 if gradename=="PREUNIT"
replace mean_tutor=mean_math_T3ET14_c6 if gradename=="STD 1"

gen median_tutor=median_math_T3ET14_c5 if gradename=="PREUNIT"
replace median_tutor=median_math_T3ET14_c6 if gradename=="STD 1"


gen p80_tutor=p80_math_T3ET14_c5 if gradename=="PREUNIT"
replace p80_tutor=p80_math_T3ET14_c6 if gradename=="STD 1"


gen p20_tutor=p20_math_T3ET14_c5 if gradename=="PREUNIT"
replace p20_tutor=p20_math_T3ET14_c6 if gradename=="STD 1"


gen sd_tutor=sd_math_T3ET14_c5 if gradename=="PREUNIT"
replace sd_tutor=sd_math_T3ET14_c6 if gradename=="STD 1"
 
gen male_tutor=male_c5 if gradename=="PREUNIT"
replace male_tutor=male_c6 if gradename=="STD 1"

gen TTR=TTR_T1DG15_PU if gradename=="PREUNIT"
replace TTR=TTR_T1DG15_C1 if gradename=="STD 1"

 
gen sd_total=sd_math_T3ET14_pu*sd_math_T3ET14_c5 if gradename=="PREUNIT"
replace sd_total=sd_math_T3ET14_c1*sd_math_T3ET14_c6 if gradename=="STD 1"

keep TTR *_tutor sd_total `controls2' gradename gradename2 dSTD1 dPU dSTD5 dSTD6 academyid former_province2 *_Z zo* treat pupilid

reshape long dev_@_Z env_@_Z lang_@_Z math_@_Z kusoma_@_Z re_@_Z read_@_Z sci_@_Z ss_@_Z swahili_@_Z, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")

label var dev__Z Development
label var env__Z Enviromental
label var lang__Z Language
label var math__Z Math
label var kusoma__Z Kusoma
label var re__Z RE
label var read__Z Reading
label var sci__Z Science
label var ss__Z "S.S."
label var swahili__Z Swahili
drop kuandika_T2ET15_Z- creative_T1ET15_Z kuandika_T3ET14_Z insha_T3ET14_Z- creative_T3ET14_Z kuandika_T1MT15_Z- eng_T1DG15_Z

gen zo_math_T3ET14_Z_ET14DG15=zo_math_T3ET14_Z_ET14DG15_pu if dPU==1
replace zo_math_T3ET14_Z_ET14DG15=zo_math_T3ET14_Z_ET14DG15_c1 if dSTD1==1

label var zo_math_T3ET14_Z_ET14DG15 "Lagged math score"



 
			
			************************************************************
			*******************     PREUNIT AND STD 1   ****************
			************************************************************
	
				***This is a pass over every subjects with the plain specification ***
				foreach var_hetero in male age dSTD1 {
					eststo clear
					foreach subject in math read kusoma {
						eststo: reg `subject'__Z c.treat##c.`var_hetero' (i.gradename2 i.former_province2)##c.(term1 term2 term3 term4 term5)  if  gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
						estadd ysumm
						qui tab academyid if e(sample)==1 & treat==0
						estadd scalar controls=r(r)
						estadd local ProvinceFE "Yes"
						estadd local Studentcontrols "No"
						estadd local Schoolscontrols "No"
						
					}
					
					esttab  using "$results\LatexCode\HeteroTutor_`var_hetero'_Pooled_Base_ET14DG15_all_PU_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
					mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
					star(* 0.10 ** 0.05 *** 0.01) ///
					rename(male 
					replace keep(treat `var_hetero' c.treat#*) stats(N N_clust controls  , labels("N. of obs." "Number of schools" "Control schools" )) ///
					nonotes \sym{***} \(p<0.01\) }")
				}

				***This is a pass wtih lagged test scores ***
				foreach var_hetero in mean_tutor sd_tutor median_tutor p80_tutor male male_tutor TTR zo_math_T3ET14_Z_ET14DG15{
					eststo clear
					foreach subject in math read kusoma   {
						eststo: reg `subject'__Z c.treat##c.`var_hetero' (i.gradename2 c.(zo*ET14DG15_pu)#dPU c.(zo*ET14DG15_c1)#dSTD1 i.former_province2)##c.(term1 term2 term3 term4 term5)  if  (gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
						estadd ysumm
						qui tab academyid if e(sample)==1 & treat==0
						estadd scalar controls=r(r)
						estadd local ProvinceFE "Yes"
						estadd local Studentcontrols "Yes"
						estadd local Schoolscontrols "No"
					}
					
					esttab  using "$results\LatexCode\HeteroTutor_`var_hetero'_Pooled_Base_ET14DG15_all_PU_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
					mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
					star(* 0.10 ** 0.05 *** 0.01) ///
					replace keep(treat `var_hetero' c.treat#*) stats(N N_clust controls  , labels("N. of obs." "Number of schools" "Control schools" )) ///
					nonotes \sym{***} \(p<0.01\) }")
				}
				
				***This is a pass wtih lagged test scores and school controls ***
				foreach var_hetero in mean_tutor sd_tutor median_tutor p80_tutor male male_tutor TTR zo_math_T3ET14_Z_ET14DG15{
					eststo clear
					foreach subject in math read kusoma   {
						eststo: reg `subject'__Z c.treat##c.`var_hetero' (i.gradename2 c.(zo*ET14DG15_pu)#dPU c.(zo*ET14DG15_c1)#dSTD1 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5) if ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
						estadd ysumm
						qui tab academyid if e(sample)==1 & treat==0
						estadd scalar controls=r(r)
						estadd local ProvinceFE "Yes"
						estadd local Studentcontrols "Yes"
						estadd local Schoolscontrols "Yes"
					}
					
					esttab  using "$results\LatexCode\HeteroTutor_`var_hetero'_Pooled_Base_ET14DG15_all_PU_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
					mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
					star(* 0.10 ** 0.05 *** 0.01) ///
					replace keep(treat `var_hetero' c.treat#*) stats(N N_clust controls  , labels("N. of obs." "Number of schools" "Control schools" )) ///
					nonotes \sym{***} \(p<0.01\) }")
				}


			
			
			
		
