set matsize 1000, perm
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M


keep `controls2' gradename gradename2 dPU dNU dBC dc1 dc2 dc3 dc4 dc5 dc6 dc7 academyid former_province2 *_Z zo* treat pupilid IndexET14DG15

reshape long dev_@_Z env_@_Z lang_@_Z math_@_Z kusoma_@_Z re_@_Z read_@_Z sci_@_Z ss_@_Z swahili_@_Z, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")
gen term6=(term=="T3ET15")

label var dev__Z Development
label var env__Z Enviromental
label var lang__Z Language
label var math__Z Math
label var kusoma__Z Kusoma
label var re__Z RE
label var read__Z Reading
label var sci__Z Science
label var ss__Z "S.S."
label var swahili__Z Swahili
drop kuandika_T2ET15_Z- creative_T1ET15_Z kuandika_T3ET14_Z insha_T3ET14_Z- creative_T3ET14_Z kuandika_T1MT15_Z- eng_T1DG15_Z

foreach baseline in   ET14DG15{
		/*
			**************************************************
			*******************     PREUNIT   ****************
			**************************************************
			
			***This is a pass over all subjects to use different specifications ***
			
			foreach subject in math read kusoma env{

				eststo clear
				
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				
				eststo: reg `subject'__Z treat (c.zo*`baseline'_pu i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				eststo: reg `subject'__Z treat (`controls' c.zo*`baseline'_pu i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_PU.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "`baseline' score = zo*`baseline'_pu"  "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes

			} /*Subjects that PU is evaluated on */
			
			***This is a pass over each subjects to use the simplest specification ***

			eststo clear
			foreach subject in math read kusoma env{
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes
			
			***This is a pass over each subjects to use with lagged test scores***

			eststo clear
			foreach subject in math read kusoma env{
				eststo: reg `subject'__Z treat (c.zo*`baseline'_pu i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  "`baseline' score = zo*`baseline'_pu"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			***This is a pass over each subjects to use with lagged test scores and school controls***

			eststo clear
			foreach subject in math read kusoma env{
				eststo: reg `subject'__Z treat (`controls' c.zo*`baseline'_pu i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r) 
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  "`baseline' score = zo*`baseline'_pu" "School controls = mthly_fees_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			**************************************************
			*******************     STD 1     ****************
			**************************************************
			
			***This is a pass over all subjects to use different specifications ***

			foreach subject in math read kusoma sci ss re{

				eststo clear
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

			
				eststo: reg `subject'__Z treat (c.zo*`baseline'_c1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				eststo: reg `subject'__Z treat (`controls' c.zo*`baseline'_c1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "Former Province F.E.= *former_province2"  "`baseline' score = zo*`baseline'_c1" "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes

			} /*Subjects that std1 is evaluated on */
			
			***This is a pass over each subjects to use the simplest specification ***

			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}	
			
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			***This is a pass over each subjects to use the simplest specification ***
	
			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'__Z treat (c.zo*`baseline'_c1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}	
			
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"  "`baseline' score = zo*`baseline'_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes
			

			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'__Z treat (`controls' c.zo*`baseline'_c1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"    "`baseline' score = zo*`baseline'_c1"  "School controls = mthly_fees_c1" ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes
*/
			
			************************************************************
			*******************     PREUNIT AND STD 1   ****************
			************************************************************
			
			
			foreach subject in math read kusoma{

				eststo clear
				
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local Studentcontrols "No"
				estadd local Schoolscontrols "No"
				
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if (gradename=="PREUNIT" | gradename=="STD 1") & (IndexET14DG15!=.), vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local Studentcontrols "No"
				estadd local Schoolscontrols "No"
				
				eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_pu)#c.dPU c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid) 
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "No"

				eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_pu)#c.dPU c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5 term6) if gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"

				
				capture drop overlapping
				capture teffects ipw (`subject'__Z) (treat (i.gradename2 c.(zo*`baseline'_pu)#c.dPU c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5 term6), logit) if (gradename=="PREUNIT" | gradename=="STD 1"), osample(overlapping)
				eststo: teffects ipw (`subject'__Z) (treat (i.gradename2 c.(zo*`baseline'_pu)#c.dPU c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5 term6), logit) if ( overlapping==0 & (gradename=="PREUNIT" | gradename=="STD 1")), vce(r)
				
				
				
				estadd ysumm
				qui tab academyid if e(sample)==1
				estadd scalar N_clust=r(r)
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_PU_C1.tex", se ar2 label b(a2) se(a2) nocon nonumber /// 
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) rename(r1vs0.treat treat) varlabels(treat Treatment) stats(N N_clust controls students Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Lagged test scores" "Student and school controls")) ///
				nonotes fragment

			} 
			
		
			if "`baseline'"!= "T1DG15" { /*This is the standard one */
				***This is a pass over every subjects with the plain specification ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Z treat (i.gradename2 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if  gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "No"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes


				***This is a pass wtih lagged test scores ***
				eststo clear
		
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_pu)#c.dPU c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if  (gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes

				***This is a pass wtih lagged test scores and school controls ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_pu)#c.dPU c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5 term6) if ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "Yes"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes
			}/*close if of standard world */
	/*
	
			if "`baseline'"== "T1DG15" { /*This is the weird one as there is no DG for PU */
				***This is a pass over every subjects with the plain specification ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Z treat ((i.gradename2 i.former_province2)##c.(term1 term2 term3 term4 term5 term6))##c.(term1 term2 term3 term4 term5 term6) if  gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "No"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes


				***This is a pass wtih lagged test scores ***
				eststo clear
		
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if   (gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes

				***This is a pass wtih lagged test scores and school controls ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_c1)#c.dSTD1 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5 term6)  if  ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "Yes"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_PU_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes
			} /*close if of weird world */
			*/
			
			/*
			**************************************************
			*******************     STD 5     ****************
			**************************************************

			***This is a pass over every subjects ***
			
			foreach subject in math lang swahili  sci ss {
				eststo clear
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				eststo: reg `subject'__Z treat (c.zo*`baseline'_c5 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				eststo: reg `subject'__Z treat  (`controls' c.zo*`baseline'_c5 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "`baseline' score = zo*`baseline'_c5"  "School controls = mthly_fees_c1" ) ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes
			}

			***This is a pass over each subjects to use the simplest specification ***
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			***This is a pass over each subjects wtih lagged test scores ***
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat (i.former_province2 c.zo*`baseline'_c5)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2" "`baseline' score = zo*`baseline'_c5"   ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes
			
			***This is a pass over each subjects wtih lagged test scores adn school controls ***
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat  (`controls' c.zo*`baseline'_c5 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C5_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   "`baseline' score = zo*`baseline'_c5" "School controls = mthly_fees_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			
			
			**************************************************
			*******************     STD 6     ****************
			**************************************************

			***This is a pass over every subjects ***
			
			foreach subject in math lang swahili  sci ss {
				eststo clear
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				
				
				eststo: reg `subject'__Z treat (c.zo*`baseline'_c6 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				eststo: reg `subject'__Z treat (`controls' c.zo*`baseline'_c6 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_C6.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "`baseline' score = zo*`baseline'_c6" "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes
			}

			***This is a pass over each subjects to use the simplest specification ***
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C6_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"     ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			***This is a pass over each subjects with lagged test scores ***
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat (i.former_province2  c.zo*`baseline'_c6)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C6.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"  "`baseline' score = zo*`baseline'_c6"    ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			***This is a pass over each subjects with lagged test scores and school controls ***
			eststo clear
				foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat   (`controls' c.zo*`baseline'_c6 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C6_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"  "`baseline' score = zo*`baseline'_c6"  "School controls = mthly_fees_c1" ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			*/
			
			
			
			************************************************************
			*******************     STD 5 AND STD 6     ****************
			************************************************************

			***This is a pass over every subjects with the plain specification ***
			
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat (i.gradename2  i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C6_C5_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students    , labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes


			***This is a pass over every subjects with lagged test scores ***
			
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat (i.gradename2 c.(zo*`baseline'_c5)#dSTD5 c.(zo*`baseline'_c6)#dSTD6 i.former_p)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "No"
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C6_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students , labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

			***This is a pass over every subjects with lagged test scores and school controls ***
			
			eststo clear
			foreach subject in math lang swahili  sci ss {
				eststo: reg `subject'__Z treat (i.gradename2 `controls' c.(zo*`baseline'_c5)#dSTD5 c.(zo*`baseline'_c6)#dSTD6 i.former_province2)##c.(term1 term2 term3 term4 term5 term6) if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"
			}
			esttab  using "$results\LatexCode\Pooled_Base_`baseline'_all_C6_C5_controls.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students    , labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes

}/* close foreach baseline */


