set matsize 1000, perm
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M

gen z0_math=math_T3ET14_Z


keep `controls2' gradename gradename2 dSTD1 dPU dSTD5 dSTD6 academyid former_province2 *_Z z0* treat pupilid IndexET14DG15

reshape long dev_@_Z env_@_Z lang_@_Z math_@_Z kusoma_@_Z re_@_Z read_@_Z sci_@_Z ss_@_Z swahili_@_Z, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")

label var dev__Z Development
label var env__Z Enviromental
label var lang__Z Language
label var math__Z Math
label var kusoma__Z Kusoma
label var re__Z RE
label var read__Z Reading
label var sci__Z Science
label var ss__Z "S.S."
label var swahili__Z Swahili
drop kuandika_T2ET15_Z- creative_T1ET15_Z kuandika_T3ET14_Z insha_T3ET14_Z- creative_T3ET14_Z kuandika_T1MT15_Z- eng_T1DG15_Z

foreach baseline in   ET14DG15{

			
			************************************************************
			*******************     PREUNIT AND STD 1   ****************
			************************************************************
			
			
			foreach subject in math{

				eststo clear
				
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 2" | gradename=="STD 3" |  gradename=="STD 4", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui sum pupilid if e(sample)==1
				estadd scalar students=r(N)
				estadd local Studentcontrols "No"
				estadd local Schoolscontrols "No"

				
				eststo: reg `subject'__Z treat (i.gradename2##c.z0_math i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 2" | gradename=="STD 3" |  gradename=="STD 4", vce(cluster academyid) 
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui sum pupilid if e(sample)==1
				estadd scalar students=r(N)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "No"

				eststo: reg `subject'__Z treat (i.gradename2##c.z0_math i.former_province2 `controls')##c.(term1 term2 term3 term4 term5) if gradename=="STD 2" | gradename=="STD 3" |  gradename=="STD 4", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui sum pupilid if e(sample)==1
				estadd scalar students=r(N)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"

				
			
				
			
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_C2C3C4.tex", se ar2 label b(a2) se(a2) nocon nonumber /// 
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) rename(r1vs0.treat treat) varlabels(treat Treatment) stats(N N_clust controls students Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Lagged test scores" "Student and school controls")) ///
				nonotes fragment

			} 
			
			
				foreach subject in math{

				eststo clear
				
				eststo: reg `subject'__Z treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="Baby Class" |  gradename=="Nursery", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui sum pupilid if e(sample)==1
				estadd scalar students=r(N)
				estadd local Studentcontrols "No"
				estadd local Schoolscontrols "No"

				
				eststo: reg `subject'__Z treat (i.gradename2##c.z0_math i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="Baby Class" |  gradename=="Nursery", vce(cluster academyid) 
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui sum pupilid if e(sample)==1
				estadd scalar students=r(N)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "No"

				eststo: reg `subject'__Z treat (i.gradename2##c.z0_math i.former_province2 `controls')##c.(term1 term2 term3 term4 term5) if  gradename=="Baby Class" |  gradename=="Nursery", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui sum pupilid if e(sample)==1
				estadd scalar students=r(N)
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"

				
			
				
			
				
				esttab  using "$results\LatexCode\Pooled_Base_`baseline'_`subject'_BANU.tex", se ar2 label b(a2) se(a2) nocon nonumber /// 
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) rename(r1vs0.treat treat) varlabels(treat Treatment) stats(N N_clust controls students Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Lagged test scores" "Student and school controls")) ///
				nonotes fragment

			} 
			
		

}/* close foreach baseline */


