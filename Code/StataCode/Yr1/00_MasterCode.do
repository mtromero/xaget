clear all
* Central Path
 *global mipath "C:/Users/Mauricio/Dropbox/Learning Lab/XageT"
 *global mipath "E:/Users/Mauricio/Dropbox/Learning Lab/XageT/"
  global mipath "/media/mauricio/TeraHDD1/Dropbox/Learning Lab/XageT"
set matsize 5000
* Path Tree
  global basein 	"$mipath/RawData"
  global base_out   "$mipath/CreatedData"
  global dir_do     "C:/Users/Mauricio/Documents/git/XageT/Code/StataCode"
  *global dir_do     "/media/mauricio/TeraHDD/git/xaget"
  global results    "$mipath/Results"
  global exceltables  "$mipath/Results/ExcelTables"
  global graphs     "$mipath/Results/Graphs"
  global latexcodes     "$mipath/Results/LatexCodes"
	
	
	do "$dir_do/01_ReadData"
	do "$dir_do/02_checkbalance"
	do "$dir_do/02_BalanceAndAttrition"
	do "$dir_do/03a_MainTreatmentEffects_ownData_All"
	do "$dir_do/03b_MainTreatmentEffects_ownData_Evolution"
	do "$dir_do/03c_MainTreatmentEffects_ownData_Pooled"
	*do "$dir_do/04_Censoring"
	do "$dir_do/05_heterogeneity.do"
	do "$dir_do/05_heterogeneity_grade.do"
	do "$dir_do/06a_NonParam_Baseline"
	do "$dir_do/06b_NonParam_Baseline_Tutors"
	do "$dir_do/07a_NonParam_Endilne"
	do "$dir_do/07b_NonParam_Endline_Tutors"
	do "$dir_do/08_NonParam_TutorsMean"
	do "$dir_do/09_NonParam_TutorsSD"
