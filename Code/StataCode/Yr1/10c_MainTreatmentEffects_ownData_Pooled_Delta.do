set matsize 1000, perm
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M



keep `controls2' gradename gradename2 dSTD1 dPU dSTD5 dSTD6 academyid former_province2 *_Delta treat pupilid

reshape long dev_@_Delta env_@_Delta lang_@_Delta math_@_Delta kusoma_@_Delta re_@_Delta read_@_Delta sci_@_Delta ss_@_Delta swahili_@_Delta, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")

label var dev__Delta Development
label var env__Delta Enviromental
label var lang__Delta Language
label var math__Delta Math
label var kusoma__Delta Kusoma
label var re__Delta RE
label var read__Delta Reading
label var sci__Delta Science
label var ss__Delta "S.S."
label var swahili__Delta Swahili

	
			**************************************************
			*******************     PREUNIT   ****************
			**************************************************
			
			***This is a pass over all subjects to use different specifications ***
			
			foreach subject in math env{

				eststo clear
				
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)


				eststo: reg `subject'__Delta treat (`controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_Delta_`subject'_PU.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"  "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			} /*Subjects that PU is evaluated on */
			
			***This is a pass over each subjects to use the simplest specification ***

			eststo clear
			foreach subject in math   env{
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_PU_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
			

			***This is a pass over each subjects to use with lagged test scores and school controls***

			eststo clear
			foreach subject in math   env{
				eststo: reg `subject'__Delta treat (`controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r) 
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_PU_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  "School controls = mthly_fees_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			**************************************************
			*******************     STD 1     ****************
			**************************************************
			
			***This is a pass over all subjects to use different specifications ***

			foreach subject in math read kusoma sci ss re{

				eststo clear
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

			

				eststo: reg `subject'__Delta treat (`controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_Delta_`subject'_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "Former Province F.E.= *former_province2"   "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			} /*Subjects that std1 is evaluated on */
			
			***This is a pass over each subjects to use the simplest specification ***

			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}	
			
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			***This is a pass over each subjects to use the simplest specification ***
	


			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'__Delta treat (`controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"     "School controls = mthly_fees_c1" ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			
			************************************************************
			*******************     PREUNIT AND STD 1   ****************
			************************************************************

				***This is a pass over every subjects with the plain specification ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Delta treat (i.gradename2 i.former_province2)##c.(term1 term2 term3 term4 term5) if  gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "No"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_Delta_all_PU_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



				***This is a pass wtih lagged test scores and school controls ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'__Delta treat (i.gradename2 i.former_province2 `controls')##c.(term1 term2 term3 term4 term5) if ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					qui tab pupilid if e(sample)==1
					estadd scalar students=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "Yes"
				}
				
				esttab  using "$results\LatexCode\Pooled_Base_Delta_all_PU_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				mgroups("Math" "Languages", pattern(1 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


			
			**************************************************
			*******************     STD 5     ****************
			**************************************************

			***This is a pass over every subjects ***
			
			foreach subject in math lang swahili  re sci ss {
				eststo clear
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)



				eststo: reg `subject'__Delta treat  (`controls')##c.(term1 term2 term3 term4 term5) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_Delta_`subject'_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"    "School controls = mthly_fees_c1" ) ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
			}

			***This is a pass over each subjects to use the simplest specification ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


			
			***This is a pass over each subjects wtih lagged test scores adn school controls ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'__Delta treat  (`controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C5_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   "School controls = mthly_fees_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			
			
			**************************************************
			*******************     STD 6     ****************
			**************************************************

			***This is a pass over every subjects ***
			
			foreach subject in math lang swahili  re sci ss {
				eststo clear
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				
				


				eststo: reg `subject'__Delta treat (`controls'  i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)

				esttab  using "$results\LatexCode\Pooled_Base_Delta_`subject'_C6.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
				nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
			}

			***This is a pass over each subjects to use the simplest specification ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'__Delta treat i.former_province2##c.(term1 term2 term3 term4 term5) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C6_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"     ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



			***This is a pass over each subjects with lagged test scores and school controls ***
			eststo clear
				foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'__Delta treat   (`controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
			}
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C6_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   "School controls = mthly_fees_c1" ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students, labels("N. of obs." "Number of schools" "Control schools" "Students")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

			
			
			
			
			************************************************************
			*******************     STD 5 AND STD 6     ****************
			************************************************************

			***This is a pass over every subjects with the plain specification ***
			
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'__Delta treat (i.gradename2  i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "No"
				estadd local Schoolscontrols "No"
			}
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C6_C5_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



			***This is a pass over every subjects with lagged test scores and school controls ***
			
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'__Delta treat (i.gradename2 `controls' i.former_province2)##c.(term1 term2 term3 term4 term5) if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				qui tab pupilid if e(sample)==1
				estadd scalar students=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"
			}
			esttab  using "$results\LatexCode\Pooled_Base_Delta_all_C6_C5_controls.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls students ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Students" "Province FE" "Student controls" "School controls")) ///
			nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



