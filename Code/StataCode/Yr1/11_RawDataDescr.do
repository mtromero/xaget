
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear




foreach Period in T3ET14 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15{
	foreach subject in math read kusoma lang swahili  re sci ss{
		bys treat gradename: cumul `subject'_`Period', gen(cum_`subject'_`Period') eq
	}
}


		
foreach Period in T3ET14 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15{
	foreach subject in math  {
	
		if "`subject'"=="math" local name "Math"
		if "`subject'"=="read" local name "English"
		if "`subject'"=="kusoma" local name "Swahili"
		if "`subject'"=="lang" local name "English"
		if "`subject'"=="swahili" local name "Swahili"
		if "`subject'"=="sci" local name "Science"
		if "`subject'"=="ss" local name "Social Sciences"

		
		twoway (kdensity `subject'_`Period' if gradename=="PREUNIT" & treat==0, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity `subject'_`Period' if gradename=="PREUNIT" & treat==1, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(Density) title("Distribution of `name' scores by treatment" "Pre Unit") graphregion(color(white))
		graph export "$results\Graphs\Dist_`subject'_`Period'_PU.pdf", as(pdf) replace
		
		twoway (line cum_`subject'_`Period' `subject'_`Period' if gradename=="PREUNIT" & treat==0,sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_`subject'_`Period' `subject'_`Period' if gradename=="PREUNIT" & treat==1, sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(CDF) title("Distribution of `name' scores by treatment" "Pre Unit") graphregion(color(white))
		graph export "$results\Graphs\CDF_`subject'_`Period'_PU.pdf", as(pdf) replace		
	}
}

foreach Period in T3ET14 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15{
	foreach subject in math read kusoma{
	
		if "`subject'"=="math" local name "Math"
		if "`subject'"=="read" local name "English"
		if "`subject'"=="kusoma" local name "Swahili"
		if "`subject'"=="lang" local name "English"
		if "`subject'"=="swahili" local name "Swahili"
		if "`subject'"=="sci" local name "Science"
		if "`subject'"=="ss" local name "Social Sciences"


		twoway (kdensity `subject'_`Period' if gradename=="STD 1" & treat==0, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity `subject'_`Period' if gradename=="STD 1" & treat==1, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(Density) title("Distribution of `name' scores by treatment" "Standard 1") graphregion(color(white))
		graph export "$results\Graphs\Dist_`subject'_`Period'_C1.pdf", as(pdf) replace
		
		twoway (line cum_`subject'_`Period' `subject'_`Period' if gradename=="STD 1" & treat==0,sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_`subject'_`Period' `subject'_`Period' if gradename=="STD 1" & treat==1, sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(CDF) title("Distribution of `name' scores by treatment" "Standard 1") graphregion(color(white))
		graph export "$results\Graphs\CDF_`subject'_`Period'_C1.pdf", as(pdf) replace
		
	}
}


		




foreach Period in T3ET14 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15{
	foreach subject in math lang swahili sci ss{
	
		if "`subject'"=="math" local name "Math"
		if "`subject'"=="read" local name "English"
		if "`subject'"=="kusoma" local name "Swahili"
		if "`subject'"=="lang" local name "English"
		if "`subject'"=="swahili" local name "Swahili"
		if "`subject'"=="sci" local name "Science"
		if "`subject'"=="ss" local name "Social Sciences"

		twoway (kdensity `subject'_`Period' if gradename=="STD 5" & treat==0, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity `subject'_`Period' if gradename=="STD 5" & treat==1, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(Density) title("Distribution of `name' scores by treatment" "Standard 5") graphregion(color(white))
		graph export "$results\Graphs\Dist_`subject'_`Period'_C5.pdf", as(pdf) replace
		twoway (line cum_`subject'_`Period' `subject'_`Period' if gradename=="STD 5" & treat==0, sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_`subject'_`Period' `subject'_`Period' if gradename=="STD 5" & treat==1, sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(Density) title("Distribution of `name' scores by treatment" "Standard 5") graphregion(color(white))
		graph export "$results\Graphs\CDF_`subject'_`Period'_C5.pdf", as(pdf) replace
		twoway (kdensity `subject'_`Period' if gradename=="STD 6" & treat==0, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity `subject'_`Period' if gradename=="STD 6" & treat==1, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(Density) title("Distribution of `name' scores by treatment" "Standard 6") graphregion(color(white))
		graph export "$results\Graphs\Dist_`subject'_`Period'_C6.pdf", as(pdf) replace
		twoway (line cum_`subject'_`Period' `subject'_`Period' if gradename=="STD 6" & treat==0, sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_`subject'_`Period' `subject'_`Period' if gradename=="STD 6" & treat==1, sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `Period'") ytitle(Density) title("Distribution of `name' scores by treatment" "Standard 6") graphregion(color(white))
		graph export "$results\Graphs\CDF_`subject'_`Period'_C6.pdf", as(pdf) replace
	}
}
