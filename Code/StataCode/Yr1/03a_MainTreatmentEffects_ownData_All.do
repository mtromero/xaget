
use "$base_out/2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M

local controlspu_T3ET14 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_T3ET14 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_T3ET14 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_T3ET14 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3

local controlspu_ET14DG15 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_ET14DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3

local controlspu_T1DG15 
local controlsc1_T1DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3
local controlsc5_T1DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3
local controlsc6_T1DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3

local controlspu_T1MT15 read_T1MT15_Z kusoma_T1MT15_Z env_T1MT15_Z math_T1MT15_Z read_T1MT15_Z2 kusoma_T1MT15_Z2 env_T1MT15_Z2 math_T1MT15_Z2 read_T1MT15_Z3 kusoma_T1MT15_Z3 env_T1MT15_Z3 math_T1MT15_Z3
local controlsc1_T1MT15 math_T1MT15_Z read_T1MT15_Z  kusoma_T1MT15_Z re_T1MT15_Z sci_T1MT15_Z ss_T1MT15_Z math_T1MT15_Z2 read_T1MT15_Z2  kusoma_T1MT15_Z2 re_T1MT15_Z2 sci_T1MT15_Z2 ss_T1MT15_Z2 math_T1MT15_Z3 read_T1MT15_Z3  kusoma_T1MT15_Z3 re_T1MT15_Z3 sci_T1MT15_Z3 ss_T1MT15_Z3
local controlsc5_T1MT15 lang_T1MT15_Z math_T1MT15_Z sci_T1MT15_Z re_T1MT15_Z ss_T1MT15_Z  swahili_T1MT15_Z lang_T1MT15_Z2 math_T1MT15_Z2 sci_T1MT15_Z2 re_T1MT15_Z2 ss_T1MT15_Z2 swahili_T1MT15_Z2 lang_T1MT15_Z3 math_T1MT15_Z3 sci_T1MT15_Z3 re_T1MT15_Z3 ss_T1MT15_Z3  swahili_T1MT15_Z3
local controlsc6_T1MT15 lang_T1MT15_Z math_T1MT15_Z sci_T1MT15_Z re_T1MT15_Z ss_T1MT15_Z  swahili_T1MT15_Z lang_T1MT15_Z2 math_T1MT15_Z2 sci_T1MT15_Z2 re_T1MT15_Z2 ss_T1MT15_Z2 swahili_T1MT15_Z2 lang_T1MT15_Z3 math_T1MT15_Z3 sci_T1MT15_Z3 re_T1MT15_Z3 ss_T1MT15_Z3  swahili_T1MT15_Z3




*********************************************************************************
*********************************************************************************
*********************************************************************************
*******************     MAIN ANALYSIS   *****************************************
*********************************************************************************
*********************************************************************************
*********************************************************************************
* 
*T1MT15 T1ET15 
*T3ET14 T1DG15 T1MT15 T2ET15
*T3ET14 T1DG15
foreach baseline in  ET14DG15{
	foreach Period in T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		if "`baseline'"!="`Period'"! {
		
			**************************************************
			*******************     PREUNIT   ****************
			**************************************************
			
			***This is a pass over all subjects to use different specifications ***
			
			foreach subject in math read kusoma env{

				eststo clear
				
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)


				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="PREUNIT" & !missing(Index`baseline'), vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				
				eststo: reg `subject'_`Period'_Z treat `controlspu_`baseline'' i.former_province2 if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat `controls' `controlspu_`baseline'' i.former_province2 if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_`subject'_PU.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "`baseline' score = `controlspu_`baseline''"  "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			} /*Subjects that PU is evaluated on */
			
			***This is a pass over each subjects to use the simplest specification ***

			eststo clear
			foreach subject in math read kusoma env{
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			
			***This is a pass over each subjects to use with lagged test scores***

			eststo clear
			foreach subject in math read kusoma env{
				eststo: reg `subject'_`Period'_Z treat `controlspu_`baseline'' i.former_province2 if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			} /*Subjects that PU is evaluated on */

			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  "`baseline' score = `controlspu_`baseline''"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			***This is a pass over each subjects to use with lagged test scores and school controls***

			eststo clear
			foreach subject in math read kusoma env{
				eststo: reg `subject'_`Period'_Z treat `controls' `controlspu_`baseline'' i.former_province2 if gradename=="PREUNIT", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r) 
			} /*Subjects that PU is evaluated on */

			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 ) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
			indicate(  "County F.E.= *former_province2"  "`baseline' score = `controlspu_`baseline''" "School controls = mthly_fees_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			**************************************************
			*******************     STD 1     ****************
			**************************************************
			
			***This is a pass over all subjects to use different specifications ***

			foreach subject in math read kusoma sci ss re{

				eststo clear
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 1" & !missing(Index`baseline'), vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				
				eststo: reg `subject'_`Period'_Z treat `controlsc1_`baseline'' i.former_province2 if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat `controls' `controlsc1_`baseline'' i.former_province2 if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_`subject'_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "Former Province F.E.= *former_province2"  "`baseline' score = `controlsc1_`baseline''" "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) replace /// 
				keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			} /*Subjects that std1 is evaluated on */
			
			***This is a pass over each subjects to use the simplest specification ***

			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}	
			
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			***This is a pass over each subjects to use the simplest specification ***
	
			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'_`Period'_Z treat `controlsc1_`baseline'' i.former_province2 if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}	
			
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"  "`baseline' score = `controlsc1_`baseline''"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			

			eststo clear
			foreach subject in math read kusoma  sci ss re{
				eststo: reg `subject'_`Period'_Z treat `controls' `controlsc1_`baseline'' i.former_province2 if gradename=="STD 1", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
			indicate(  "Former Province F.E.= *former_province2"    "`baseline' score = `controlsc1_`baseline''"  "School controls = mthly_fees_c1" ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			
			************************************************************
			*******************     PREUNIT AND STD 1   ****************
			************************************************************
			if "`baseline'"!= "T1DG15" { /*This is the standard one */
				***This is a pass over every subjects with the plain specification ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'_`Period'_Z treat i.gradename2 i.former_province2 if  gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "No"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


				***This is a pass wtih lagged test scores ***
				eststo clear
		
				foreach subject in math read kusoma   {
					eststo: reg `subject'_`Period'_Z treat i.gradename2 c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2 if  (gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

				***This is a pass wtih lagged test scores and school controls ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'_`Period'_Z treat i.gradename2 c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2 `controls' if ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "Yes"
				}
				
				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				mgroups("Math" "Languages", pattern(1 1 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			}/*close if of standard world */

			if "`baseline'"== "T1DG15" { /*This is the weird one as there is no DG for PU */
				***This is a pass over every subjects with the plain specification ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'_`Period'_Z treat i.gradename2 i.former_province2 if  gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "No"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_C1_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


				***This is a pass wtih lagged test scores ***
				eststo clear
		
				foreach subject in math read kusoma   {
					eststo: reg `subject'_`Period'_Z treat i.gradename2 c.(zo*`baseline'_c1)#dSTD1 i.former_province2 if   (gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "No"
				}
				
				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_C1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
				mgroups("Math" "Languages", pattern(1 1 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

				***This is a pass wtih lagged test scores and school controls ***
				eststo clear
				foreach subject in math read kusoma   {
					eststo: reg `subject'_`Period'_Z treat i.gradename2 c.(zo*`baseline'_c1)#dSTD1 i.former_province2 `controls' if  ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
					estadd ysumm
					qui tab academyid if e(sample)==1 & treat==0
					estadd scalar controls=r(r)
					estadd local ProvinceFE "Yes"
					estadd local Studentcontrols "Yes"
					estadd local Schoolscontrols "Yes"
				}
				
				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_PU_C1_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				mgroups("Math" "Languages", pattern(1 1 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			} /*close if of weird world */
			
			**************************************************
			*******************     STD 5     ****************
			**************************************************

			***This is a pass over every subjects ***
			
			foreach subject in math lang swahili  re sci ss {
				eststo clear
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 5" & !missing(Index`baseline'), vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat `controlsc5_`baseline'' i.former_province2 if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat `controls' `controlsc5_`baseline'' i.former_province2 if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_`subject'_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "`baseline' score = `controlsc5_`baseline''"  "School controls = mthly_fees_c1" ) ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			}

			***This is a pass over each subjects to use the simplest specification ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			***This is a pass over each subjects wtih lagged test scores ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.former_province2 `controlsc5_`baseline'' if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2" "`baseline' score = `controlsc5_`baseline''"   ) ///
			star(* 0.10 ** 0.05 *** 0.01) ///
			replace keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			
			***This is a pass over each subjects wtih lagged test scores adn school controls ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat `controls' `controlsc5_`baseline'' i.former_province2 if gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C5_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"   "`baseline' score = `controlsc5_`baseline''" "School controls = mthly_fees_c1"  ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			
			
			**************************************************
			*******************     STD 6     ****************
			**************************************************

			***This is a pass over every subjects ***
			
			foreach subject in math lang swahili  re sci ss {
				eststo clear
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 6" & !missing(Index`baseline'), vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				
				
				eststo: reg `subject'_`Period'_Z treat `controlsc6_`baseline'' i.former_province2 if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				eststo: reg `subject'_`Period'_Z treat `controls' `controlsc6_`baseline'' i.former_province2 if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)

				esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_`subject'_C6.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
				indicate(  "County F.E.= *former_province2"   "`baseline' score = `controlsc6_`baseline''" "School controls = mthly_fees_c1"  ) ///
				star(* 0.10 ** 0.05 *** 0.01) ///
				replace keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
				nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
			}

			***This is a pass over each subjects to use the simplest specification ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.former_province2 if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C6_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"     ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			***This is a pass over each subjects with lagged test scores ***
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.former_province2  `controlsc6_`baseline'' if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C6.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"  "`baseline' score = `controlsc6_`baseline''"    ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			***This is a pass over each subjects with lagged test scores and school controls ***
			eststo clear
				foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat `controls'  `controlsc6_`baseline'' i.former_province2 if gradename=="STD 6", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C6_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			indicate(  "County F.E.= *former_province2"  "`baseline' score = `controlsc6_`baseline''"  "School controls = mthly_fees_c1" ) ///
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			
			
			
			
			************************************************************
			*******************     STD 5 AND STD 6     ****************
			************************************************************

			***This is a pass over every subjects with the plain specification ***
			
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.gradename2  i.former_province2 if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "No"
				estadd local Schoolscontrols "No"
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C6_C5_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Province FE" "Student controls" "School controls")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


			***This is a pass over every subjects with lagged test scores ***
			
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.gradename2 c.(`controlsc5_`baseline'')#dSTD5 c.(`controlsc6_`baseline'')#dSTD6 i.former_province2 if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "No"
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C6_C5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Province FE" "Student controls" "School controls")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

			***This is a pass over every subjects with lagged test scores and school controls ***
			
			eststo clear
			foreach subject in math lang swahili  re sci ss {
				eststo: reg `subject'_`Period'_Z treat i.gradename2 `controls' c.(`controlsc5_`baseline'')#dSTD5 c.(`controlsc6_`baseline'')#dSTD6 i.former_province2 if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
				estadd ysumm
				qui tab academyid if e(sample)==1 & treat==0
				estadd scalar controls=r(r)
				estadd local ProvinceFE "Yes"
				estadd local Studentcontrols "Yes"
				estadd local Schoolscontrols "Yes"
			}
			esttab  using "$results/LatexCode/Per_`Period'_Base_`baseline'_all_C6_C5_controls.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
			mgroups("Math" "Languages" "Others", pattern(1 1 0 1 0 0) prefix(/multicolumn{@span}{c}{) suffix(}) span erepeat(/cmidrule(lr){@span}))  /// 
			star(* 0.10 ** 0.05 *** 0.01) replace /// 
			keep(treat) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
			nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

		}/* close if that baseline is different from end period*/
	} /* close foreach period at the end */
}/* close foreach baseline */


