set matsize 1000, perm
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M




keep `controls2' gradename gradename2 dSTD1 dPU dSTD5 dSTD6 academyid former_province2 *_Delta treat pupilid

reshape long dev_@_Delta env_@_Delta lang_@_Delta math_@_Delta kusoma_@_Delta re_@_Delta read_@_Delta sci_@_Delta ss_@_Delta swahili_@_Delta, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")

*T3ET14 T1DG15

	
	foreach subject in math   {
		reg `subject'__Delta (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5  if gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_C1PU_plain.pdf", replace
	}
	foreach subject in math {
		reg `subject'__Delta (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5  if gradename=="PREUNIT" , vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_PU_plain.pdf", replace
	}
	foreach subject in math read kusoma {
		reg `subject'__Delta (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5  if gradename=="STD 1", vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_C1_plain.pdf", replace
	}
	

		
	foreach subject in math   {
		reg `subject'__Delta (c.treat i.gradename2 `controls'   i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5 if  ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_C1PU_control.pdf", replace
	}
	foreach subject in math {
		reg `subject'__Delta (c.treat i.gradename2 `controls'   i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5 if  ( gradename=="PREUNIT"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_PU_control.pdf", replace
	}
	foreach subject in math read kusoma {
		reg `subject'__Delta (c.treat i.gradename2 `controls'   i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5 if  ( gradename=="STD 1"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_C1_control.pdf", replace
	}



	foreach subject in math lang swahili re sci ss{
		reg `subject'__Delta (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5 if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_C5C6_plain.pdf", replace
	}
	

	foreach subject in math lang swahili re sci ss{
		reg `subject'__Delta (c.treat i.gradename2 `controls'  i.former_province2)#c.(term1 term2 term3 term4 term5) term1 term2 term3 term4 term5 if (gradename=="STD 6" | gradename=="STD 5"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_Delta_C5C6_control.pdf", replace
	}


