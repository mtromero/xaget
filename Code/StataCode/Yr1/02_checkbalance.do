set more off
clear all
capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 se_1 se_2 d_p
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
 reg `var' TD1 TD2 i.`strat_id' `if', nocons vce(cluster `clus_id')
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p'  = nullmat(`d_p'),r(p)
 matrix A=e(b)
 matrix B=e(V)
 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
 sum `var' if TD1==1 & e(sample)==1
 mat `mu_1' = nullmat(`mu_1'), r(mean)
 mat `se_1' = nullmat(`se_1'), r(sd)
 sum `var' if TD2==1 & e(sample)==1
 mat `mu_2' = nullmat(`mu_2'),r(mean)
 mat `se_2' = nullmat(`se_2'), r(sd)
}
foreach mat in mu_1 mu_2 mu_3  se_1 se_2 d_p {
 mat coln ``mat'' = `varlist'
}
 local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3  se_1 se_2  d_p {
 eret mat `mat' = ``mat''
}
end

use "$base_out\PTR.dta", clear
rename grade gradename_T3ET14
drop ptr_T1DG15
collapse (mean) ptr_T3ET14, by(academyid gradename_T3ET14)
encode gradename_T3ET14, gen(gradename_T3ET142)
drop gradename_T3ET14
rename gradename_T3ET142 gradename_T3ET14
reshape wide ptr_T3ET14, i(academyid) j(gradename_T3ET14)
save "$base_out\TempPTR.dta", replace 

use "$base_out\PTR.dta", clear
rename grade gradename
drop ptr_T3ET14
collapse (mean) ptr_T1DG15, by(academyid gradename)
encode gradename, gen(gradename2)
drop gradename
rename gradename2 gradename
reshape wide ptr_T1DG15, i(academyid) j(gradename)
merge 1:1 academyid using "$base_out\TempPTR.dta" 
drop _merge
merge 1:1 academyid using "$base_out\SchoolLeveLData.dta" 
drop if _merge==1


label var ptr_T1DG153 "PTR PU T1DG15"
label var ptr_T1DG154 "PTR C1 T1DG15"
label var ptr_T3ET143 "PTR PU T3ET14"
label var ptr_T3ET144 "PTR C1 T3ET14"

  
label var DaysOpen "Days since launch date (as of 1/Jan/2015)"
label var wagecat_D1 "Monthly teacher wage of 11250 KSH"
label var wagecat_D2 "Monthly teacher wage of 10400 KSH"
label var wagecat_D3 "Monthly teacher wage of 7970 KSH"
label var c1_aic "C1 Allowed in class"
label var c6_aic "C6 Allowed in class"
label var c1_active "C1 enrolled"
label var c6_active "C6 enrolled"
label var mthly_fees_c1 "Monthly Fees C1"

encode academyid, gen(academyid2)
encode former_province, gen(former_province2)

xi: my_ptest  DaysOpen wagecat_D1 wagecat_D2 wagecat_D3 c1_aic c1_active c6_aic c6_active religion_D1 mthly_fees_c1 ptr_T1DG153 ptr_T1DG154 ptr_T3ET143 ptr_T3ET144, by(treat) clus_id(academyid2) strat_id(former_province2)

esttab using "$results\LatexCode\BalanceTable_SchoolLevel.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 

esttab using "$results\LatexCode\BalanceTable_SchoolLevel.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Treatment" "p-value")  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) d_p(fmt(a2) star pvalue(d_p))" "se_1(par) se_2(par) .") 
/*
eststo clear
foreach subject of varlist  DaysOpen wagecat_D1 wagecat_D2 wagecat_D3 c1_aic c1_active c6_aic c6_active religion_D1 mthly_fees_c1 ptr_T1DG153 ptr_T1DG154 ptr_T3ET143 ptr_T3ET144{
eststo: reg `subject' treat i.former_province2 , vce(cluster academyid)
estadd ysumm
qui tab academyid if e(sample)==1 & treat==0
estadd scalar controls=r(r)
qui sum  `subject' if e(sample)==1 & treat==1
estadd scalar mean_treat=r(mean)
qui sum  `subject' if e(sample)==1 & treat==0
estadd scalar mean_control=r(mean)
}
esttab  using "$results\LatexCode\BalanceSchool.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace /// 
keep(treat) stats(mean_treat mean_control N_clust controls, labels("Treatment mean" "Control mean" "Number of schools" "Control schools")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
*/
