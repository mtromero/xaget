set matsize 1000, perm
use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M



local controlspu_T3ET14 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_T3ET14 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_T3ET14 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_T3ET14 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3

local controlspu_ET14DG15 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_ET14DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3

local controlspu_T1DG15 
local controlsc1_T1DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3
local controlsc5_T1DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3
local controlsc6_T1DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3

local controlspu_T1MT15 read_T1MT15_Z kusoma_T1MT15_Z env_T1MT15_Z math_T1MT15_Z read_T1MT15_Z2 kusoma_T1MT15_Z2 env_T1MT15_Z2 math_T1MT15_Z2 read_T1MT15_Z3 kusoma_T1MT15_Z3 env_T1MT15_Z3 math_T1MT15_Z3
local controlsc1_T1MT15 math_T1MT15_Z read_T1MT15_Z  kusoma_T1MT15_Z re_T1MT15_Z sci_T1MT15_Z ss_T1MT15_Z math_T1MT15_Z2 read_T1MT15_Z2  kusoma_T1MT15_Z2 re_T1MT15_Z2 sci_T1MT15_Z2 ss_T1MT15_Z2 math_T1MT15_Z3 read_T1MT15_Z3  kusoma_T1MT15_Z3 re_T1MT15_Z3 sci_T1MT15_Z3 ss_T1MT15_Z3
local controlsc5_T1MT15 lang_T1MT15_Z math_T1MT15_Z sci_T1MT15_Z re_T1MT15_Z ss_T1MT15_Z  swahili_T1MT15_Z lang_T1MT15_Z2 math_T1MT15_Z2 sci_T1MT15_Z2 re_T1MT15_Z2 ss_T1MT15_Z2 swahili_T1MT15_Z2 lang_T1MT15_Z3 math_T1MT15_Z3 sci_T1MT15_Z3 re_T1MT15_Z3 ss_T1MT15_Z3  swahili_T1MT15_Z3
local controlsc6_T1MT15 lang_T1MT15_Z math_T1MT15_Z sci_T1MT15_Z re_T1MT15_Z ss_T1MT15_Z  swahili_T1MT15_Z lang_T1MT15_Z2 math_T1MT15_Z2 sci_T1MT15_Z2 re_T1MT15_Z2 ss_T1MT15_Z2 swahili_T1MT15_Z2 lang_T1MT15_Z3 math_T1MT15_Z3 sci_T1MT15_Z3 re_T1MT15_Z3 ss_T1MT15_Z3  swahili_T1MT15_Z3




keep `controls2' gradename gradename2 dSTD1 dPU dSTD5 dSTD6 academyid former_province2 *_Z zo* treat pupilid

reshape long dev_@_Z env_@_Z lang_@_Z math_@_Z kusoma_@_Z re_@_Z read_@_Z sci_@_Z ss_@_Z swahili_@_Z, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")
gen term6=(term=="T3ET15")

*T3ET14 T1DG15
foreach baseline in  ET14DG15{
	
	foreach subject in math read kusoma {
		reg `subject'__Z (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6  if gradename=="PREUNIT" | gradename=="STD 1", vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1PU_plain.pdf", replace
		
		reg `subject'__Z (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6  if gradename=="PREUNIT" , vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_PU_plain.pdf", replace
		
		reg `subject'__Z (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6  if gradename=="STD 1", vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1_plain.pdf", replace
	}
	
	if "`baseline'"!= "T1DG15" { /*This is the standard one */
		foreach subject in math read kusoma {
			reg `subject'__Z (c.treat i.gradename2   c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1PU.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2   c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if ( gradename=="PREUNIT"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_PU.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2   c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if ( gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1.pdf", replace
		}
		
		foreach subject in math read kusoma {
			reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if  ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1PU_control.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if  ( gradename=="PREUNIT"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_PU_control.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_pu)#dPU c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if  ( gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1_control.pdf", replace
		}
	}
	
	if "`baseline'"== "T1DG15" { /*This is the weird one */
		foreach subject in math read kusoma {
			reg `subject'__Z (c.treat i.gradename2  c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if  ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1PU.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2  c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if  ( gradename=="PREUNIT" |), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_PU.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2  c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if  ( gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1.pdf", replace
		}
		
		foreach subject in math read kusoma {
			reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if ( gradename=="PREUNIT" | gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1PU_control.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if ( gradename=="PREUNIT"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_PU_control.pdf", replace
			
			reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_c1)#dSTD1 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if (gradename=="STD 1"), vce(cluster academyid)
			coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects for `subject'")
			graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C1_control.pdf", replace
		}
	}


	foreach subject in math lang swahili re sci ss{
		reg `subject'__Z (c.treat i.gradename2  i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if gradename=="STD 6" | gradename=="STD 5", vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C5C6_plain.pdf", replace
	}
	
	foreach subject in math lang swahili re sci ss{
		reg `subject'__Z (c.treat i.gradename2 c.(zo*`baseline'_c5)#dSTD5 c.(zo*`baseline'_c6)#dSTD6 i.former_province2)#c.(term1 term2 term3 term4 term5)  term1 term2 term3 term4 term5 if (gradename=="STD 6" | gradename=="STD 5"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C5C6.pdf", replace
		
		reg `subject'__Z (c.treat i.gradename2 c.(zo*`baseline'_c5)#dSTD5 c.(zo*`baseline'_c6)#dSTD6 i.former_province2)#c.(term1 term2 term3 term4 term5)  term1 term2 term3 term4 term5 if (gradename=="STD 5"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C5.pdf", replace
		
		reg `subject'__Z (c.treat i.gradename2 c.(zo*`baseline'_c5)#dSTD5 c.(zo*`baseline'_c6)#dSTD6 i.former_province2)#c.(term1 term2 term3 term4 term5)  term1 term2 term3 term4 term5 if (gradename=="STD 6"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C6.pdf", replace
	}

	foreach subject in math lang swahili re sci ss{
		reg `subject'__Z (c.treat i.gradename2 `controls'  c.(zo*`baseline'_c5)#dSTD5 c.(zo*`baseline'_c6)#dSTD6 i.former_province2)#c.(term1 term2 term3 term4 term5 term6) term1 term2 term3 term4 term5 term6 if (gradename=="STD 6" | gradename=="STD 5"), vce(cluster academyid)
		coefplot, graphregion(color(white)) baselevels keep(c.treat*) ci rename(c.treat#c.term1="T1-MT" c.treat#c.term2="T1-ET" c.treat#c.term3="T2-MT" c.treat#c.term4="T2-ET" c.treat#c.term5="T3-MT" c.treat#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment effects `subject'")
		graph export "$results\Graphs\Evol_`subject'_base_`baseline'_C5C6_control.pdf", replace
	}

}

