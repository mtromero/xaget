clear mata
clear all
set matsize 1100
set seed 1234
local it = 100


use "$base_out\2015t1_FullyLinked_mainTE_own.dta", clear

local controls c.mthly_fees_c1 c.wagecat_D2 c.wagecat_D3 c.ptr_T1DG15 c.male c.DOB_M
local controls2 mthly_fees_c1 wagecat_D2 wagecat_D3 ptr_T1DG15 male DOB_M


local controlspu_ET14DG15 env_T3ET14_Z math_T3ET14_Z env_T3ET14_Z2 math_T3ET14_Z2  env_T3ET14_Z3 math_T3ET14_Z3
local controlsc1_ET14DG15 sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 read_T3ET14_Z env_T3ET14_Z math_T3ET14_Z  kusoma_T3ET14_Z read_T3ET14_Z2 env_T3ET14_Z2 math_T3ET14_Z2  kusoma_T3ET14_Z2 read_T3ET14_Z3 env_T3ET14_Z3 math_T3ET14_Z3 kusoma_T3ET14_Z3
local controlsc5_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3
local controlsc6_ET14DG15 math_T1DG15_Z sci_T1DG15_Z ss_T1DG15_Z eng_T1DG15_Z swahili_T1DG15_Z math_T1DG15_Z2 sci_T1DG15_Z2 ss_T1DG15_Z2 eng_T1DG15_Z2 swahili_T1DG15_Z2 math_T1DG15_Z3 sci_T1DG15_Z3 ss_T1DG15_Z3 eng_T1DG15_Z3 swahili_T1DG15_Z3 lang_T3ET14_Z math_T3ET14_Z sci_T3ET14_Z re_T3ET14_Z ss_T3ET14_Z swahili_T3ET14_Z lang_T3ET14_Z2 math_T3ET14_Z2 sci_T3ET14_Z2 re_T3ET14_Z2 ss_T3ET14_Z2 swahili_T3ET14_Z2 lang_T3ET14_Z3 math_T3ET14_Z3 sci_T3ET14_Z3 re_T3ET14_Z3 ss_T3ET14_Z3 swahili_T3ET14_Z3



keep `controls2' gradename gradename2 dSTD1 dPU dSTD5 dSTD6 academyid former_province2 *_Z treat pupilid IndexET14DG15

reshape long dev_@_Z env_@_Z lang_@_Z math_@_Z kusoma_@_Z re_@_Z read_@_Z sci_@_Z ss_@_Z swahili_@_Z, i(pupilid) j(term) string
drop if term=="T3ET14"
drop if term=="T1DG15"
compress
gen term1=(term=="T1MT15")
gen term2=(term=="T1ET15")
gen term3=(term=="T2MT15")
gen term4=(term=="T2ET15")
gen term5=(term=="T3MT15")

label var dev__Z Development
label var env__Z Enviromental
label var lang__Z Language
label var math__Z Math
label var kusoma__Z Kusoma
label var re__Z RE
label var read__Z Reading
label var sci__Z Science
label var ss__Z "S.S."
label var swahili__Z Swahili
drop kuandika_T2ET15_Z- creative_T1ET15_Z kuandika_T3ET14_Z insha_T3ET14_Z- creative_T3ET14_Z kuandika_T1MT15_Z- eng_T1DG15_Z


keep if  gradename=="PREUNIT" | gradename=="STD 1"
keep *__Z treat academyid former_province2 gradename2 term1- term5 `controls2'
save "$base_out\Student_Pcentile_NP.dta", replace

foreach subject in math read kusoma {
use "$base_out\Student_Pcentile_NP.dta", clear
keep  treat academyid `subject'__Z former_province2 gradename2  term1 - term5 `controls2'
reg `subject'__Z  (i.gradename2 `controls' i.former_province2)##c.(term1 term2 term3 term4 term5)
predict `subject'__Resid, resid 
compress
save "$base_out\temp\TempBoot_Pass", replace

qui egen kernel_range = fill(.01(.01)1)
qui replace kernel_range = . if kernel_range>1
mkmat kernel_range if kernel_range != .
matrix diff = kernel_range
matrix x = kernel_range

quietly forvalues j = 1(1)`it' {
use "$base_out\temp\TempBoot_Pass", clear

bsample, strata(treat former_province2) cluster(academyid)


bysort treat: egen rank`subject' = rank(`subject'__Resid), unique
bysort treat: egen max_rank`subject' = max(rank`subject')
bysort treat: gen PctileLag`subject' = rank`subject'/max_rank`subject' 


egen kernel_range = fill(.01(.01)1)
qui replace kernel_range = . if kernel_range>1

*regressing endline scores on percentile rankings
lpoly `subject'__Resid PctileLag`subject' if treat==0 , gen(xcon pred_con) at (kernel_range) nograph
lpoly `subject'__Resid PctileLag`subject' if treat==1 , gen(xtre pred_tre) at (kernel_range) nograph
	
	
mkmat pred_tre if pred_tre != . 
mkmat pred_con if pred_con != . 
matrix diff = diff, pred_tre - pred_con

}

matrix diff = diff'

*each variable is a percentile that is being estimated (can sort by column to get 2.5th and 97.5th confidence interval)
svmat diff
keep diff* 

matrix conf_int = J(100, 2, 100)
qui drop if _n == 1

*sort each column (percentile) and saving 25th and 975th place in a matrix
forvalues i = 1(1)100{
sort diff`i'
matrix conf_int[`i', 1] = diff`i'[0.025*`it']
matrix conf_int[`i', 2] = diff`i'[0.975*`it']	
}

*******************Graphs for control, treatment, and difference using actual data (BASELINE)*************************************
use "$base_out\temp\TempBoot_Pass", clear
 
bysort treat: egen rank`subject' = rank(`subject'__Resid), unique
bysort treat: egen max_rank`subject' = max(rank`subject')
bysort treat: gen PctileLag`subject' = rank`subject'/max_rank`subject'  

 
egen kernel_range = fill(.01(.01)1)
qui replace kernel_range = . if kernel_range>1

lpoly `subject'__Resid PctileLag`subject' if treat==0 , gen(xcon pred_con) at (kernel_range) nograph
lpoly `subject'__Resid PctileLag`subject' if treat==1 , gen(xtre pred_tre) at (kernel_range) nograph
	
gen diff = pred_tre - pred_con

*variables for confidence interval bands
svmat conf_int



save "$base_out\Lowess_Quantile_`subject'.dta", replace

graph twoway (line pred_con xcon, lcolor(blue) lpattern("--.....") legend(lab(1 "Control"))) ///
(line pred_tre xtre, lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
(line diff xcon, lcolor(black) lpattern(solid) legend(lab(3 "Difference"))) ///
(line conf_int1 xcon, lcolor(black) lpattern(shortdash) legend(lab(4 "95% Confidence Band"))) ///
(line conf_int2 xcon, lcolor(black) lpattern(shortdash) legend(lab(5 "95% Confidence Band"))) ///
,yline(0, lcolor(gs10)) xtitle(Percentile of Endline Score) ytitle(`subject' Endline Score) legend(order(1 2 3 4)) ///
title("Quantile treatment effects for `subject'", size(vsmall))
graph export "$results\Graphs\Lowess_Quantile_`subject'.pdf", as(pdf) replace

}




