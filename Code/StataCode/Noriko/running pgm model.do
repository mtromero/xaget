/***************************************
Pupil Growth Model for Maths Tutoring Programme
***************************************/
global pgm15 "C:\Users\User\Documents\SugarSync Shared Folders\Student Growth Model\PGM 2015\3a. Main Datasets"
global pgmmath "C:\Users\User\Documents\SugarSync Shared Folders\Academic Team Requests\2015.05.04 Pupil Growth Model for Reading Programme AC"
set more off

log using "$pgmmath\log of regression.smcl", replace

//Step 1. bring in raw data. 
use "$pgm15\T1DG15_T1MT15_T1ET15_FINAL_15.04.21.dta", clear
keep if gradename_T1ET15=="STD 1" | gradename_T1ET15=="STD 2" | gradename_T1ET15 == "STD 3" | ///
gradename_T1ET15=="STD 4" | gradename_T1ET15=="STD 5" | gradename_T1ET15=="STD 6"

drop if score_math_c1_T1DG15 ==. & score_math_c2_T1DG15 ==. & score_math_c5_T1DG15 ==. & score_math_c6_T1DG15 ==. & ///
score_eng_c1_T1DG15 ==. & score_eng_c2_T1DG15 ==. & score_eng_c5_T1DG15 ==. & score_eng_c6_T1DG15 ==. & ///
score_sci_c1_T1DG15 ==. & score_sci_c2_T1DG15 ==. & score_sci_c5_T1DG15 ==. & score_sci_c6_T1DG15 ==. & ///
score_ss_c1_T1DG15 ==. & score_ss_c2_T1DG15 ==. & score_ss_c5_T1DG15 ==. & score_ss_c6_T1DG15 ==. & ///
score_math_c1_T1ET15 ==. & score_math_c2_T1ET15 ==. & score_math_c5_T1ET15 ==. & score_math_c6_T1ET15 ==. & ///
score_lang_c1_T1ET15 ==. & score_lang_c2_T1ET15 ==. & score_lang_c5_T1ET15 ==. & score_lang_c6_T1ET15 ==. & ///
score_sci_c1_T1ET15 ==. & score_sci_c2_T1ET15 ==. & score_sci_c5_T1ET15 ==. & score_sci_c6_T1ET15 ==. & ///
score_ss_c1_T1ET15 ==. & score_ss_c2_T1ET15 ==. & score_ss_c5_T1ET15 ==. & score_ss_c6_T1ET15 ==.

/**standardizing score**/
*C1 Maths
egen stdscore_math_c1_T1DG15 = std(score_math_c1_T1DG15)
gen stdscore_math2_c1_T1DG15 = stdscore_math_c1_T1DG15^2
gen stdscore_math3_c1_T1DG15 = stdscore_math_c1_T1DG15^3

*C1 Language
egen stdscore_lang_c1_T1DG15 = std(score_eng_c1_T1DG15)
gen stdscore_lang2_c1_T1DG15 = stdscore_lang_c1_T1DG15^2
gen stdscore_lang3_c1_T1DG15 = stdscore_lang_c1_T1DG15^3

*C1 Science
egen stdscore_sci_c1_T1DG15 = std(score_sci_c1_T1DG15)
gen stdscore_sci2_c1_T1DG15 = stdscore_sci_c1_T1DG15^2
gen stdscore_sci3_c1_T1DG15 = stdscore_sci_c1_T1DG15^3

*C1 Social Studies
egen stdscore_ss_c1_T1DG15 = std(score_ss_c1_T1DG15)
gen stdscore_ss2_c1_T1DG15 = stdscore_ss_c1_T1DG15^2
gen stdscore_ss3_c1_T1DG15 = stdscore_ss_c1_T1DG15^3

*C2 Maths
egen stdscore_math_c2_T1DG15 = std(score_math_c2_T1DG15)
gen stdscore_math2_c2_T1DG15 = stdscore_math_c2_T1DG15^2
gen stdscore_math3_c2_T1DG15 = stdscore_math_c2_T1DG15^3

*C2 Language
egen stdscore_lang_c2_T1DG15 = std(score_eng_c2_T1DG15)
gen stdscore_lang2_c2_T1DG15 = stdscore_lang_c2_T1DG15^2
gen stdscore_lang3_c2_T1DG15 = stdscore_lang_c2_T1DG15^3

*C2 Science
egen stdscore_sci_c2_T1DG15 = std(score_sci_c2_T1DG15)
gen stdscore_sci2_c2_T1DG15 = stdscore_sci_c2_T1DG15^2
gen stdscore_sci3_c2_T1DG15 = stdscore_sci_c2_T1DG15^3

*C2 Social Studies
egen stdscore_ss_c2_T1DG15 = std(score_ss_c2_T1DG15)
gen stdscore_ss2_c2_T1DG15 = stdscore_ss_c2_T1DG15^2
gen stdscore_ss3_c2_T1DG15 = stdscore_ss_c2_T1DG15^3

*C5 Maths
egen stdscore_math_c5_T1DG15 = std(score_math_c5_T1DG15)
gen stdscore_math2_c5_T1DG15 = stdscore_math_c5_T1DG15^2
gen stdscore_math3_c5_T1DG15 = stdscore_math_c5_T1DG15^3

*C5 Language
egen stdscore_lang_c5_T1DG15 = std(score_eng_c5_T1DG15)
gen stdscore_lang2_c5_T1DG15 = stdscore_lang_c5_T1DG15^2
gen stdscore_lang3_c5_T1DG15 = stdscore_lang_c5_T1DG15^3

*C5 Science
egen stdscore_sci_c5_T1DG15 = std(score_sci_c5_T1DG15)
gen stdscore_sci2_c5_T1DG15 = stdscore_sci_c5_T1DG15^2
gen stdscore_sci3_c5_T1DG15 = stdscore_sci_c5_T1DG15^3

*C5 Social Studies
egen stdscore_ss_c5_T1DG15 = std(score_ss_c5_T1DG15)
gen stdscore_ss2_c5_T1DG15 = stdscore_ss_c5_T1DG15^2
gen stdscore_ss3_c5_T1DG15 = stdscore_ss_c5_T1DG15^3

*C6 Maths
egen stdscore_math_c6_T1DG15 = std(score_math_c6_T1DG15)
gen stdscore_math2_c6_T1DG15 = stdscore_math_c6_T1DG15^2
gen stdscore_math3_c6_T1DG15 = stdscore_math_c6_T1DG15^3

*C6 Language
egen stdscore_lang_c6_T1DG15 = std(score_eng_c6_T1DG15)
gen stdscore_lang2_c6_T1DG15 = stdscore_lang_c6_T1DG15^2
gen stdscore_lang3_c6_T1DG15 = stdscore_lang_c6_T1DG15^3

*C6 Science
egen stdscore_sci_c6_T1DG15 = std(score_sci_c6_T1DG15)
gen stdscore_sci2_c6_T1DG15 = stdscore_sci_c6_T1DG15^2
gen stdscore_sci3_c6_T1DG15 = stdscore_sci_c6_T1DG15^3

*C6 Social Studies
egen stdscore_ss_c6_T1DG15 = std(score_ss_c6_T1DG15)
gen stdscore_ss2_c6_T1DG15 = stdscore_ss_c6_T1DG15^2
gen stdscore_ss3_c6_T1DG15 = stdscore_ss_c6_T1DG15^3

*Endterm Maths
egen stdscore_math_c1_T1ET15 = std(score_math_c1_T1ET15)
egen stdscore_math_c2_T1ET15 = std(score_math_c2_T1ET15)
egen stdscore_math_c5_T1ET15 = std(score_math_c5_T1ET15)
egen stdscore_math_c6_T1ET15 = std(score_math_c6_T1ET15)

*Endterm Language
egen stdscore_lang_c1_T1ET15 = std(score_lang_c1_T1ET15)
egen stdscore_lang_c2_T1ET15 = std(score_lang_c2_T1ET15)
egen stdscore_lang_c5_T1ET15 = std(score_lang_c5_T1ET15)
egen stdscore_lang_c6_T1ET15 = std(score_lang_c6_T1ET15)

*Endterm Science
egen stdscore_sci_c1_T1ET15 = std(score_sci_c1_T1ET15)
egen stdscore_sci_c2_T1ET15 = std(score_sci_c2_T1ET15)
egen stdscore_sci_c5_T1ET15 = std(score_sci_c5_T1ET15)
egen stdscore_sci_c6_T1ET15 = std(score_sci_c6_T1ET15)

*Endterm SS
egen stdscore_ss_c1_T1ET15 = std(score_ss_c1_T1ET15)
egen stdscore_ss_c2_T1ET15 = std(score_ss_c2_T1ET15)
egen stdscore_ss_c5_T1ET15 = std(score_ss_c5_T1ET15)
egen stdscore_ss_c6_T1ET15 = std(score_ss_c6_T1ET15)


//Step 2 Creating new variables for standardized scores.


gen score_math_T1DG15=.
gen score_sci_T1DG15=.
gen score_ss_T1DG15=.
gen score_lang_T1DG15=.

gen score_math2_T1DG15=.
gen score_sci2_T1DG15=.
gen score_ss2_T1DG15=.
gen score_lang2_T1DG15=.

gen score_math3_T1DG15=.
gen score_sci3_T1DG15=.
gen score_ss3_T1DG15=.
gen score_lang3_T1DG15=.

gen score_math_T1ET15=.
gen score_sci_T1ET15=.
gen score_ss_T1ET15=.
gen score_lang_T1ET15=.

foreach i in 1 2 5 6 {
replace score_math_T1DG15=stdscore_math_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_sci_T1DG15=stdscore_sci_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_lang_T1DG15=stdscore_lang_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_ss_T1DG15 =stdscore_ss_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_math2_T1DG15=stdscore_math2_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_sci2_T1DG15=stdscore_sci2_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_lang2_T1DG15=stdscore_lang2_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_ss2_T1DG15 =stdscore_ss2_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_math3_T1DG15=stdscore_math3_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_sci3_T1DG15=stdscore_sci3_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_lang3_T1DG15=stdscore_lang3_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
replace score_ss3_T1DG15 =stdscore_ss3_c`i'_T1DG15 if gradename_T1DG15=="STD `i'"
}


foreach i in 1 2 5 6 {
replace score_math_T1ET15=stdscore_math_c`i'_T1ET15 if gradename_T1ET15=="STD `i'"
replace score_sci_T1ET15=stdscore_sci_c`i'_T1ET15 if gradename_T1ET15=="STD `i'"
replace score_ss_T1ET15=stdscore_ss_c`i'_T1ET15 if gradename_T1ET15=="STD `i'"
replace score_lang_T1ET15=stdscore_lang_c`i'_T1ET15 if gradename_T1ET15=="STD `i'"
}

//Step 3. create a dummy variable.

*Tag RCT treatment vs. control group.

gen rct=.

replace rct=1 if ///
	academyid_T1ET15=="Ndumberi-KBU" | ///
	academyid_T1ET15=="Ndorome-KIR" | ///
	academyid_T1ET15=="Sagana-KIR" | ///
	academyid_T1ET15=="Kamirithu-KBU" | ///
	academyid_T1ET15=="Murang'a-MRA" | ///
	academyid_T1ET15=="Kagio-KIR" | ///
	academyid_T1ET15=="Wang'uru-KIR" | ///
	academyid_T1ET15=="Mairo Inya-NDR" | ///
	academyid_T1ET15=="Njabini-NDR" | ///
	academyid_T1ET15=="Karatina-NER" | ///
	academyid_T1ET15=="Kerugoya-KIR" | ///
	academyid_T1ET15=="Ngarariga-KBU" | ///
	academyid_T1ET15=="Gikambura-KBU" | ///
	academyid_T1ET15=="Banana-KBU" | ///
	academyid_T1ET15=="Thiba-KIR" | ///
	academyid_T1ET15=="Gatundu-KBU" | ///
	academyid_T1ET15=="Gachie-KBU" | ///
	academyid_T1ET15=="Naro Moru-NER" | ///
	academyid_T1ET15=="Kirathimo-KBU" | ///
	academyid_T1ET15=="Ruiru-KBU" | ///
	academyid_T1ET15=="Thogoto-KBU" | ///
	academyid_T1ET15=="Gachororo-KBU" | ///
	academyid_T1ET15=="Othaya-NER" | ///
	academyid_T1ET15=="Kiandutu-KBU" | ///
	academyid_T1ET15=="Ngangarithi-NER" | ///
	academyid_T1ET15=="Watamu-KLF" | ///
	academyid_T1ET15=="Ng'ombeni-KLE" | ///
	academyid_T1ET15=="Owino Ouru-MBA" | ///
	academyid_T1ET15=="Umoja-KLE"
replace rct=1 if ///
	academyid_T1ET15=="Mnarani-KLF" | ///
	academyid_T1ET15=="Kinango-KLE" | ///
	academyid_T1ET15=="Kanamai-KLF" | ///
	academyid_T1ET15=="Leisure-MBA" | ///
	academyid_T1ET15=="Kwa Hola-MBA" | ///
	academyid_T1ET15=="Ujamaa-MBA" | ///
	academyid_T1ET15=="Kwale-KLE" | ///
	academyid_T1ET15=="Consolata-MBA" | ///
	academyid_T1ET15=="Kona Jara-MBA" | ///
	academyid_T1ET15=="Kaloleni-KLF" | ///
	academyid_T1ET15=="Mariakani-KLF" | ///
	academyid_T1ET15=="Kiembeni-MBA" | ///
	academyid_T1ET15=="Mtangani-KLF" | ///
	academyid_T1ET15=="Takaye-KLF" | ///
	academyid_T1ET15=="Mtopanga-MBA" | ///
	academyid_T1ET15=="Mwakingali-TVT" | ///
	academyid_T1ET15=="Utange-MBA" | ///
	academyid_T1ET15=="Mwamambi-KLE" | ///
	academyid_T1ET15=="Lamu-LAU" | ///
	academyid_T1ET15=="Kibundani-KLE" | ///
	academyid_T1ET15=="Bura-TAN" | ///
	academyid_T1ET15=="Maji Safi-MBA" | ///
	academyid_T1ET15=="Shanzu-MBA" | ///
	academyid_T1ET15=="Dallas-EBU" | ///
	academyid_T1ET15=="Timau-MRU" | ///
	academyid_T1ET15=="Tala-MCS" | ///
	academyid_T1ET15=="Kooje-MRU"
replace rct=1 if ///
	academyid_T1ET15=="Kangundo-MCS" | ///
	academyid_T1ET15=="Kambi Juu-ISL" | ///
	academyid_T1ET15=="Maua-MRU" | ///
	academyid_T1ET15=="Kibwezi-MUE" | ///
	academyid_T1ET15=="Kitui-KTU" | ///
	academyid_T1ET15=="Chuka-TNT" | ///
	academyid_T1ET15=="Magundu-MRU" | ///
	academyid_T1ET15=="Wote-MUE" | ///
	academyid_T1ET15=="Makindu-MUE" | ///
	academyid_T1ET15=="Dandora 5-NBO" | ///
	academyid_T1ET15=="Tassia-NBO" | ///
	academyid_T1ET15=="Kwa Njenga Mosque-NBO" | ///
	academyid_T1ET15=="Baba Dogo-NBO" | ///
	academyid_T1ET15=="Kimbo-NBO" | ///
	academyid_T1ET15=="Satellite-NBO" | ///
	academyid_T1ET15=="Kingston-NBO" | ///
	academyid_T1ET15=="Gicagi-NBO" | ///
	academyid_T1ET15=="Muthua-NBO" | ///
	academyid_T1ET15=="Korogocho-NBO" | ///
	academyid_T1ET15=="Waithaka-NBO" | ///
	academyid_T1ET15=="Maili Saba-NBO" | ///
	academyid_T1ET15=="Kiambiu-NBO" | ///
	academyid_T1ET15=="Magena-KSI" | ///
	academyid_T1ET15=="Siany-KIS" | ///
	academyid_T1ET15=="Awendo-MIG" | ///
	academyid_T1ET15=="Ahero-KIS" | ///
	academyid_T1ET15=="Awasi-KIS" | ///
	academyid_T1ET15=="Bomondo-NYI" | ///
	academyid_T1ET15=="Hollywood-KIS" | ///
	academyid_T1ET15=="Oruba-MIG" | ///
	academyid_T1ET15=="Nyamasaria-KIS" | ///
	academyid_T1ET15=="Keumbu-KSI" | ///
	academyid_T1ET15=="Oyugis-HMA" | ///
	academyid_T1ET15=="Getare-KSI" | ///
	academyid_T1ET15=="Kibos-KIS"
replace rct=1 if ///
	academyid_T1ET15=="Ekerenyo-NYI" | ///
	academyid_T1ET15=="Nyamataro-KSI" | ///
	academyid_T1ET15=="Ikonge-NYI" | ///
	academyid_T1ET15=="Katito-KIS" | ///
	academyid_T1ET15=="Wasweta-MIG" | ///
	academyid_T1ET15=="Isibania-MIG" | ///
	academyid_T1ET15=="Shauri Yako-HMA" | ///
	academyid_T1ET15=="Picadilly-KIS" | ///
	academyid_T1ET15=="Mwembe-KSI" | ///
	academyid_T1ET15=="Suneka-KSI" | ///
	academyid_T1ET15=="Muhoroni-KIS" | ///
	academyid_T1ET15=="Usenge-SYA" | ///
	academyid_T1ET15=="Tabaka-KSI" | ///
	academyid_T1ET15=="Rongo-MIG" | ///
	academyid_T1ET15=="Getembe-KSI" | ///
	academyid_T1ET15=="Sori-MIG" | ///
	academyid_T1ET15=="Keroka-NYI" | ///
	academyid_T1ET15=="Heshima-NKU" | ///
	academyid_T1ET15=="Kihoto-NKU" | ///
	academyid_T1ET15=="Machine-NKU" | ///
	academyid_T1ET15=="Munyaka-USG" | ///
	academyid_T1ET15=="Chebilat-BMT" | ///
	academyid_T1ET15=="Mawanga-NKU" | ///
	academyid_T1ET15=="Eldama Ravine-BAR" | ///
	academyid_T1ET15=="Njoro-NKU" | ///
	academyid_T1ET15=="Elburgon-NKU" | ///
	academyid_T1ET15=="Turbo-USG" | ///
	academyid_T1ET15=="Kabarnet-BAR" | ///
	academyid_T1ET15=="Panama-USG" | ///
	academyid_T1ET15=="Mai Mahiu-NKU" | ///
	academyid_T1ET15=="Free Area-NKU" | ///
	academyid_T1ET15=="Rongai-KAJ" | ///
	academyid_T1ET15=="Likii-LKP" | ///
	academyid_T1ET15=="Bomet-BMT"
replace rct=1 if ///
	academyid_T1ET15=="Kapkatet-KCO" | ///
	academyid_T1ET15=="Kapsabet-NDI" | ///
	academyid_T1ET15=="Mogotio-BAR" | ///
	academyid_T1ET15=="Umoja-NKU" | ///
	academyid_T1ET15=="Kipkarren River-NDI" | ///
	academyid_T1ET15=="Huruma-USG" | ///
	academyid_T1ET15=="Rhonda-NKU" | ///
	academyid_T1ET15=="Nakuru West-NKU" | ///
	academyid_T1ET15=="Sotik-BMT" | ///
	academyid_T1ET15=="Kamukunji-USG" | ///
	academyid_T1ET15=="Kware-NKU" | ///
	academyid_T1ET15=="Kimumu-USG" | ///
	academyid_T1ET15=="Maji Mazuri-BAR" | ///
	academyid_T1ET15=="Makutano-PKT" | ///
	academyid_T1ET15=="Kaptembwo-NKU" | ///
	academyid_T1ET15=="Kona Mbaya-USG" | ///
	academyid_T1ET15=="Kiamunyeki-NKU" | ///
	academyid_T1ET15=="Tuwani-TRN" | ///
	academyid_T1ET15=="Jasho Nyingi-NKU" | ///
	academyid_T1ET15=="Illasit-KAJ" | ///
	academyid_T1ET15=="Karagita-NKU" | ///
	academyid_T1ET15=="Malaba-BSA" | ///
	academyid_T1ET15=="Cheptais-BGM"
replace rct=1 if ///
	academyid_T1ET15=="Mjini-BGM" | ///
	academyid_T1ET15=="Luanda-VHG" | ///
	academyid_T1ET15=="Webuye-BGM" | ///
	academyid_T1ET15=="Butere-KAK" | ///
	academyid_T1ET15=="Scheme-KAK" | ///
	academyid_T1ET15=="Kimilili-BGM" | ///
	academyid_T1ET15=="Cheptulu-VHG" | ///
	academyid_T1ET15=="Chwele-BGM" | ///
	academyid_T1ET15=="Shibale-KAK" | ///
	academyid_T1ET15=="Misikhu-BGM" | ///
	academyid_T1ET15=="Kamukuywa-BGM" | ///
	academyid_T1ET15=="Majengo-VHG" | ///
	academyid_T1ET15=="Kefinco-KAK" | ///
	academyid_T1ET15=="Kapsokwony-BGM" | ///
	academyid_T1ET15=="Ojamii-BSA" | ///
	academyid_T1ET15=="Sichirai-KAK" | ///
	academyid_T1ET15=="Funyula-BSA" | ///
	academyid_T1ET15=="Malava-KAK"

replace rct=0 if ///
	academyid_T1ET15=="Skuta-NER" | ///
	academyid_T1ET15=="Kutus-KIR" | ///
	academyid_T1ET15=="Karia-KBU" | ///
	academyid_T1ET15=="Kamakwa-NER" | ///
	academyid_T1ET15=="Miami-MBA" | ///
	academyid_T1ET15=="Mwatate-TVT" | ///
	academyid_T1ET15=="Majengo Mapya-MBA" | ///
	academyid_T1ET15=="Kaloleni-TVT" | ///
	academyid_T1ET15=="Mishomoroni-MBA" | ///
	academyid_T1ET15=="Checheles-ISL" | ///
	academyid_T1ET15=="Emali-MUE" | ///
	academyid_T1ET15=="Sinai-NBO" | ///
	academyid_T1ET15=="Dandora 4-NBO" | ///
	academyid_T1ET15=="Bandani-KIS" | ///
	academyid_T1ET15=="Kendu Bay-HMA" | ///
	academyid_T1ET15=="Nyawita-KIS" | ///
	academyid_T1ET15=="Nyamache-KSI" | ///
	academyid_T1ET15=="Mbita-HMA" | ///
	academyid_T1ET15=="Nyagacho-KCO" | ///
	academyid_T1ET15=="Kipkaren-USG" | ///
	academyid_T1ET15=="Gicagi-KAJ" | ///
	academyid_T1ET15=="Road Block-USG" | ///
	academyid_T1ET15=="Rumuruti-LKP" | ///
	academyid_T1ET15=="Londiani-KCO" | ///
	academyid_T1ET15=="Mitume-TRN" | ///
	academyid_T1ET15=="Mbale-VHG" | ///
	academyid_T1ET15=="Bukembe-BGM" | ///
	academyid_T1ET15=="Shamakhokho-VHG"
	
	*prep for regression
	gen PU=0
	replace PU=1 if gradename_T1ET15=="PREUNIT"
	gen C1=0
	replace C1=1 if gradename_T1ET15=="STD 1"
	gen C2=0
	replace C2=1 if gradename_T1ET15=="STD 2"
	gen C3=0
	replace C3=1 if gradename_T1ET15=="STD 3"
	gen C4=0
	replace C4=1 if gradename_T1ET15=="STD 4"
	gen C5=0
	replace C5=1 if gradename_T1ET15=="STD 5"
	gen C6=0
	replace C6=1 if gradename_T1ET15=="STD 6"
	gen C7=0
	replace C7=1 if gradename_T1ET15=="STD 7"
	gen C8=0
	replace C8=1 if gradename_T1ET15=="STD 8"
	
	foreach grade in PU C1 C2 C3 C4 C5 C6 C7 C8 {
	gen rct_`grade'=rct*`grade'
	}

//Step 4. Running regression analysis.

regress score_sci_T1ET15 score_sci_T1DG15 score_sci2_T1DG15 score_sci3_T1DG15 C1 C5 C6 rct_C1 rct_C5 rct_C6 if score_sci_T1DG15!=. & score_sci_T1ET15!=., cluster(academyid_T1ET15)
regress score_math_T1ET15 score_math_T1DG15 score_math2_T1DG15 score_math3_T1DG15 C1 C5 C6 rct_C1 rct_C5 rct_C6 if score_math_T1DG15!=. & score_math_T1ET15!=., cluster(academyid_T1ET15)
regress score_ss_T1ET15 score_ss_T1DG15 score_ss2_T1DG15 score_ss3_T1DG15 C1 C5 C6 rct_C1 rct_C5 rct_C6 if score_ss_T1DG15!=. & score_ss_T1ET15!=., cluster(academyid_T1ET15)
regress score_lang_T1ET15 score_lang_T1DG15 score_lang2_T1DG15 score_lang3_T1DG15 C1 C5 C6 rct_C1 rct_C5 rct_C6 if score_lang_T1DG15!=. & score_lang_T1ET15!=., cluster(academyid_T1ET15)

save "$pgmmath\3. Output\pupil growth model for reading programme.dta", replace
log close
clear


