use "C:\Users\Mauricio\Downloads\KE12_hhld\KE12_hhld.dta", clear
drop if grade!=1
keep english_imputed id_regionName
gen const=1
collapse (sum) const, by(english_imputed id_regionName)
drop if missing( english_imputed)
reshape wide const , i( id_regionName ) j(english_imputed)
egen Total=rowtotal(const1 const2 const3 const4 const5)
forvalues i=1/5{
replace const`i'=const`i'/Total
}
drop Total

use "C:\Users\Mauricio\Downloads\KE12_hhld\KE12_hhld.dta", clear
drop if grade!=1
keep swahili_imputed id_regionName
gen const=1
collapse (sum) const, by(swahili_imputed id_regionName)
drop if missing( swahili_imputed)
reshape wide const , i( id_regionName ) j(swahili_imputed)
egen Total=rowtotal(const1 const2 const3 const4 const5)
forvalues i=1/5{
replace const`i'=const`i'/Total
}
drop Total
