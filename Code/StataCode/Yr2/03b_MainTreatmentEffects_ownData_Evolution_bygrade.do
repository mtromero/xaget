
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period
encode academyid, gen(academyid2)
/*

gen term6=(term=="T3ET16")
*/
	

		
	foreach subject in English math {
		local label : var label percentscore_Z
		
		
		if("`subject'"!="SwaLanguage" & "`subject'"!="SwaCompReading"){				
		reghdfe percentscore_Z c.treatment#c.(term*) $control $controls_prescores if  dNU==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_NU_control.pdf", replace
		
		reghdfe percentscore_Z c.treatment#c.(term*) $control $controls_prescores if  dBC==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_BC_control.pdf", replace
		}
		reghdfe percentscore_Z c.treatment#c.(term*) $control  $controls_prescores if  dPU==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_PU_control.pdf", replace
		
		reghdfe percentscore_Z c.treatment#c.(term*) $control $controls_prescores if  dc1==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C1_control.pdf", replace
		
		
		reghdfe percentscore_Z c.treatment#c.(term*) $control $controls_prescores if  dc2==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C2_control.pdf", replace
		
		
		
	}
	
	
	
	


		
	foreach subject in English math {
		local label : var label percentscore_Z
		
			
		
		reghdfe percentscore_Z c.treatment#c.(term*) $controls_old $controls_prescores if  dc3==1 & Subject=="`subject'", vce(cluster academyid2)
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C3_controls.pdf", replace
		
		reghdfe percentscore_Z c.treatment#c.(term*) $controls_old  $controls_prescores if  dc4==1 & Subject=="`subject'", vce(cluster academyid2)
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C4_controls.pdf", replace
		
		reghdfe percentscore_Z c.treatment#c.(term*)  $controls_old $controls_prescores if  dc5==1 & Subject=="`subject'", vce(cluster academyid2)
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C5_controls.pdf", replace
		
		reghdfe percentscore_Z c.treatment#c.(term*) $controls_old $controls_prescores if  dc6==1 & Subject=="`subject'", vce(cluster academyid2)
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C6_controls.pdf", replace
		
		
		reghdfe percentscore_Z c.treatment#c.(term*)  $controls_old  $controls_prescores if  dc7==1 & Subject=="`subject'", vce(cluster academyid2)
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Treatment effect") title("Evolution of treatment for `subject'")
		graph export "$results/Graphs/Evol_`subject'_C7_controls.pdf", replace
		
	}
	
	
