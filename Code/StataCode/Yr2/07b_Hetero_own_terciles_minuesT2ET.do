
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 GradeName2016 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors *tutor

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
drop if period=="T2ET16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)

encode academyid, gen(academyid2)

foreach var in English_T3ET15_Z Swahili_T3ET15_Z math_T3ET15_Z lang_T1DG16_Z math_T1DG16_Z swa_T1DG16_Z{
egen terc_`var' = fastxtile(`var'), by(gradename2 period) nq(3)
}

sort academyid2 gradename2 period English_T3ET15_Z
gen wgt=0
replace wgt=1 if English_T3ET15_Z!=.
by academyid2 gradename2 period : egen sumwgt = sum(wgt)
by academyid2 gradename2 period : gen rsum = sum(wgt)
by academyid2 gradename2 period : gen ptile = int(100*rsum/sumwgt)



foreach var in English_T3ET15_Z Swahili_T3ET15_Z math_T3ET15_Z lang_T1DG16_Z math_T1DG16_Z swa_T1DG16_Z{
egen tercwithin_`var' = fastxtile (`var'), by(academyid2 gradename2 period) nq(3)
}
drop period
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/
**PUPILS

rename terc_lang_T1DG16_Z terc_English_T1DG16_Z
rename terc_swa_T1DG16_Z  terc_Swahili_T1DG16_Z
rename tercwithin_lang_T1DG16_Z  tercwithin_English_T1DG16_Z
rename tercwithin_swa_T1DG16_Z tercwithin_Swahili_T1DG16_Z

/*
reghdfe percentscore_Z c.treatment#i.terc_math_T3ET15_Z i.terc_math_T3ET15_Z $controls $controls_prescores if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z c.treatment#i.tercwithin_math_T3ET15_Z i.tercwithin_math_T3ET15_Z $controls $controls_prescores if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
fvset base 1 terc_math_T3ET15_Z
char terc_math_T3ET15_Z[omit] 1
fvset base 1 tercwithin_math_T3ET15_Z
char tercwithin_math_T3ET15_Z[omit] 1
	
reghdfe percentscore_Z c.treatment $controls $controls_prescores if pupils==1 & Subject=="English", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z c.treatment $controls $controls_prescores if pupils==1 & Subject=="English" & terc_English_T3ET15_Z!=., vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z c.treatment#i.terc_English_T3ET15_Z i.terc_English_T3ET15_Z $controls $controls_prescores if pupils==1 & Subject=="English", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z c.treatment#i.terc_English_T3ET15_Z  $controls $controls_prescores if pupils==1 & Subject=="English", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
*/

foreach time in T1DG16 T3ET15{
foreach kind in terc tercwithin{

eststo clear
foreach subject in math English {
    capture drop Tercile
	gen Tercile=`kind'_`subject'_`time'_Z
	replace Tercile=0 if Tercile==.
	fvset base 1 Tercile
    char Tercile[omit] 1
	label var Tercile "Tercile"
	
		eststo: reghdfe percentscore_Z c.treatment#i.Tercile i.Tercile $controls $controls_prescores if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
		drop Tercile
	}

				
	esttab  using "$results/LatexCode/het_minus2TET_pupils_`kind'_`time'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace keep(*#c.treatment ) nomtitles stats() ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}
}
	
