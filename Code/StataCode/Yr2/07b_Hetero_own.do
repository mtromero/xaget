
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors *tutor

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period
encode academyid, gen(academyid2)
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/
**PUPILS

label var Pupils_S "Enrollment"
label var Teachers_S "Teachers"
label var PTR_S "PTR"
label var TTR "TTR"

foreach var of varlist DateOfBirth Male AgeFirstBridge Pupils_S Teachers_S PTR_S TTR{
	sum `var' if treatment==0 & pupils==1
	replace `var'=(`var'-r(mean))/r(sd)

	
	eststo clear
	foreach subject in $Outcomes_pupils {
	
		eststo: reghdfe percentscore_Z c.treatment##c.`var' $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
	}
				
	esttab  using "$results/LatexCode/het_all_pupils_`var'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace keep(treatment `var' c.treatment#c.`var') nomtitles stats() ///
	nonotes 
	
	/*
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reg percentscore_Z c.treatment##c.`var' (i.gradename2 $controls $controls_prescores )#c.(term*) formerprovince2##stratascore if   (dc1==1 | dc2==1) & Subject=="`subject'", vce(cluster academyid)
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
	}
				
	esttab  using "$results/LatexCode/het_older_pupils_`var'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace keep(treatment `var' c.treatment#c.`var') stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	*/
}
	
