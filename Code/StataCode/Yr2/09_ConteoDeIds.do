
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR
/*
gen post=(period>=2)

reghdfe percentscore_Z c.treatment#c.post  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab(pupilid period)
*/



drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
drop if period=="T2ET16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
drop period


encode academyid, gen(academyid2)

foreach var in dPU dNU dBC dc1 dc2 dc3 dc4 dc5 dc6 dc7{
	distinct pupilid if `var'==1 & percentscore_Z!=.
	
	local tempm=string(r(ndistinct), "%9.2gc")
	file open newfile using "$results/LatexCode//N_Ids_`var'.tex", write replace
	file write newfile "`tempm'"
	file close newfile	
}
