
use "$basein\Fully_Linked_2015.dta",clear
drop *teach*
merge 1:1 pupilid using "$basein\Fully_Linked_2015_T3ET14.dta", keepus(*T3ET14)
drop if flag_T1DG15_T3ET15 > 0
drop _merge
foreach year in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
	foreach year2 in  T1DG15 T1MT15 T1ET15 T2MT15 T2ET15 T3MT15 T3ET15{
		replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
		replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
		replace academyid_`year2'="Mariakani-KLF" if academyid_`year2'=="Mariakani-MBA"
		replace academyid_`year2'="Ugunja-SYA" if academyid_`year2'=="Ugunja-KIS"
		drop if academyid_`year'!=academyid_`year2' &  academyid_`year'!="" &  academyid_`year2'!=""
	}
}
foreach year in  T3MT15 T2ET15 T2MT15 T1ET15 T1MT15 T1DG15 T3ET14 {
	replace academyid_`year'="Mariakani-KLF" if academyid_`year'=="Mariakani-MBA"
	replace academyid_`year'="Ugunja-SYA" if academyid_`year'=="Ugunja-KIS"
	replace academyid_T3ET15=academyid_`year' if academyid_T3ET15=="" & academyid_`year'!=""
}
rename academyid_T3ET15 academyid
keep pupilid percentscore* academyid
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
replace academyid="Mazeras-KLF" if academyid=="Mazeras-MBA"
merge m:1 academyid using "$base_out/RandomizationXageT.dta", keepus(academyid)
drop if _merge==1
drop _merge academyid

rename percentscore_*_ba_* percentscore_*_BA_*
rename percentscore_*_ns_* percentscore_*_NS_*
rename percentscore_*_pu_* percentscore_*_PU_*
*percentscore_@_c3_T1ET15  percentscore_@_c4_T1ET15
reshape long  percentscore_@_BA_T1ET15 percentscore_@_c1_T1ET15 percentscore_@_c2_T1ET15 percentscore_@_c3_T1ET15 percentscore_@_c4_T1ET15 ///
percentscore_@_c5_T1ET15 percentscore_@_c6_T1ET15 percentscore_@_c7_T1ET15 percentscore_@_c8_T1ET15 ///
percentscore_@_NS_T1ET15 percentscore_@_PU_T1ET15 ///
percentscore_@_BA_T1MT15 percentscore_@_c1_T1MT15 percentscore_@_c2_T1MT15 percentscore_@_c3_T1MT15 percentscore_@_c4_T1MT15 ///
percentscore_@_c5_T1MT15 percentscore_@_c6_T1MT15 percentscore_@_c7_T1MT15 percentscore_@_c8_T1MT15 ///
percentscore_@_NS_T1MT15 percentscore_@_PU_T1MT15 ///
percentscore_@_BA_T2MT15 percentscore_@_c1_T2MT15 percentscore_@_c2_T2MT15 percentscore_@_c3_T2MT15 percentscore_@_c4_T2MT15 ///
percentscore_@_c5_T2MT15 percentscore_@_c6_T2MT15 percentscore_@_c7_T2MT15 percentscore_@_c8_T2MT15 ///
percentscore_@_NS_T2MT15 percentscore_@_PU_T2MT15 ///
percentscore_@_BA_T2ET15 percentscore_@_c1_T2ET15 percentscore_@_c2_T2ET15 percentscore_@_c3_T2ET15 percentscore_@_c4_T2ET15 ///
percentscore_@_c5_T2ET15 percentscore_@_c6_T2ET15 percentscore_@_c7_T2ET15 percentscore_@_c8_T2ET15 ///
percentscore_@_NS_T2ET15 percentscore_@_PU_T2ET15 ///
percentscore_@_BA_T3ET15 percentscore_@_c1_T3ET15 percentscore_@_c2_T3ET15 percentscore_@_c3_T3ET15 percentscore_@_c4_T3ET15 ///
percentscore_@_c5_T3ET15 percentscore_@_c6_T3ET15 percentscore_@_c7_T3ET15 percentscore_@_c8_T3ET15 ///
percentscore_@_NS_T3ET15 percentscore_@_PU_T3ET15 ///
percentscore_@_BA_T3MT15 percentscore_@_c1_T3MT15 percentscore_@_c2_T3MT15 percentscore_@_c3_T3MT15 percentscore_@_c4_T3MT15 ///
percentscore_@_c5_T3MT15 percentscore_@_c6_T3MT15 percentscore_@_c7_T3MT15 percentscore_@_c8_T3MT15 ///
percentscore_@_NS_T3MT15 percentscore_@_PU_T3MT15 ///
percentscore_@_BA_T1DG15 percentscore_@_c1_T1DG15 percentscore_@_c2_T1DG15 percentscore_@_c3_T1DG15 percentscore_@_c4_T1DG15 ///
percentscore_@_c5_T1DG15 percentscore_@_c6_T1DG15 percentscore_@_c7_T1DG15 percentscore_@_c8_T1DG15 ///
percentscore_@_NS_T1DG15 percentscore_@_PU_T1EDG5 ///
percentscore_@_BA_T3ET14 percentscore_@_c1_T3ET14 percentscore_@_c2_T3ET14 percentscore_@_c3_T3ET14 percentscore_@_c4_T3ET14 ///
percentscore_@_c5_T3ET14 percentscore_@_c6_T3ET14 percentscore_@_c7_T3ET14 percentscore_@_c8_T3ET14 ///
percentscore_@_NS_T3ET14 percentscore_@_PU_T3ET14 ///
, i(pupilid) j(Subject) string
rename percentscore__* percentscore_*
rename percentscore_PU_T1EDG5 percentscore_PU_T1DG5
reshape long  percentscore_@_T3ET14 percentscore_@_T1DG15  percentscore_@_T1MT15 percentscore_@_T1ET15 percentscore_@_T2MT15 percentscore_@_T2ET15 percentscore_@_T3MT15 percentscore_@_T3ET15, i(pupilid Subject) j(grade) string
rename percentscore__* percentscore_*

reshape long  percentscore_@, i(pupilid Subject grade) j(period) string
rename percentscore_ percentscore
drop if percentscore==.
compress
rename pupilid PupilID 
rename grade GradeName 
replace GradeName="Baby Class" if  GradeName=="BA"
replace GradeName="Nursery" if  GradeName=="NS"
replace GradeName="PREUNIT" if  GradeName=="PU"
replace GradeName="STD 1" if  GradeName=="c1"
replace GradeName="STD 2" if  GradeName=="c2"
replace GradeName="STD 3" if  GradeName=="c3"
replace GradeName="STD 4" if  GradeName=="c4"
replace GradeName="STD 5" if  GradeName=="c5"
replace GradeName="STD 6" if  GradeName=="c6"
replace GradeName="STD 7" if  GradeName=="c7"
replace GradeName="STD 8" if  GradeName=="c8"

encode period, gen (period2)

recode period2 (7=0)  (8=-1) (4=-2) (5=-3) (2=-4) (3=-5) (1=-6) (6=-7) 
drop if period2==-6 | period2==-7
label define per_lab 0 "T3ET15" -1 "T3MT15" -2 "T2ET15" -3 "T2MT15" -4 "T1ET15" -5 "T1MT15" -6 "T1DG15" -7 "T3ET15", replace
label values period2 per_lab
drop period
rename period2 period
drop GradeName
replace percentscore=100 if percentscore>100			   


						  
replace Subject = subinstr(Subject," ","",.)
replace Subject = subinstr(Subject,"/","",.)
replace Subject = subinstr(Subject,"Exam","",.)
replace Subject = subinstr(Subject,"Diagnostic","",.)
replace Subject = subinstr(Subject,"Studies","",.)
replace Subject ="EnglishLanguage" if Subject=="lang"
replace Subject ="EnglishComposition" if Subject=="read"
replace Subject ="EnglishComposition" if Subject=="comp"
replace Subject ="CREIRE" if Subject=="creative" 
replace Subject ="CREIRE" if Subject=="create"
replace Subject ="CREIRE" if Subject=="re" 
drop if Subject=="ssre"
replace Subject ="Mathematics" if Subject=="math"
replace Subject ="KiswahiliInsha" if Subject=="insha"
replace Subject ="KiswahiliInsha" if Subject=="kuandika"
replace Subject ="KiswahiliLugha" if Subject=="kusoma"
replace Subject ="KiswahiliLugha" if Subject=="lugha"


replace Subject ="EngCompReading" if Subject=="EnglishComposition"
replace Subject ="EngLanguage" if Subject=="EnglishLanguage"
replace Subject ="math" if Subject=="Mathematics"
replace Subject ="dev" if Subject=="Developmental"
replace Subject ="SwaLanguage" if Subject=="KiswahiliLugha"
replace Subject ="SwaCompReading" if Subject=="KiswahiliInsha"

replace Subject ="ssci" if Subject=="scienceandSocial"
replace Subject ="sci" if Subject=="science"
replace Subject ="ss" if Subject=="social"
replace Subject ="crere" if Subject=="CREIRE"


replace Subject ="lang" if Subject=="eng"
replace Subject ="swa" if Subject=="kiswahili"
replace Subject ="swa" if Subject=="swahili"
replace Subject ="math" if Subject=="Mathematics"
replace Subject ="sci" if Subject=="science"
replace Subject ="ss" if Subject=="social"
replace Subject ="env" if Subject=="environmental"
drop if Subject=="re"
drop if Subject=="crere"

save "$base_out/2015_Panel.dta", replace
