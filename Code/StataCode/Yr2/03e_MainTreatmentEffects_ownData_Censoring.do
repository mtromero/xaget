
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z percentscore_Z_LC percentscore_Z_RC *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR
/*
gen post=(period>=2)

intreg percentscore_Z c.treatment#c.post  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab(pupilid period)
*/



drop if period<=-1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period


encode academyid, gen(academyid2)
/*
intreg percentscore_Z treatment  TTR if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
intreg percentscore_Z treatment  TTR $controls_prescores  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
intreg percentscore_Z treatment   $controls_prescores  $controls_old  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
intreg percentscore_Z treatment   $controls_prescores  $controls2  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
*/
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/
**PUPILS
*intreg percentscore_Z treatment  $controls_prescores  if  pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))

*intreg percentscore_Z treatment  $controls_prescores  $controls if  pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
*pupils
		
	foreach cual in core {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: intreg percentscore_Z_LC percentscore_Z_RC treatment  $controls_prescores i.(gradename2#formerprovince2#stratascore)##c.(term*)  if  pupils==1 & Subject=="`subject'", vce(cluster academyid2) 
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Pooled_all_pupils_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: intreg percentscore_Z_LC percentscore_Z_RC treatment  $controls_prescores i.(gradename2#formerprovince2#stratascore)##c.(term*)  if  (dc1==1 | dc2==1) & Subject=="`subject'", vce(cluster academyid2) 
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Pooled_older_pupils_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
		
		
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: intreg percentscore_Z_LC percentscore_Z_RC treatment  $controls $controls_prescores i.(gradename2#formerprovince2#stratascore)##c.(term*)  if pupils==1 & Subject=="`subject'", vce(cluster academyid2)
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
		}
					
		esttab  using "$results/LatexCode/Pooled_all_pupils_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
		
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: intreg percentscore_Z_LC percentscore_Z_RC treatment  $controls $controls_prescores i.(gradename2#formerprovince2#stratascore)##c.(term*)  if   (dc1==1 | dc2==1) & Subject=="`subject'", vce(cluster academyid2) 
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
		}
					
		esttab  using "$results/LatexCode/Pooled_older_pupils_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
	
	}
	
	**tutors	
	*tutors
	foreach cual in core {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: intreg percentscore_Z_LC percentscore_Z_RC treatment  $controls_prescores i.(gradename2#formerprovince2#stratascore)##c.(term*)  if  tutors==1 & Subject=="`subject'", vce(cluster academyid2) 
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Pooled_all_tutors_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: intreg percentscore_Z_LC percentscore_Z_RC treatment  $controls_old $controls_prescores i.(gradename2#formerprovince2#stratascore)##c.(term*)  if  tutors==1 & Subject=="`subject'", vce(cluster academyid2)
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
		}
					
		esttab  using "$results/LatexCode/Pooled_all_tutors_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	}
