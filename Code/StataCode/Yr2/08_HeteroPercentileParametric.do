
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 GradeName2016 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors *tutor

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)

encode academyid, gen(academyid2)

foreach var in English_T3ET15_Z Swahili_T3ET15_Z math_T3ET15_Z lang_T1DG16_Z math_T1DG16_Z swa_T1DG16_Z{
egen terc_`var' = fastxtile(`var'), by(gradename2 period) nq(10)
}


foreach var in English_T3ET15_Z Swahili_T3ET15_Z math_T3ET15_Z lang_T1DG16_Z math_T1DG16_Z swa_T1DG16_Z{
egen tercwithin_`var' = fastxtile (`var'), by(academyid2 gradename2 period) nq(10)
}
drop period


rename terc_lang_T1DG16_Z terc_English_T1DG16_Z
rename terc_swa_T1DG16_Z  terc_Swahili_T1DG16_Z
rename tercwithin_lang_T1DG16_Z  tercwithin_English_T1DG16_Z
rename tercwithin_swa_T1DG16_Z tercwithin_Swahili_T1DG16_Z


		
foreach time in T1DG16 T3ET15{
foreach kind in terc tercwithin{

eststo clear
foreach subject in math English {
    capture drop Tercile
	gen Tercile=`kind'_`subject'_`time'_Z
	replace Tercile=0 if Tercile==.
	fvset base 1 Tercile
    char Tercile[omit] 1
	label var Tercile "Tercile"
	
		eststo: reghdfe percentscore_Z c.treatment#i.Tercile i.Tercile $controls $controls_prescores if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
		drop Tercile
		
		test (_b[1.Tercile#c.treatment]=_b[2.Tercile#c.treatment]= _b[3.Tercile#c.treatment]= _b[4.Tercile#c.treatment]= _b[5.Tercile#c.treatment]=_b[6.Tercile#c.treatment]=_b[7.Tercile#c.treatment]= _b[8.Tercile#c.treatment]= _b[9.Tercile#c.treatment]= _b[10.Tercile#c.treatment])
		local p_all=string(r(p), "%9.2gc")
		
		
		coefplot, graphregion(color(white)) baselevels keep(*c.treatment*) ci ///
		rename(0.Tercile#c.treatment="NA" 1.Tercile#c.treatment="1" 2.Tercile#c.treatment="2" 3.Tercile#c.treatment="3" 4.Tercile#c.treatment="4" ///
		5.Tercile#c.treatment="5" 6.Tercile#c.treatment="6" 7.Tercile#c.treatment="7" 8.Tercile#c.treatment="8" 9.Tercile#c.treatment="9" 10.Tercile#c.treatment="10")  ///
		levels(95 90) ciopts(lwidth(medium thick)) ///
		vertical yline(0)  xtitle("Decile in `time'",size(medlarge)) ytitle("Math tutoring treatment effect",size(medlarge)) title("Math tutoring treatment effect by decile in `time'",size(medlarge))
		graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
	}

				
	esttab  using "$results/LatexCode/het_all_pupils_`kind'_`time'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace keep(*#c.treatment ) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}
}
	
