
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors *tutor

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period
encode academyid, gen(academyid2)
drop if term4==1
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/
**PUPILS

/*
reghdfe percentscore_Z c.treatment##c.DateOfBirth $controls_prescores  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
sum DateOfBirth if treatment==0 & pupils==1
replace DateOfBirth=(DateOfBirth-r(mean)/r(sd))
sum DateOfBirth if treatment==0 & pupils==1		
reghdfe percentscore_Z c.treatment##c.DateOfBirth $controls_prescores  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
*/

label var Pupils_S "Enrollment"
label var Teachers_S "Teachers"
label var PTR_S "PTR"
label var TTR "TTR"
label var mean_T1DG16_tutor "Tutors' score in T1DG16"
label var mean_T3ET15_tutor "Tutors' score in T3ET15"
label var median_T1DG16_tutor "Tutors'  score in T1DG16"
label var median_T3ET15_tutor "Tutors' score in T3ET15"
label var math_T3ET15_Z "Tutee score in T3ET15"
label var Male_tutor "Proportion of male tutors"
label var Age_tutor "Tutors' average age"
label var AgeFirstBridge "Age joined Bridge"
gen Age=2016-year(DateOfBirth)	
	
foreach var of varlist DateOfBirth Male AgeFirstBridge Age_tutor Male_tutor median_T3ET15_tutor  PTR_S TTR Pupils_S {
	sum `var' if treatment==0 & pupils==1
	replace `var'=`var'-r(mean)
}

*controls_prescores_nomath
eststo clear
eststo:  reghdfe percentscore_Z c.treatment##c.median_T3ET15_tutor##c.math_T3ET15_Z    if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)
estadd local ProvinceFE "Yes"
estadd local Studentcontrols "Yes"
estadd local Schoolscontrols "Yes"

eststo:  reghdfe percentscore_Z c.treatment##c.median_T3ET15_tutor##c.math_T3ET15_Z  $controls_prescores_nomath  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)
estadd local ProvinceFE "Yes"
estadd local Studentcontrols "Yes"
estadd local Schoolscontrols "Yes"

eststo:  reghdfe percentscore_Z c.treatment##c.median_T3ET15_tutor##c.math_T3ET15_Z $controls $controls_prescores_nomath  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)
estadd local ProvinceFE "Yes"
estadd local Studentcontrols "Yes"
estadd local Schoolscontrols "Yes"
		
estout using "$results/LatexCode/het_triple_interact_withoutT2ET.tex", cells(b(star fmt(%9.3f)) se(par fmt(%9.3f)) ) ///
starlevels(* 0.10 ** 0.05 *** 0.01) stats(N r2, fmt(0 3) labels("Observations" "Adjusted \$R^2\$"))  label   ///
style(tex)	nonumber  replace keep(*treatment* *median_T3ET15_tutor* *math_T3ET15_Z*)   mlabels(none) collabels(none)

