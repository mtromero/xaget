
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR
/*
gen post=(period>=2)

reghdfe percentscore_Z c.treatment#c.post  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab(pupilid period)
*/
gen Tested=!missing(percentscore_Z)


drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)


label var term1 T1MT16
label var term2 T1ET16
label var term3 T2MT16
label var term4 T2ET16
label var term5 T3MT16
label var term6 T3ET16
keep pupilid period Tested Subject
reshape wide Tested, i(pupilid period) j(Subject) string

label var Testedmath "Math"
label var TestedEngCompReading "English (Reading)"
label var TestedEngLanguage "English (Writing)"

eststo clear
estpost tabstat Testedmath TestedEngLanguage TestedEngCompReading , by(period)  statistics(mean sd) columns(statistics)
esttab using "$results/LatexCode/Evolv_Tested.tex", main(mean) aux(sd) nostar unstack  nonote nomtitle nonumber  replace label booktabs

 /*

using "$results/LatexCode/Evolv_Tested_pupils_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(term*) stats(N N_clust  , labels( "N. of obs." "Number of schools" )) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

/*
reghdfe percentscore_Z treatment  TTR if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z treatment  TTR $controls_prescores  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z treatment   $controls_prescores  $controls_old  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
reghdfe percentscore_Z treatment   $controls_prescores  $controls2  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab((gradename2#formerprovince2#stratascore)##c.(term*))
*/
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/
**PUPILS
*reghdfe percentscore_Z treatment  $controls_prescores  if  pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))

*reghdfe percentscore_Z treatment  $controls_prescores  $controls if  pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
*pupils
	foreach cual in core {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reg Tested term*   if  pupils==1 & Subject=="`subject'", vce(cluster academyid2) nocons
			estadd ysumm
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Evolv_Tested_pupils_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(term*) stats(N N_clust  , labels( "N. of obs." "Number of schools" )) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}	

	**tutors		
	*tutors
	foreach cual in core {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reg Tested term*   if  tutors==1 & Subject=="`subject'", vce(cluster academyid2) nocons
			estadd ysumm
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Evolv_Tested_tutors_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(term*) stats(N N_clust  , labels( "N. of obs." "Number of schools" )) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}


	foreach cual in core {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reg  term*   if  Subject=="`subject'", vce(cluster academyid2) nocons
			estadd ysumm
		
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Evolv_Tested_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(term*) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}

*/
