
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear



*********************************************************************************
*********************************************************************************
*********************************************************************************
*******************     MAIN ANALYSIS   *****************************************
*********************************************************************************
*********************************************************************************
*********************************************************************************
* T1MT16 T1ET16 T2MT16 T2ET16

*


*
foreach Period in T1MT16 T1ET16 T2MT16 T2ET16 T3MT16  T3ET16{
		

	
	**PUPILS
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reghdfe percentscore_Z treatment  if pupils==1 & period=="`Period'":periodl & Subject=="`subject'" , vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "No"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_all_pupils_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_pupils_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reghdfe percentscore_Z treatment  if  (dc1==1 | dc2==1) & period=="`Period'":periodl & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "No"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_older_pupils_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_pupils_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")

	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reghdfe percentscore_Z treatment $controls_prescores if  pupils==1 &  period=="`Period'":periodl & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_all_pupils.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_pupils_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reghdfe percentscore_Z treatment  $controls_prescores  if  (dc1==1 | dc2==1) & period=="`Period'":periodl & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_older_pupils.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_pupils_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	
	
	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reghdfe percentscore_Z treatment $controls $controls_prescores  if  pupils==1 & period=="`Period'":periodl & Subject=="`subject'" , vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_all_pupils_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_pupils_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	
	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reghdfe percentscore_Z treatment $controls $controls_prescores  if   (dc1==1 | dc2==1) & period=="`Period'":periodl & Subject=="`subject'" , vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_older_pupils_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_pupils_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	
	
	
	
	**tutors			
	eststo clear
	foreach subject in $Outcomes_tutors {
		eststo: reghdfe percentscore_Z treatment i.gradename2  if  tutors==1 & period=="`Period'":periodl & Subject=="`subject'" , vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "No"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_all_tutors_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_tutors_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


	eststo clear
	foreach subject in $Outcomes_tutors {
		eststo: reghdfe percentscore_Z treatment  $controls_prescores if  tutors==1 & period=="`Period'":periodl & Subject=="`subject'" , vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_all_tutors.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_tutors_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	
	eststo clear
	foreach subject in $Outcomes_tutors {
		eststo: reghdfe percentscore_Z treatment $controls_old $controls_prescores  if  tutors==1 & period=="`Period'":periodl & Subject=="`subject'" , vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
	}
				
	esttab  using "$results/LatexCode/Per_`Period'_all_tutors_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	mtitles($Outcomes_tutors_labels) ///
	replace keep(treatment) stats(N N_clust controls ProvinceFE Studentcontrols Schoolscontrols , labels("N. of obs." "Number of schools" "Control schools" "Province FE" "Student controls" "School controls")) ///
	nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	
				
} /* close foreach period at the end */

