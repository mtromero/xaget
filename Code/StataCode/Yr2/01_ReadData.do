
/*

use "$basein\Fully_Linked_2015.dta",clear
drop *teach*
merge 1:1 pupilid using "$basein\Fully_Linked_2015_T3ET14.dta", keepus(*T3ET14)
drop if flag_T1DG15_T3ET15 > 0
drop _merge
keep pupilid percentscore*
rename percentscore_*_ba_* percentscore_*_BA_*
rename percentscore_*_ns_* percentscore_*_NS_*
rename percentscore_*_pu_* percentscore_*_PU_*
*percentscore_@_c3_T1ET15  percentscore_@_c4_T1ET15
reshape long  percentscore_@_BA_T1ET15 percentscore_@_c1_T1ET15 percentscore_@_c2_T1ET15 percentscore_@_c3_T1ET15 percentscore_@_c4_T1ET15 ///
percentscore_@_c5_T1ET15 percentscore_@_c6_T1ET15 percentscore_@_c7_T1ET15 percentscore_@_c8_T1ET15 ///
percentscore_@_NS_T1ET15 percentscore_@_PU_T1ET15 ///
percentscore_@_BA_T1MT15 percentscore_@_c1_T1MT15 percentscore_@_c2_T1MT15 percentscore_@_c3_T1MT15 percentscore_@_c4_T1MT15 ///
percentscore_@_c5_T1MT15 percentscore_@_c6_T1MT15 percentscore_@_c7_T1MT15 percentscore_@_c8_T1MT15 ///
percentscore_@_NS_T1MT15 percentscore_@_PU_T1MT15 ///
percentscore_@_BA_T2MT15 percentscore_@_c1_T2MT15 percentscore_@_c2_T2MT15 percentscore_@_c3_T2MT15 percentscore_@_c4_T2MT15 ///
percentscore_@_c5_T2MT15 percentscore_@_c6_T2MT15 percentscore_@_c7_T2MT15 percentscore_@_c8_T2MT15 ///
percentscore_@_NS_T2MT15 percentscore_@_PU_T2MT15 ///
percentscore_@_BA_T2ET15 percentscore_@_c1_T2ET15 percentscore_@_c2_T2ET15 percentscore_@_c3_T2ET15 percentscore_@_c4_T2ET15 ///
percentscore_@_c5_T2ET15 percentscore_@_c6_T2ET15 percentscore_@_c7_T2ET15 percentscore_@_c8_T2ET15 ///
percentscore_@_NS_T2ET15 percentscore_@_PU_T2ET15 ///
percentscore_@_BA_T3ET15 percentscore_@_c1_T3ET15 percentscore_@_c2_T3ET15 percentscore_@_c3_T3ET15 percentscore_@_c4_T3ET15 ///
percentscore_@_c5_T3ET15 percentscore_@_c6_T3ET15 percentscore_@_c7_T3ET15 percentscore_@_c8_T3ET15 ///
percentscore_@_NS_T3ET15 percentscore_@_PU_T3ET15 ///
percentscore_@_BA_T3MT15 percentscore_@_c1_T3MT15 percentscore_@_c2_T3MT15 percentscore_@_c3_T3MT15 percentscore_@_c4_T3MT15 ///
percentscore_@_c5_T3MT15 percentscore_@_c6_T3MT15 percentscore_@_c7_T3MT15 percentscore_@_c8_T3MT15 ///
percentscore_@_NS_T3MT15 percentscore_@_PU_T3MT15 ///
percentscore_@_BA_T1DG15 percentscore_@_c1_T1DG15 percentscore_@_c2_T1DG15 percentscore_@_c3_T1DG15 percentscore_@_c4_T1DG15 ///
percentscore_@_c5_T1DG15 percentscore_@_c6_T1DG15 percentscore_@_c7_T1DG15 percentscore_@_c8_T1DG15 ///
percentscore_@_NS_T1DG15 percentscore_@_PU_T1EDG5 ///
percentscore_@_BA_T3ET14 percentscore_@_c1_T3ET14 percentscore_@_c2_T3ET14 percentscore_@_c3_T3ET14 percentscore_@_c4_T3ET14 ///
percentscore_@_c5_T3ET14 percentscore_@_c6_T3ET14 percentscore_@_c7_T3ET14 percentscore_@_c8_T3ET14 ///
percentscore_@_NS_T3ET14 percentscore_@_PU_T3ET14 ///
, i(pupilid) j(Subject) string
rename percentscore__* percentscore_*

reshape long  percentscore_@_T3ET14 percentscore_@_T1DG15  percentscore_@_T1MT15 percentscore_@_T1ET15 percentscore_@_T2MT15 percentscore_@_T2ET15 percentscore_@_T3MT15 percentscore_@_T3ET15, i(pupilid) j(grade) string
rename percentscore__* percentscore_*

reshape long  percentscore_@, i(pupilid) j(period) string

*/



use "$basein/PTR by Academy Grade Period_2016.11.01.dta",clear
rename AcademyID academyid
rename GradeName gradename
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
drop if academyid==""
preserve
collapse (count) Teachers_G=PTR (sum) Pupils_G=PTR  , by(academyid gradename period)
gen PTR_G=Pupils_G/Teachers_G
compress
save "$base_out/PTR_2106.dta", replace
drop if period!=1
save "$base_out/PTR_2106_G_DG.dta", replace
restore
collapse (count) Teachers_S=PTR (sum) Pupils_S=PTR  , by(academyid period)
gen PTR_S=Pupils_S/Teachers_S
compress
save "$base_out/PTR_2106_S.dta", replace
drop if period!=1
drop period
save "$base_out/PTR_2106_DG.dta", replace


use "$basein/PTR by Academy Grade Period_2016.11.01.dta",clear
drop if period==0 | GradeName=="STD 8"
rename AcademyID academyid
rename GradeName gradename
replace academyid="Mariakani-KLF" if academyid=="Mariakani-MBA"
replace academyid="Ugunja-SYA" if academyid=="Ugunja-KIS"
drop if academyid==""
collapse (sum) Pupils_Tutees=PTR  , by(academyid gradename period)
preserve
drop if gradename=="STD 3" | gradename=="STD 4" | gradename=="STD 5" | gradename=="STD 6" | gradename=="STD 7"
save "$base_out/PupilsAll.dta", replace
restore
gen gradeTutor="Baby Class" if gradename=="STD 3"
replace gradeTutor="Nursery" if gradename=="STD 4"
replace gradeTutor="PREUNIT" if gradename=="STD 5"
replace gradeTutor="STD 1" if gradename=="STD 6"
replace gradeTutor="STD 2" if gradename=="STD 7"
drop if gradeTutor=="" 
drop gradename
rename gradeTutor gradename
rename Pupils_Tutees Pupils_Tutors
save "$base_out/PupilsTutors.dta", replace
merge 1:1 academyid gradename period using "$base_out/PupilsAll.dta"
gen TTR=Pupils_Tutees/Pupils_Tutors
collapse (median) TTR  , by(academyid gradename)
rename gradename GradeName2016
keep TTR academyid GradeName2016
save "$base_out/TTR.dta", replace


use "$basein/pupil_gender_birthday_2016.11.02.dta",clear
rename PupilID pupilid
*replace DateOfBirth="" if DateOfBirth=="N/A"
*gen DOB = date(DateOfBirth, "DMY")
*format DOB %td
*gen DOB_M=mofd(DOB)
*format DOB_M %tm
*replace FirstDay="" if FirstDay=="N/A"
*gen FirstDayDate = date(FirstDay, "DMY")
*format FirstDayDate %td

gen AgeFirstBridge=FirstDay-DateOfBirth
replace AgeFirstBridge=AgeFirstBridge/365
rename Gender gender
replace gender="" if gender=="U" | gender=="S"
replace gender="F" if gender=="F "
replace gender="M" if gender=="M "

bys pupilid: gen dup=_n
drop if dup>1
drop dup
compress
save "$base_out/StudentSES_2016.dta", replace


import delimited "$basein/ListAcademies_2016.csv", bindquote(strict) stripquote(yes) clear 
compress 
rename academy_name academyid
drop _merge
keep country launchdate academyid streamtype fee_category wage_category religion charactercounty county formerprovince
save "$base_out/ListAcademies.dta", replace



import delimited "$basein/FinalRandomizationYr2.csv", bindquote(strict) stripquote(yes) clear 
compress
replace academyid="Mazeras-KLF" if academyid=="Mazeras-MBA"
save "$base_out/RandomizationXageT.dta", replace

use "$base_out/ListAcademies.dta", clear
merge 1:1 academyid using "$base_out/RandomizationXageT.dta"
drop if _merge==1
drop _merge

replace launchdate = subinstr(launchdate, "/", "-", .) 
gen launchdate2 = date(launchdate, "MDY")
format launchdate2 %td

gen DaysOpen=mdy(1,1,2016)- launchdate2 
tab wage_category, gen(wagecat_D)

merge 1:1 academyid using "$base_out/PTR_2106_DG.dta"
drop if _merge !=3
drop _merge
save "$base_out/SchoolLeveLData.dta", replace






use "$basein/Assessment_T3ET15_to_T3ET16.dta",clear
drop FirstName MiddleName LastName Score MaxScore mean_score sd_score std_score SubjectGroupTitle
replace AcademyID="Mazeras-KLF" if AcademyID=="Mazeras-MBA" 
replace AcademyID="Ugunja-SYA" if AcademyID=="Ugunja-KIS" 

replace Subject = subinstr(Subject," ","",.)
replace Subject = subinstr(Subject,"/","",.)
replace Subject = subinstr(Subject,"Exam","",.)
replace Subject = subinstr(Subject,"Diagnostic","",.)
replace Subject = subinstr(Subject,"Studies","",.)
replace Subject ="EnglishLanguage" if Subject=="Language"
replace Subject ="EnglishComposition" if Subject=="Reading"
replace Subject ="EnglishComposition" if Subject=="Composition"
replace Subject ="CREIRE" if Subject=="Creative" & GradeName=="PREUNIT"
replace Subject ="CREIRE" if Subject=="RE" & GradeName=="STD 8"
replace Subject ="Mathematics" if Subject=="Maths"
replace Subject ="KiswahiliInsha" if Subject=="Insha"
replace Subject ="KiswahiliInsha" if Subject=="Kuandika"
replace Subject ="KiswahiliLugha" if Subject=="Kusoma"
replace Subject ="KiswahiliLugha" if Subject=="Lugha"


replace Subject ="EngCompReading" if Subject=="EnglishComposition"
replace Subject ="EngLanguage" if Subject=="EnglishLanguage"
replace Subject ="math" if Subject=="Mathematics"
replace Subject ="dev" if Subject=="Developmental"
replace Subject ="SwaLanguage" if Subject=="KiswahiliLugha"
replace Subject ="SwaCompReading" if Subject=="KiswahiliInsha"

replace Subject ="ssci" if Subject=="ScienceandSocial"
replace Subject ="sci" if Subject=="Science"
replace Subject ="ss" if Subject=="Social"
replace Subject ="crere" if Subject=="CREIRE"


replace Subject ="lang" if Subject=="English"
replace Subject ="swa" if Subject=="Kiswahili"
replace Subject ="math" if Subject=="Mathematics"
replace Subject ="sci" if Subject=="Science"
replace Subject ="ss" if Subject=="Social"
replace Subject ="env" if Subject=="Environmental"



merge 1:1 PupilID period Subject using "$base_out/2015_Panel.dta"

bys PupilID: egen obspost=count(percentscore)   if period>=2
bys PupilID: egen obspost2=max(obspost)
drop if obspost2==.
drop obspost2 obspost
drop _merge

bys PupilID period: egen GradeNameMode=mode(GradeName), minmode
bys PupilID period: egen AcademyIDMode=mode(AcademyID), minmode
bys PupilID: egen EnrolledDateMode=min(EnrolledDate)
replace  GradeName=GradeNameMode
replace  AcademyID=AcademyIDMode
replace  EnrolledDate=EnrolledDateMode
drop GradeNameMode AcademyIDMode EnrolledDateMode

capture drop nvals
egen nvals=nvals(AcademyID) , by(PupilID) 
gen ChangeAcademy=(nvals==2)

drop if ChangeAcademy==1

capture drop nvals
egen nvals=nvals(GradeName), by(PupilID) 
drop if nvals==3

capture drop nvals
egen nvals=nvals(GradeName) if period>=1, by(PupilID) 
egen nvals2=mean(nvals), by(PupilID) 
drop if nvals2==2
drop nvals nvals2



drop ChangeAcademy 

drop AssessmentType stream treatmentRCTYr2 
bys Subject period GradeName: egen AssesmentMode=mode(AssessmentDate)
replace AssessmentDate=AssesmentMode 
drop AssesmentMode
reshape wide percentscore AssessmentDate, j(Subject) i(PupilID period) string
tsset PupilID period
by PupilID: gen count=_N if period>1
by PupilID: replace count=count[_n+1] if count==.
by PupilID: replace count=count[_n+1] if count==.
by PupilID: replace count=count[_n+1] if count==.
drop if count==.
drop count

egen nvals=nvals(GradeName), by(PupilID) 

tsfill , full



xfill AcademyID EnrolledDate, i(PupilID)
gsort PupilID period

by PupilID: replace GradeName="Nursery" if period==1 & GradeName[_n-1]=="Baby Class" & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==1 & GradeName[_n-1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==1 & GradeName[_n-1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==1 & GradeName[_n-1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==1 & GradeName[_n-1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==1 & GradeName[_n-1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==1 & GradeName[_n-1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==1 & GradeName[_n-1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==1 & GradeName[_n-1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 8" if period==1 & GradeName[_n-1]=="STD 7"  & GradeName==""


by PupilID: replace GradeName="Baby Class"  if period==0 & GradeName[_n+1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="Nursery" if period==0 & GradeName[_n+1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==0 & GradeName[_n+1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==0 & GradeName[_n+1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==0 & GradeName[_n+1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==0 & GradeName[_n+1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==0 & GradeName[_n+1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==0 & GradeName[_n+1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==0 & GradeName[_n+1]=="STD 7"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==0 & GradeName[_n+1]=="STD 8"  & GradeName==""


gsort PupilID period
by PupilID: replace GradeName=GradeName[_n-1] if period>=1 & GradeName==""
gsort PupilID -period
by PupilID: replace GradeName=GradeName[_n-1] if period>=1 & GradeName==""
gsort PupilID -period
by PupilID: replace GradeName=GradeName[_n-1] if period<=-1 & GradeName==""
gsort PupilID period
by PupilID: replace GradeName=GradeName[_n-1] if period<=-1 & GradeName==""

gsort PupilID period
by PupilID: replace GradeName="Nursery" if period==1 & GradeName[_n-1]=="Baby Class" & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==1 & GradeName[_n-1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==1 & GradeName[_n-1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==1 & GradeName[_n-1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==1 & GradeName[_n-1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==1 & GradeName[_n-1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==1 & GradeName[_n-1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==1 & GradeName[_n-1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==1 & GradeName[_n-1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 8" if period==1 & GradeName[_n-1]=="STD 7"  & GradeName==""

by PupilID: replace GradeName="Baby Class"  if period==0 & GradeName[_n+1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="Nursery" if period==0 & GradeName[_n+1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==0 & GradeName[_n+1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==0 & GradeName[_n+1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==0 & GradeName[_n+1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==0 & GradeName[_n+1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==0 & GradeName[_n+1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==0 & GradeName[_n+1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==0 & GradeName[_n+1]=="STD 7"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==0 & GradeName[_n+1]=="STD 8"  & GradeName==""

gsort PupilID period
by PupilID: replace GradeName=GradeName[_n-1] if period>=1 & GradeName==""
gsort PupilID -period
by PupilID: replace GradeName=GradeName[_n-1] if period>=1 & GradeName==""
gsort PupilID -period
by PupilID: replace GradeName=GradeName[_n-1] if period<=-1 & GradeName==""
gsort PupilID period
by PupilID: replace GradeName=GradeName[_n-1] if period<=-1 & GradeName==""




/*
by PupilID: replace GradeName="Nursery" if period==1 & GradeName[_n-1]=="Baby Class" & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==1 & GradeName[_n-1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==1 & GradeName[_n-1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==1 & GradeName[_n-1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==1 & GradeName[_n-1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==1 & GradeName[_n-1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==1 & GradeName[_n-1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==1 & GradeName[_n-1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==1 & GradeName[_n-1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 8" if period==1 & GradeName[_n-1]=="STD 7"  & GradeName==""

by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period>=1 & GradeName==""

by PupilID: replace GradeName="Baby Class"  if period==0 & GradeName[_n+1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="Nursery" if period==0 & GradeName[_n+1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==0 & GradeName[_n+1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==0 & GradeName[_n+1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==0 & GradeName[_n+1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==0 & GradeName[_n+1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==0 & GradeName[_n+1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==0 & GradeName[_n+1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==0 & GradeName[_n+1]=="STD 7"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==0 & GradeName[_n+1]=="STD 8"  & GradeName==""

by PupilID: replace GradeName=GradeName[_n-1] if period>=2 & GradeName==""
by PupilID: replace GradeName=GradeName[_n+1] if period==1 & GradeName==""

by PupilID: replace GradeName="Nursery" if period==1 & GradeName[_n-1]=="Baby Class"  & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==1 & GradeName[_n-1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==1 & GradeName[_n-1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==1 & GradeName[_n-1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==1 & GradeName[_n-1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==1 & GradeName[_n-1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==1 & GradeName[_n-1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==1 & GradeName[_n-1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==1 & GradeName[_n-1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 8" if period==1 & GradeName[_n-1]=="STD 7"  & GradeName==""

by PupilID: replace GradeName="Baby Class"  if period==0 & GradeName[_n+1]=="Nursery"  & GradeName==""
by PupilID: replace GradeName="Nursery" if period==0 & GradeName[_n+1]=="PREUNIT"  & GradeName==""
by PupilID: replace GradeName="PREUNIT" if period==0 & GradeName[_n+1]=="STD 1"  & GradeName==""
by PupilID: replace GradeName="STD 1" if period==0 & GradeName[_n+1]=="STD 2"  & GradeName==""
by PupilID: replace GradeName="STD 2" if period==0 & GradeName[_n+1]=="STD 3"  & GradeName==""
by PupilID: replace GradeName="STD 3" if period==0 & GradeName[_n+1]=="STD 4"  & GradeName==""
by PupilID: replace GradeName="STD 4" if period==0 & GradeName[_n+1]=="STD 5"  & GradeName==""
by PupilID: replace GradeName="STD 5" if period==0 & GradeName[_n+1]=="STD 6"  & GradeName==""
by PupilID: replace GradeName="STD 6" if period==0 & GradeName[_n+1]=="STD 7"  & GradeName==""
by PupilID: replace GradeName="STD 7" if period==0 & GradeName[_n+1]=="STD 8"  & GradeName==""
*/

capture drop nvals
egen nvals=nvals(GradeName), by(PupilID) 
drop if nvals==3

capture drop nvals
capture drop nvals2
egen nvals=nvals(GradeName) if period>=1, by(PupilID) 
egen nvals2=mean(nvals), by(PupilID) 
drop if nvals2==2
drop nvals nvals2

gsort PupilID period
gen Skip=1 if GradeName=="Baby Class" & GradeName[_n+1]!="Nursery" & period==0
replace Skip=1 if GradeName=="Nursery" & GradeName[_n+1]!="PREUNIT" & period==0
replace Skip=1 if GradeName=="PREUNIT" & GradeName[_n+1]!="STD 1" & period==0
replace Skip=1 if GradeName=="STD 1" & GradeName[_n+1]!="STD 2" & period==0
replace Skip=1 if GradeName=="STD 2" & GradeName[_n+1]!="STD 3" & period==0
replace Skip=1 if GradeName=="STD 3" & GradeName[_n+1]!="STD 4" & period==0
replace Skip=1 if GradeName=="STD 4" & GradeName[_n+1]!="STD 5" & period==0
replace Skip=1 if GradeName=="STD 5" & GradeName[_n+1]!="STD 6" & period==0
replace Skip=1 if GradeName=="STD 6" & GradeName[_n+1]!="STD 7" & period==0
replace Skip=1 if GradeName=="STD 7" & GradeName[_n+1]!="STD 8" & period==0
egen Skip2=mean(Skip), by(PupilID) 
drop if Skip2==1
drop Skip Skip2

drop if GradeName=="STD 8"
drop if GradeName=="STD 7" & period==0
by PupilID: gen count=_N

drop if GradeName==""

drop if GradeName=="PREUNIT" & period==1
drop if GradeName=="Baby Class" & period==1
drop if GradeName=="Nursery" & period==1

gen GradeName2016=GradeName if period>=1
gsort PupilID -period
by PupilID: replace GradeName2016=GradeName2016[_n-1] if period<=0
gsort PupilID period
drop if GradeName2016==""

gen dPU=(GradeName2016=="PREUNIT")
gen dNU=(GradeName2016=="Nursery")
gen dBC=(GradeName2016=="Baby Class")
gen dc1=(GradeName2016=="STD 1")
gen dc2=(GradeName2016=="STD 2")
gen dc3=(GradeName2016=="STD 3")
gen dc4=(GradeName2016=="STD 4")
gen dc5=(GradeName2016=="STD 5")
gen dc6=(GradeName2016=="STD 6")
gen dc7=(GradeName2016=="STD 7")
gen pupils= (dPU==1 | dNU==1 | dBC==1 | dc1==1 | dc2==1) 
gen tutors = (dc3==1 | dc4==1 | dc5==1 | dc6==1 | dc7==1) 


gen percentscoreEnglish=.
gen percentscoreSwahili=.
gen percentscoreOther=.

gen AssessmentDateEnglish=.
gen AssessmentDateSwahili=.
gen AssessmentDateOther=.
qui foreach grade in BC NU PU c1 c2 c3 c4 c5 c6 c7{
	di "`grade'"
	levelsof period if d`grade'==1, local(levelsperiod) 
	foreach pr in  `levelsperiod'{
	di "`pr'"
		capture drop nmis
		egen nmis=rmiss2(percentscoreEngCompReading percentscoreEngLanguage) if d`grade'==1 & period==`pr'
		count if nmis==0
		scalar num=r(N)
		count if nmis!=.
		scalar den=r(N)
		if(num/den>=0.1){
		pca percentscoreEngCompReading percentscoreEngLanguage if d`grade'==1 & period==`pr', components(1) 
		predict Temp if d`grade'==1 & period==`pr', score
		replace percentscoreEnglish=Temp if d`grade'==1 & period==`pr'
		capture drop Temp
		
		egen Temp=rowmin(AssessmentDateEngCompReading AssessmentDateEngLanguage) if d`grade'==1 & period==`pr'
		replace AssessmentDateEnglish=Temp if d`grade'==1 & period==`pr'
		capture drop Temp
		
		}
		capture drop nmis
		egen nmis=rmiss2(percentscoreSwaCompReading percentscoreSwaLanguage) if d`grade'==1 & period==`pr'
		
		
		count if nmis==0
		scalar num=r(N)
		count if nmis!=.
		scalar den=r(N)
		if(num/den>=0.1){
		pca percentscoreSwaCompReading percentscoreSwaLanguage if d`grade'==1 & period==`pr', components(1) 
		predict Temp if d`grade'==1 & period==`pr', score
		replace percentscoreSwahili=Temp if d`grade'==1 & period==`pr'
		capture drop Temp
		
		egen Temp=rowmin(AssessmentDateSwaCompReading AssessmentDateSwaLanguage) if d`grade'==1 & period==`pr'
		replace AssessmentDateSwahili=Temp if d`grade'==1 & period==`pr'
		capture drop Temp
		}
		capture drop nmis
		egen nmis=rmiss2(percentscoresci percentscoress) if d`grade'==1 & period==`pr'
		count if nmis==0
		scalar num=r(N)
		count if nmis!=.
		scalar den=r(N)
		if(num/den>=0.1){
		pca percentscoresci percentscoress if d`grade'==1 & period==`pr', components(1) 
		predict Temp if d`grade'==1 & period==`pr', score
		replace percentscoreOther=Temp if d`grade'==1 & period==`pr'
		capture drop Temp
		
		egen Temp=rowmin(AssessmentDatesci AssessmentDatess) if d`grade'==1 & period==`pr'
		replace AssessmentDateOther=Temp if d`grade'==1 & period==`pr'
		capture drop Temp
		}
	}
}

reshape long percentscore AssessmentDate, j(Subject) i(PupilID period) string
bys Subject period GradeName: egen AssesmentMode=mode(AssessmentDate)
replace AssessmentDate=AssesmentMode 
drop AssesmentMode







rename AcademyID academyid
merge m:1 academyid using "$base_out/SchoolLeveLData.dta"
drop if _merge==1
drop _merge
drop if treatment==.


encode formerprovince, gen(formerprovince2)
encode county, gen(county2)
encode GradeName, gen(gradename2)
encode academyid, gen(academyid2)

replace percentscore=100 if percentscore>100 & !missing(percentscore) & Subject!="English" & Subject!="Other" & Subject!="Swahili"
replace percentscore=0 if percentscore<0 & !missing(percentscore) & Subject!="English" & Subject!="Other" & Subject!="Swahili"



gen percentscore_Z=.
gen percentscore_Z_LC=.
gen percentscore_Z_RC=.
drop if period>=1 & AssessmentDate==.
drop if AssessmentDate<EnrolledDate & period>=1 
levelsof GradeName, local(levelsGrd) 
levelsof Subject, local(levelsSub) 
levelsof period, local(levelsperiod) 
foreach pr in  `levelsperiod'{
foreach grd in  `levelsGrd'{
foreach sub in  `levelsSub' {
qui sum percentscore if treatment==0 & GradeName=="`grd'" & Subject=="`sub'" & period==`pr',d
qui replace percentscore_Z=(percentscore-r(mean))/r(sd) if GradeName=="`grd'" & Subject=="`sub'" & period==`pr'
qui replace percentscore_Z_LC=(percentscore-r(mean))/r(sd) if GradeName=="`grd'"  & Subject=="`sub'" & percentscore!=0 & period==`pr' & Subject!="English" & Subject!="Other" & Subject!="Swahili"
qui replace percentscore_Z_RC=(percentscore-r(mean))/r(sd) if GradeName=="`grd'"  & Subject=="`sub'" & percentscore!=100 & period==`pr' & Subject!="English" & Subject!="Other" & Subject!="Swahili"
}
}
}

gen percentscore_Z2=percentscore_Z*percentscore_Z
gen percentscore_Z3=percentscore_Z*percentscore_Z*percentscore_Z





 
label var treatment "Math tutoring"






label var dc1 "STD 1"
label var dc2 "STD 2"
label var dc3 "STD 3"
label var dc4 "STD 4"
label var dc5 "STD 5"
label var dc6 "STD 6"
label var dc7 "STD 7"
label var dPU "PU"
label var dNU "NU"
label var dBC "BC"




rename GradeName gradename
merge m:1 academyid gradename using "$base_out/PTR_2106_G_DG.dta"
drop if _merge==2
drop  _merge

rename PupilID pupilid
merge m:1 pupilid using "$base_out/StudentSES_2016.dta"
drop if _merge==2
drop _merge


gen Male=(gender=="M")


levelsof Subject, local(levelsSub)
foreach pr in T3ET15 T1DG16{ 
foreach sub in  `levelsSub' {
gen `sub'_`pr'_Z=percentscore_Z if period=="`pr'":periodl & Subject=="`sub'"
bys pupilid: egen `sub'_`pr'_Z_temp=mean(`sub'_`pr'_Z)
replace `sub'_`pr'_Z=`sub'_`pr'_Z_temp
drop `sub'_`pr'_Z_temp
}
}

foreach var of varlist *T3ET15* *T1DG16* {
 gen tested_`var'=!missing(`var')
 gen `var'_mod=`var'
 replace `var'_mod=0 if `var'_mod==.
 gen `var'2_mod=`var'_mod*`var'_mod
 gen `var'3_mod=`var'_mod*`var'_mod*`var'_mod
}

 foreach var of varlist *T3ET15* *T1DG16* {
  sum `var'
 if (`r(N)'==0 ) {
 qui drop `var'
 }
 else if (  `r(sum)'==0  |`r(sum)'==1 | `r(sd)'==0 | `r(sd)'==.) {
 qui drop `var'
 }
 }
 
 


compress
save "$base_out/2016t1_FullyLinked_mainTE_own.dta", replace






*************** MEAN PER GRADE *****************

use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
keep percentscore_Z Subject pupilid period dPU-dc7 academyid GradeName2016  DateOfBirth gender
rename percentscore_Z _Z
reshape wide @_Z , j(Subject) i(pupilid period) string

gen Index_T1DG16=.
gen Index_T3ET15=.
foreach grade in BC NU PU c1 c2 c3 c4 c5 c6 c7{
	if("`grade'"!="BC" & "`grade'"!="NU" & "`grade'"!="PU"){
	di "`grade'"
		pca ${controls_T1DG16_`grade'_simple} if d`grade'==1 & period==1, components(1) 
		predict Index if d`grade'==1 & period==1, score
		replace Index_T1DG16=Index if d`grade'==1 & period==1
		capture drop Index
	}
	if("`grade'"!="BC"){
	di "`grade'"
		pca ${controls_T3ET15_`grade'_simple} if d`grade'==1 & period==0, components(1) 
		predict Index if d`grade'==1 & period==0, score
		replace Index_T3ET15=Index if d`grade'==1 & period==0
		capture drop Index
	}
}
preserve
collapse (mean) Index_T1DG16 Index_T3ET15, by(academyid GradeName2016)
rename Index_T1DG16 mean_T1DG16_tutor
rename Index_T3ET15 mean_T3ET15_tutor
gen gradeTutor="Baby Class" if GradeName2016=="STD 3"
replace gradeTutor="Nursery" if GradeName2016=="STD 4"
replace gradeTutor="PREUNIT" if GradeName2016=="STD 5"
replace gradeTutor="STD 1" if GradeName2016=="STD 6"
replace gradeTutor="STD 2" if GradeName2016=="STD 7"
drop if gradeTutor==""
compress
save "$base_out/Mean.dta", replace
restore

preserve
collapse (median) Index_T1DG16 Index_T3ET15, by(academyid GradeName2016)
rename Index_T1DG16 median_T1DG16_tutor
rename Index_T3ET15 median_T3ET15_tutor
gen gradeTutor="Baby Class" if GradeName2016=="STD 3"
replace gradeTutor="Nursery" if GradeName2016=="STD 4"
replace gradeTutor="PREUNIT" if GradeName2016=="STD 5"
replace gradeTutor="STD 1" if GradeName2016=="STD 6"
replace gradeTutor="STD 2" if GradeName2016=="STD 7"
drop if gradeTutor==""
compress
save "$base_out/Median.dta", replace
restore

preserve
collapse (sd) Index_T1DG16 Index_T3ET15, by(academyid GradeName2016)
rename Index_T1DG16 sd_T1DG16_tutor
rename Index_T3ET15 sd_T3ET15_tutor
gen gradeTutor="Baby Class" if GradeName2016=="STD 3"
replace gradeTutor="Nursery" if GradeName2016=="STD 4"
replace gradeTutor="PREUNIT" if GradeName2016=="STD 5"
replace gradeTutor="STD 1" if GradeName2016=="STD 6"
replace gradeTutor="STD 2" if GradeName2016=="STD 7"
drop if gradeTutor==""
compress
save "$base_out/sd.dta", replace
restore

preserve
gen Age=2016-year(DateOfBirth)
gen Male=(gender=="M")
keep if period==1
collapse (mean) Male Age, by(academyid GradeName2016)
rename Male Male_tutor
rename Age Age_tutor
gen gradeTutor="Baby Class" if GradeName2016=="STD 3"
replace gradeTutor="Nursery" if GradeName2016=="STD 4"
replace gradeTutor="PREUNIT" if GradeName2016=="STD 5"
replace gradeTutor="STD 1" if GradeName2016=="STD 6"
replace gradeTutor="STD 2" if GradeName2016=="STD 7"
drop if gradeTutor==""
compress
save "$base_out/maleage.dta", replace
restore



use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
gen gradeTutor=GradeName2016
merge m:1 academyid gradeTutor using "$base_out/Mean.dta"
drop if _merge==2
drop _merge
merge m:1 academyid gradeTutor using "$base_out/Median.dta"
drop if _merge==2
drop _merge
merge m:1 academyid gradeTutor using "$base_out/sd.dta"
drop if _merge==2
drop _merge
merge m:1 academyid gradeTutor using "$base_out/maleage.dta"
drop if _merge==2
drop _merge
compress
save "$base_out/2016t1_FullyLinked_mainTE_own.dta", replace

use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
merge m:1 academyid GradeName2016 using "$base_out/TTR.dta"

label var DateOfBirth "Date of birth"

drop if _merge==2
drop _merge
save "$base_out/2016t1_FullyLinked_mainTE_own.dta", replace



