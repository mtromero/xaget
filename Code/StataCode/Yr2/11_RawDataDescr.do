
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
set graphics off
bys treatment period Subject gradename2: cumul percentscore, gen(cum_percentscore) eq
bys treatment period Subject gradename2: cumul percentscore_Z, gen(cum_percentscore_Z) eq


levelsof GradeName2016, local(levelsGrd) 
levelsof period, local(levelspr) 

	foreach grd in  `levelsGrd'{
	foreach pr in `levelspr'{
		preserve
		keep if period==`pr' & GradeName2016=="`grd'"
		drop if percentscore==.
		if(_N>0){
			levelsof Subject, local(levelSubject) 
			foreach sub in `levelSubject'{
					
				if "`sub'"=="EngCompReading" local name3 "English (Reading)"
				else if "`sub'"=="EngLanguage"  local name3 "English (Writing)"	
				else if "`sub'"=="Environmental" local name3 "Environmental"
				else if "`sub'"=="SwaCompReading"  local name3 "Swahili (Reading)"	
				else if "`sub'"=="SwaLanguage" local name3 "Swahili (Writing)"
				else if "`sub'"=="crere"  local name3 "Religion"	
				else if "`sub'"=="lang" local name3 "English"
				else if "`sub'"=="dev"  local name3 "Development"	
				else if "`sub'"=="math" local name3 "Math"
				else if "`sub'"=="sci" local name3 "Science"
				else if "`sub'"=="ss"  local name3 "S.S."
				else if "`sub'"=="ssci" local name3 "Science/S.S."
				else if "`sub'"=="swa" local name3 "Swahili"
				
				if "`pr'"=="0" local name2 "T3ET15"
				else if "`pr'"=="1"  local name2 "T1DG16"	
				else if "`pr'"=="2" local name2 "T1MT16"
				else if "`pr'"=="3"  local name2 "T1ET16"	
				else if "`pr'"=="4" local name2 "T2MT16"
				else if "`pr'"=="5"  local name2 "T2ET16"	
				else if "`pr'"=="6" local name2 "T3MT16"
				else if "`pr'"=="7"  local name2 "T3ET16"	
		
				
				if(!(`pr'==1 & ("`grd'"=="Baby Class" |  "`grd'"=="Nursery" | "`grd'"=="PREUNIT")) & !(`pr'==0 & ("`grd'"=="Baby Class"))){
					twoway (kdensity percentscore if GradeName2016=="`grd'" & treatment==0 & Subject=="`sub'" & period==`pr', bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity percentscore if GradeName2016=="`grd'" & treatment==1 & Subject=="`sub'" & period==`pr', bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `name2'") ytitle(Density) title("Distribution of `name3' scores by treatment" "`grd'") graphregion(color(white))
					graph export "$results/Graphs/Dist_`sub'_`pr'_`grd'.pdf", as(pdf) replace			
								
					twoway (line cum_percentscore percentscore if GradeName2016=="`grd'" & treatment==0 & Subject=="`sub'" & period==`pr', sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_percentscore percentscore if GradeName2016=="`grd'" & treatment==1 & Subject=="`sub'" & period==`pr', sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `name2'") ytitle(CDF) title("Distribution of `name3' scores by treatment" "`grd'") graphregion(color(white))
					graph export "$results/Graphs/CDF_`sub'_`pr'_`grd'.pdf", as(pdf) replace
				}
			}
		
		}
		restore
	}
}




levelsof period, local(levelspr) 
	foreach pr in `levelspr'{
		preserve
		keep if period==`pr'
		drop if percentscore==.
		if(_N>0){
			levelsof Subject, local(levelSubject) 
			foreach sub in `levelSubject'{
					
				if "`sub'"=="EngCompReading" local name3 "English (Reading)"
				else if "`sub'"=="EngLanguage"  local name3 "English (Writing)"	
				else if "`sub'"=="Environmental" local name3 "Environmental"
				else if "`sub'"=="SwaCompReading"  local name3 "Swahili (Reading)"	
				else if "`sub'"=="SwaLanguage" local name3 "Swahili (Writing)"
				else if "`sub'"=="crere"  local name3 "Religion"	
				else if "`sub'"=="lang" local name3 "English"
				else if "`sub'"=="dev"  local name3 "Development"	
				else if "`sub'"=="math" local name3 "Math"
				else if "`sub'"=="sci" local name3 "Science"
				else if "`sub'"=="ss"  local name3 "S.S."
				else if "`sub'"=="ssci" local name3 "Science/S.S."
				else if "`sub'"=="swa" local name3 "Swahili"
				
				if "`pr'"=="0" local name2 "T3ET15"
				else if "`pr'"=="1"  local name2 "T1DG16"	
				else if "`pr'"=="2" local name2 "T1MT16"
				else if "`pr'"=="3"  local name2 "T1ET16"	
				else if "`pr'"=="4" local name2 "T2MT16"
				else if "`pr'"=="5"  local name2 "T2ET16"	
				else if "`pr'"=="6" local name2 "T3MT16"
				else if "`pr'"=="7"  local name2 "T3ET16"	
		
				
				
				twoway (kdensity percentscore_Z if  treatment==0 & Subject=="`sub'" & period==`pr' & pupils==1, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity percentscore_Z if treatment==1 & Subject=="`sub'" & period==`pr' & pupils==1, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `name2'") ytitle(Density) title("Distribution of `name3' scores by treatment") graphregion(color(white))
				graph export "$results/Graphs/Dist_`sub'_`pr'_pupils.pdf", as(pdf) replace			
							
				twoway (line cum_percentscore_Z percentscore_Z if  treatment==0 & Subject=="`sub'" & period==`pr' & pupils==1, sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_percentscore_Z percentscore_Z if  treatment==1 & Subject=="`sub'" & period==`pr' & pupils==1, sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `name2'") ytitle(CDF) title("Distribution of `name3' scores by treatment") graphregion(color(white))
				graph export "$results/Graphs/CDF_`sub'_`pr'_pupils.pdf", as(pdf) replace
				
				twoway (kdensity percentscore_Z if  treatment==0 & Subject=="`sub'" & period==`pr' & tutors==1, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity percentscore_Z if treatment==1 & Subject=="`sub'" & period==`pr' & tutors==1, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `name2'") ytitle(Density) title("Distribution of `name3' scores by treatment") graphregion(color(white))
				graph export "$results/Graphs/Dist_`sub'_`pr'_tutors.pdf", as(pdf) replace			
							
				twoway (line cum_percentscore_Z percentscore_Z if  treatment==0 & Subject=="`sub'" & period==`pr' & tutors==1, sort legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (line cum_percentscore_Z percentscore_Z if  treatment==1 & Subject=="`sub'" & period==`pr' & tutors==1, sort legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Score in `name2'") ytitle(CDF) title("Distribution of `name3' scores by treatment") graphregion(color(white))
				graph export "$results/Graphs/CDF_`sub'_`pr'_tutors.pdf", as(pdf) replace
				
			}
		
		}
		restore
	}

		
		
	
				
gen Age=2016-year(DateOfBirth)
levelsof GradeName2016, local(levelsGrd) 
foreach grd in  `levelsGrd'{
	twoway (kdensity Age if GradeName2016=="`grd'" & treatment==0 & period==2, bwidth(10) legend(lab(1 "Control")) lwidth(medthick) lpattern(dash))  (kdensity Age if GradeName2016=="`grd'" & treatment==1 & period==2, bwidth(10) legend(lab(2 "Treatment")) lwidth(medthick)), legend(order(1 2)) xtitle("Age") ytitle(Density) title("Age distribution by treatment" "`grd'") graphregion(color(white))
	graph export "$results/Graphs/Dist_age_`grd'_TC.pdf", as(pdf) replace
	
	twoway (kdensity Age if GradeName2016=="`grd'" & period==2, bwidth(10) lwidth(medthick) lpattern(dash)),  xtitle("Age") ytitle(Density) title("Age distribution" "`grd'") graphregion(color(white))
	graph export "$results/Graphs/Dist_age_`grd'.pdf", as(pdf) replace
}	

drop if period!=2
	twoway (kdensity Age if GradeName2016=="Baby Class" & period==2, bwidth(1) lwidth(medthick)  legend(lab(1 "Baby Class"))) ///
	(kdensity Age if GradeName2016=="Nursery" & period==2, bwidth(1) lwidth(medthick)  legend(lab(2 "Nursery"))) ///
	(kdensity Age if GradeName2016=="PREUNIT" & period==2, bwidth(1) lwidth(medthick)  legend(lab(3 "PREUNIT"))) ///
	(kdensity Age if GradeName2016=="STD 1" & period==2, bwidth(1) lwidth(medthick)  legend(lab(4 "STD 1"))) ///
	(kdensity Age if GradeName2016=="STD 2" & period==2, bwidth(1) lwidth(medthick)  legend(lab(5 "STD 2"))) ///
	(kdensity Age if GradeName2016=="STD 3" & period==2, bwidth(1) lwidth(medthick)  legend(lab(6 "STD 3"))) ///
	(kdensity Age if GradeName2016=="STD 4" & period==2, bwidth(1) lwidth(medthick)  legend(lab(7 "STD 4"))) ///
	(kdensity Age if GradeName2016=="STD 5" & period==2, bwidth(1) lwidth(medthick)  legend(lab(8 "STD 5"))) ///
	(kdensity Age if GradeName2016=="STD 6" & period==2, bwidth(1) lwidth(medthick)  legend(lab(9 "STD 6"))) ///
	(kdensity Age if GradeName2016=="STD 7" & period==2, bwidth(1) lwidth(medthick)  legend(lab(10 "STD 7"))) ///
	 ,xtitle("Age") ytitle(Density) graphregion(color(white)) xscale(range(3 18))
	
	
	graph export "$results/Graphs/Dist_age.pdf", as(pdf) replace

