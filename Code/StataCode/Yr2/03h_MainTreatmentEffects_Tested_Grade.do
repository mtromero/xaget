
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
drop period
encode academyid, gen(academyid2)
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/
gen Tested=!missing(percentscore_Z)

	foreach subject in $Outcomes_core{
		local label : var label percentscore_Z
		reghdfe Tested c.treatment#c.(dPU  dNU dBC dc1  dc2)   $controls_prescores   if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.dBC="BC" c.treatment#c.dNU="NU" c.treatment#c.dPU="PU" c.treatment#c.dc1="C1" c.treatment#c.dc2="C2")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") title("Attrition for `subject' by grade")
		graph export "$results/Graphs/Grade_Tested_`subject'_Pupils.pdf", replace
		
		reghdfe Tested c.treatment#c.(dPU  dNU dBC dc1  dc2) $control  $controls_prescores   f  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.dBC="BC" c.treatment#c.dNU="NU" c.treatment#c.dPU="PU" c.treatment#c.dc1="C1" c.treatment#c.dc2="C2")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") title("Attrition for `subject' by grade")
		graph export "$results/Graphs/Grade_Tested_`subject'_Pupils_control.pdf", replace
		
	}	

	
	
	
	
	
	foreach subject in $Outcomes_core{
		local label : var label percentscore_Z
		
		reghdfe Tested c.treatment#c.(dc3 dc4 dc5 dc6 dc7)   $controls_prescores    if tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.dc3="C3" c.treatment#c.dc4="C4" c.treatment#c.dc5="C5" c.treatment#c.dc6="C6" c.treatment#c.dc7="C7")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") title("Attrition for `subject' by grade")	
		graph export "$results/Graphs/Grade_Tested_`subject'_tutors.pdf", replace
		
		reghdfe Tested c.treatment#c.(dc3 dc4 dc5 dc6 dc7) $controls_old $controls_prescores    if tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.dc3="C3" c.treatment#c.dc4="C4" c.treatment#c.dc5="C5" c.treatment#c.dc6="C6" c.treatment#c.dc7="C7")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") title("Attrition for `subject' by grade")	
		graph export "$results/Graphs/Grade_Tested_`subject'_tutors_controls.pdf", replace
		
		
	}
	

	
