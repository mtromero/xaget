set more off
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
keep percentscore_Z Subject pupilid period DateOfBirth gender gradename AgeFirstBridge treatment academyid2 formerprovince2 stratascore  tutors pupils GradeName2016
encode GradeName2016, gen(gradename2)
gen Tested=!missing(percentscore)
/*
preserve
collapse (max) Tested, by( pupilid period)
collapse (sum) Tested, by( pupilid )
restore
*/
/*
levelsof gradename, local(levelsGrd) 
levelsof Subject, local(levelsSub) 
foreach grd in  `levelsGrd'{
foreach sub in  `levelsSub' {
replace Tested=!missing(percentscore) if  gradename=="`grd'" & Subject=="`sub'"
}
}
*/
reshape wide percentscore_Z Tested, j(Subject) i(pupilid period) string

label var percentscore_ZEngCompReading "English (reading)"
label var TestedEngCompReading "Tested English (reading)"
label var percentscore_ZEngLanguage "English (writing)"
label var TestedEngLanguage "Tested English (writing)"
label var percentscore_Zenv "Environmental"
label var Testedenv "Tested Environmental"
label var percentscore_ZSwaCompReading "Swahili (reading)"
label var TestedSwaCompReading "Tested Swahili (reading)"
label var percentscore_ZSwaLanguage "Swahili (writing)"
label var TestedSwaLanguage "Tested Swahili (writing)"


label var percentscore_Zcrere "Religion"
label var Testedcrere "Tested Religion"
label var percentscore_Zlang "English"
label var Testedlang "Tested English"
label var percentscore_Zdev "Development"
label var Testeddev "Tested Development"
label var percentscore_Zmath "Math"
label var Testedmath "Tested Math"
label var percentscore_Zsci "Science"
label var Testedsci "Tested Science"
label var percentscore_Zss "S.S."
label var Testedss "Tested S.S."
label var percentscore_Zssci "Science/S.S."
label var Testedssci "Tested Science/S.S."
label var percentscore_Zswa "Swahili"
label var Testedswa "Tested Swahili"

                         

drop *ssci *crere


gen Age=2016-year(DateOfBirth)
gen Male=(gender=="M")
label var AgeFirstBridge "Age entered Bridge"

levelsof GradeName2016, local(levelsGrd) 
foreach grd in  `levelsGrd'{
qui my_ptest Age Male AgeFirstBridge   if GradeName2016=="`grd'" & period==2, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore)
esttab using "$results/LatexCode/Balance_`grd'_NTV.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

}
eststo clear
qui my_ptest Age Male AgeFirstBridge   if pupils==1 & period==2, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_pupils_NTV.tex", label replace fragment nolines nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 


	
*collabels("Math Tutoring" "English Tutoring" "Difference" "Difference (F.E)") ///
eststo clear
qui my_ptest Age Male AgeFirstBridge   if tutors==1 & period==2, by(treatment) clus_id(academyid2) strat_id(formerprovince2#stratascore gradename2)
esttab using "$results/LatexCode/Balance_tutors_NTV.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

preserve
tsset, clear
keep if period==2
foreach var of varlist Age Male AgeFirstBridge percentscore*{
	gen `var'_1=`var' if pupils==1 
	gen `var'_2=`var' if tutors==1 	
}
foreach var of varlist *_2 *_1 {
  sum `var'
 if (`r(N)'==0 ) {
 qui drop `var'
 }
 else if ( `r(sum)'==0  |`r(sum)'==1 | `r(sd)'==0 | `r(sd)'==.) {
 qui drop `var'
 }
}
rwolf Age_1 Age_2 Male_1 Male_2 AgeFirstBridge_1 AgeFirstBridge_2 percentscore_ZEngCompReading_1 percentscore_ZEngCompReading_2 percentscore_ZEngLanguage_1 percentscore_ZEngLanguage_2 percentscore_ZSwaCompReading_1 percentscore_ZSwaCompReading_2 percentscore_ZSwaLanguage_1 percentscore_ZSwaLanguage_2 percentscore_Zmath_1 percentscore_Zmath_2, indepvar(treatment) method(reghdfe) vce(cluster academyid2) ab(formerprovince2 stratascore gradename2) seed(845894) reps(100)
restore

	

levelsof GradeName2016, local(levelsGrd) 
levelsof period, local(levelspr) 
foreach grd in  `levelsGrd'{
foreach pr in 0{
di "`pr'"
di "`grd'"

if(!(`pr'==1 & ("`grd'"=="Baby Class" |  "`grd'"=="Nursery" | "`grd'"=="PREUNIT")) & !(`pr'==0 & ("`grd'"=="Baby Class"))){
preserve
keep if GradeName2016=="`grd'" & period==`pr'
qui ds, has(type numeric)
foreach var of varlist Tested* percent* {
  sum `var'
 if (`r(N)'==0 ) {
 qui drop `var'
 }
 else if ( `r(sum)'==0  |`r(sum)'==1 | `r(sd)'==0 | `r(sd)'==.) {
 qui drop `var'
 }
}

eststo clear
qui my_ptest   Tested* if GradeName2016=="`grd'" & period==`pr', by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore)
esttab using "$results/LatexCode/Balance_`grd'_`pr'_tested.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 



eststo clear
qui my_ptest  percentscore* if GradeName2016=="`grd'" & period==`pr', by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore)
esttab using "$results/LatexCode/Balance_`grd'_`pr'.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

restore
}
}
}



levelsof period, local(levelspr) 
foreach pr in  0{

preserve
tsset pupilid period

keep if period==`pr'
qui ds, has(type numeric)
foreach var of varlist percent* Tested* {
 quietly sum `var'
 if (`r(N)'==0 ) {
 qui drop `var'
 }
 else if (  `r(sum)'==0  |`r(sum)'==1 | `r(sd)'==0 | `r(sd)'==.) {
 qui drop `var'
 }
}
eststo clear
qui my_ptest  Tested*   if  period==`pr', by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_`pr_tested'.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 


eststo clear
qui my_ptest  percentscore*  if  period==`pr', by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_`pr'.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

restore


preserve
tsset pupilid period

keep if period==`pr' & pupils==1
qui ds, has(type numeric)
foreach var of varlist percent* Tested* {
 quietly sum `var'
 if (`r(N)'==0 ) {
 qui drop `var'
 }
 else if (  `r(sum)'==0  |`r(sum)'==1 | `r(sd)'==0 | `r(sd)'==.) {
 qui drop `var'
 }
}
eststo clear
qui my_ptest  Tested*  if  period==`pr' & pupils==1, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_`pr'_pupils_tested.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 


eststo clear
qui my_ptest  percentscore*  if  period==`pr' & pupils==1, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_`pr'_pupils.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 


restore



preserve
tsset pupilid period

keep if period==`pr' & tutors==1
qui ds, has(type numeric)
foreach var of varlist percent* Tested* {
 quietly sum `var'
 if (`r(N)'==0 ) {
 qui drop `var'
 }
 else if (  `r(sum)'==0  |`r(sum)'==1 | `r(sd)'==0 | `r(sd)'==.) {
 qui drop `var'
 }
}
eststo clear
qui my_ptest  Tested*   if  period==`pr' & tutors==1, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_`pr'_tutors_tested.tex", label replace fragment nogaps  ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 


eststo clear
qui my_ptest  percentscore*  if  period==`pr' & tutors==1, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore gradename2)
esttab using "$results/LatexCode/Balance_`pr'_tutors.tex", label replace fragment nogaps   ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

restore

}


