
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period
encode academyid, gen(academyid2)
/*

gen term6=(term=="T3ET16")
*/

gen Tested=!missing(percentscore_Z)
	
	foreach subject in $Outcomes_core{
		local label : var label percentscore_Z
		
		
		reghdfe Tested c.treatment#c.(term*) $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		test (_b[treatment#c.term1]=_b[treatment#c.term2]= _b[treatment#c.term3]= _b[treatment#c.term4]= _b[treatment#c.term5]=_b[treatment#c.term6])
		local p_all=string(r(p), "%9.2gc")
		
		
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") ///
		note("p-value(H{sub:0}:{&beta}{sub:T1-MT}={&beta}{sub:T1-ET}={&beta}{sub:T2-MT}={&beta}{sub:T2-ET}={&beta}{sub:T3-MT}={&beta}{sub:T3-ET})= `p_all'",size(medium))
		graph export "$results/Graphs/Evol_Tested_`subject'_Pupils.pdf", replace
		
		
	}
		
	foreach subject in $Outcomes_core {
		local label : var label percentscore_Z
		
		
		reghdfe Tested c.treatment#c.(term*) $control $controls_prescores if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		test (_b[treatment#c.term1]=_b[treatment#c.term2]= _b[treatment#c.term3]= _b[treatment#c.term4]= _b[treatment#c.term5]=_b[treatment#c.term6])
		local p_all=string(r(p), "%9.2gc")
		
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") ///
		note("p-value(H{sub:0}:{&beta}{sub:T1-MT}={&beta}{sub:T1-ET}={&beta}{sub:T2-MT}={&beta}{sub:T2-ET}={&beta}{sub:T3-MT}={&beta}{sub:T3-ET})= `p_all'",size(medium))
		graph export "$results/Graphs/Evol_Tested_`subject'_Pupils_control.pdf", replace
		
	}
	
	
	
	

	
	foreach subject in $Outcomes_core{
		
		local label : var label percentscore_Z

		reghdfe Tested c.treatment#c.(term*)  $controls_prescores if tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		test (_b[treatment#c.term1]=_b[treatment#c.term2]= _b[treatment#c.term3]= _b[treatment#c.term4]= _b[treatment#c.term5]=_b[treatment#c.term6])
		local p_all=string(r(p), "%9.2gc")
		
		
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") ///
		note("p-value(H{sub:0}:{&beta}{sub:T1-MT}={&beta}{sub:T1-ET}={&beta}{sub:T2-MT}={&beta}{sub:T2-ET}={&beta}{sub:T3-MT}={&beta}{sub:T3-ET})= `p_all'",size(medium))
		graph export "$results/Graphs/Evol_Tested_`subject'_tutors.pdf", replace
		
		
	}
	

		
	foreach subject in $Outcomes_core {
		local label : var label percentscore_Z

		reghdfe Tested c.treatment#c.(term*)  $control  $controls_prescores if tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		test (_b[treatment#c.term1]=_b[treatment#c.term2]= _b[treatment#c.term3]= _b[treatment#c.term4]= _b[treatment#c.term5]=_b[treatment#c.term6])
		local p_all=string(r(p), "%9.2gc")
		
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci rename(c.treatment#c.term1="T1-MT" c.treatment#c.term2="T1-ET" c.treatment#c.term3="T2-MT" c.treatment#c.term4="T2-ET" c.treatment#c.term5="T3-MT" c.treatment#c.term6="T3-ET")   vertical yline(0) xtitle("Period") ytitle("Math tutoring treatment effect") ///
		note("p-value(H{sub:0}:{&beta}{sub:T1-MT}={&beta}{sub:T1-ET}={&beta}{sub:T2-MT}={&beta}{sub:T2-ET}={&beta}{sub:T3-MT}={&beta}{sub:T3-ET})= `p_all'",size(medium))
		graph export "$results/Graphs/Evol_Tested_`subject'_tutors_controls.pdf", replace
		
	}
	
	
