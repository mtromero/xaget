
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR


drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
drop if period=="T2ET16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
drop period


encode academyid, gen(academyid2)

encode Subject, gen(Subject2)
reghdfe percentscore_Z c.treatment#i.Subject2   ($controls $controls_prescores )#i.Subject2  if  pupils==1 & (Subject=="math" | Subject=="English"), vce(cluster academyid2) ab((Subject2#gradename2#formerprovince2#stratascore)##c.(term*))
lincom 10.Subject2#c.treatment-3b.Subject2#c.treatment

local tempm=string(r(estimate), "%9.2gc")
file open newfile using "$results/LatexCode/Main_difference_estimate.tex", write replace
file write newfile "`tempm'"
file close newfile	

test 3b.Subject2#c.treatment==10.Subject2#c.treatment
local tempm=string(r(p), "%9.2gc")
file open newfile using "$results/LatexCode/Main_difference_pvalue.tex", write replace
file write newfile "`tempm'"
file close newfile

 
 
 *pupils
*core pupils
	foreach cual in  pupils_full {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls_prescores  if  pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Pooled_minusT2ET16_pupils_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust  , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		

		
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
		}
					
		esttab  using "$results/LatexCode/Pooled_minusT2ET16_pupils_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
	
	}
	
	**tutors		
	*tutors
	*core tutors
	foreach cual in  tutors_full{
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls_prescores  if  tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Pooled_minusT2ET16_tutors_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust  , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls_old $controls_prescores  if  tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
		}
					
		esttab  using "$results/LatexCode/Pooled_minusT2ET16_tutors_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	}
