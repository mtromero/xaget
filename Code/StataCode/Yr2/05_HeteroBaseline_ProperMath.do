clear mata
clear all
set matsize 1100
set seed 1234
local it = 100


use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

gen Male=(gender=="M")
local controls c.wagecat_D2 c.wagecat_D3 c.DaysOpen c.DOB c.Male c.AgeFirstBridge
local controls2  wagecat_D2 wagecat_D3 DaysOpen DOB Male AgeFirstBridge
gen NewStudent=(year(FirstDayDate)==2016)

drop *_Z2*
drop *_Z3*
egen Basemath_T1DG16=rowtotal(zo_math_T1DG16_Z*), missing
egen Basemath_T3ET15=rowtotal(zo_math_T3ET15_Z*), missing
keep `controls2' gradename gradename2 d* academyid formerprovince2 stratascore *_Z Basemath_T3ET15 Basemath_T1DG16 treatment pupilid IndexT1DG16*  IndexT3ET15*


reshape long math_@_Z EngLanguage_@_Z EngCompReading_@_Z SwaLanguage_@_Z SwaCompReading_@_Z re_@_Z sci_@_Z ss_@_Z, i(pupilid) j(term) string
label var math__Z "Math"
label var SwaCompReading__Z  "Reading/Composition"
label var EngCompReading__Z  "Reading/Composition"
label var SwaLanguage__Z  "Language"
label var EngLanguage__Z  "Language"


label var re__Z  "Religion"
label var sci__Z  "Science"
label var ss__Z  "S.S."
  

drop if term=="T3ET15"
drop if term=="T1DG16"
compress
gen term1=(term=="T1MT16")
gen term2=(term=="T1ET16")

egen IndexT1DG16=rowtotal(IndexT1DG16*), missing
egen IndexT3ET15=rowtotal(IndexT3ET15*), missing



keep if  gradename=="PREUNIT" | gradename=="STD 1" | gradename=="STD 2"
keep *__Z treat gradename academyid formerprovince2 Basemath_T3ET15  Basemath_T1DG16 gradename2 term1-term2 `controls2'
save "$base_out/Student_Pcentile_NP.dta", replace


foreach grade in "PREUNIT" "STD 1" "STD 2" "AllPupils"{
	foreach baseline in Basemath_T1DG16 Basemath_T3ET15{
		foreach subject in  math {
			if("`grade'"!="PREUNIT" | "`baseline'"!="IndexT1DG16"){
				use "$base_out/Student_Pcentile_NP.dta", clear
				keep  treat academyid `subject'__Z `baseline' formerprovince2 gradename2 gradename term* `controls2'
				if("`grade'"!="AllPupils") drop if gradename!="`grade'"
				reg `baseline'  (i.gradename2 `controls' i.formerprovince2)##c.(term*)
				predict `baseline'Resid, resid 
				reg `subject'__Z  (i.gradename2 `controls' i.formerprovince2)##c.(term*)
				predict `subject'__Resid, resid 
				compress
				save "$base_out/temp/TempBoot_Pass", replace

				qui egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1
				mkmat kernel_range if kernel_range != .
				matrix diff = kernel_range
				matrix x = kernel_range

				quietly forvalues j = 1(1)`it' {
				use "$base_out/temp/TempBoot_Pass", clear

				bsample, strata(treat formerprovince2) cluster(academyid)



				bysort treat: egen rank = rank(`baseline'Resid), unique
				bysort treat: egen max_rank = max(rank)
				bysort treat: gen PctileLag = rank/max_rank 



				egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1

				*regressing endline scores on percentile rankings
				lpoly `subject'__Resid PctileLag if treat==0 , gen(xcon pred_con) at (kernel_range) nograph
				lpoly `subject'__Resid PctileLag if treat==1 , gen(xtre pred_tre) at (kernel_range) nograph
					
					
				mkmat pred_tre if pred_tre != . 
				mkmat pred_con if pred_con != . 
				matrix diff = diff, pred_tre - pred_con

				}

				matrix diff = diff'

				*each variable is a percentile that is being estimated (can sort by column to get 2.5th and 97.5th confidence interval)
				svmat diff
				keep diff* 

				matrix conf_int = J(100, 2, 100)
				qui drop if _n == 1

				*sort each column (percentile) and saving 25th and 975th place in a matrix
				forvalues i = 1(1)100{
				sort diff`i'
				matrix conf_int[`i', 1] = diff`i'[0.025*`it']
				matrix conf_int[`i', 2] = diff`i'[0.975*`it']	
				}

				*******************Graphs for control, treatment, and difference using actual data (BASELINE)*************************************
				use "$base_out/temp/TempBoot_Pass", clear
				 
				bysort treat: egen rank = rank(`baseline'Resid), unique
				bysort treat: egen max_rank = max(rank)
				bysort treat: gen PctileLag = rank/max_rank 

				 
				egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1

				lpoly `subject'__Resid PctileLag if treat==0 , gen(xcon pred_con) at (kernel_range) nograph
				lpoly `subject'__Resid PctileLag if treat==1 , gen(xtre pred_tre) at (kernel_range) nograph

				gen diff = pred_tre - pred_con

				*variables for confidence interval bands
				svmat conf_int



				save "$base_out/Lowess_Percentile`baseline'_`subject'_`grade'.dta", replace

				graph twoway (line pred_con xcon, lcolor(blue) lpattern("--.....") legend(lab(1 "Control"))) ///
				(line pred_tre xtre, lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
				(line diff xcon, lcolor(black) lpattern(solid) legend(lab(3 "Difference"))) ///
				(line conf_int1 xcon, lcolor(black) lpattern(shortdash) legend(lab(4 "95% Confidence Band"))) ///
				(line conf_int2 xcon, lcolor(black) lpattern(shortdash) legend(lab(5 "95% Confidence Band"))) ///
				,yline(0, lcolor(gs10)) xtitle(Percentile of Baseline Score) ytitle(`subject' Endline Score) legend(order(1 2 3 4)) ///
				title("Treatment effect by percentile of baseline score for `subject' in `grade'", size(vsmall))
				graph export "$results/Graphs/Lowess_Percentile`baseline'_`subject'_`grade'.pdf", as(pdf) replace
			}/*close if*/

		}

	}
}


