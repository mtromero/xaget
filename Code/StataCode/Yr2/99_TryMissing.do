use "$basein/Assessment_T3ET15_to_T3MT16.dta",clear
drop FirstName MiddleName LastName Score MaxScore mean_score sd_score std_score AssessmentDate SubjectGroupTitle
rename AcademyID academyid
merge m:1 academyid using "$base_out/SchoolLeveLData.dta"
drop if _merge==1
drop _merge
drop if treatment==.

collapse (count) percentscore, by(academyid GradeName period Subject treatment)

codebook academyid if period==1 & (GradeName=="STD 1") & Subject=="Maths"
