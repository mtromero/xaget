
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
gen Male=(gender=="M")
local controls c.wagecat_D2 c.wagecat_D3 c.DaysOpen c.DOB c.Male c.AgeFirstBridge
local controls2  wagecat_D2 wagecat_D3 DaysOpen DOB Male AgeFirstBridge
gen NewStudent=(year(FirstDayDate)==2016)



keep `controls2' gradename gradename2 d* academyid formerprovince2 stratascore *_Z zo* treatment pupilid NewStudent


reshape long math_@_Z EngLanguage_@_Z EngCompReading_@_Z SwaLanguage_@_Z SwaCompReading_@_Z re_@_Z sci_@_Z ss_@_Z, i(pupilid) j(term) string
label var math__Z "Math"
label var SwaCompReading__Z  "Reading/Composition"
label var EngCompReading__Z  "Reading/Composition"
label var SwaLanguage__Z  "Language"
label var EngLanguage__Z  "Language"


label var re__Z  "Religion"
label var sci__Z  "Science"
label var ss__Z  "S.S."
  

drop if term=="T3ET15"
drop if term=="T1DG16"
compress
gen term1=(term=="T1MT16")
gen term2=(term=="T1ET16")



**PUPILS
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reg `subject'__Z c.treatment##c.NewStudent c.(term1 term2) formerprovince2##stratascore if  dc1==1, vce(cluster academyid)
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Pooled_grd1_pupils_plain.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonotes fragment keep(treatment NewStudent c.treatment#c.NewStudent) stats(N N_clust controls, labels("N. of obs." "Number of schools" "Control schools")) ///

	

	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reg `subject'__Z treatment (c.(zo_*T3ET15*c1)#c.dc1)#c.(term1 term2) formerprovince2##stratascore if  dc1==1, vce(cluster academyid)
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Pooled_grd1_pupils_T3ET15.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonotes fragment keep(treatment) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///

	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reg `subject'__Z c.treatment##c.NewStudent (c.(zo_*T1DG16*c1)#c.dc1)#c.(term1 term2) formerprovince2##stratascore if  dc1==1, vce(cluster academyid)
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Pooled_grd1_pupils_T1DG16.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonotes fragment keep(treatment NewStudent c.treatment#c.NewStudent) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///

	
	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reg `subject'__Z treatment (c.(zo_*c1)#c.dc1)#c.(term1 term2) formerprovince2##stratascore if  dc1==1, vce(cluster academyid)
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "No"
	}
				
	esttab  using "$results/LatexCode/Pooled_grd1_pupils.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonotes fragment keep(treatment) stats(N N_clust controls  , labels("N. of obs." "Number of schools" "Control schools" )) ///

	

	eststo clear
	foreach subject in $Outcomes_pupils {
		eststo: reg `subject'__Z treatment (`controls' c.(zo_*c1)#dc1)#c.(term1 term2) formerprovince2##stratascore if  dc1==1, vce(cluster academyid)
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
	}
				
	esttab  using "$results/LatexCode/Pooled_grd1_pupils_control.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber ///
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonotes fragment keep(treatment) stats(N N_clust controls , labels("N. of obs." "Number of schools" "Control schools")) ///

