clear mata
clear all
set matsize 1100
set seed 1234
local it = 100


use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
drop if period<=-1
keep percentscore_Z Subject pupilid period dPU-dc7 academyid GradeName2016  DateOfBirth gender pupils tutors gradename2 $controls2 formerprovince2 stratascore treatment
rename percentscore_Z _Z
reshape wide @_Z , j(Subject) i(pupilid period) string

gen Index_T1DG16=.
gen Index_T3ET15=.
foreach grade in BC NU PU c1 c2 c3 c4 c5 c6 c7{
	if("`grade'"!="BC" & "`grade'"!="NU" & "`grade'"!="PU"){
	di "`grade'"
		pca ${controls_T1DG16_`grade'_simple} if d`grade'==1 & period==1, components(1) 
		predict Index if d`grade'==1 & period==1, score
		replace Index_T1DG16=Index if d`grade'==1 & period==1
		capture drop Index
	}
	if("`grade'"!="BC"){
	di "`grade'"
		pca ${controls_T3ET15_`grade'_simple} if d`grade'==1 & period==0, components(1) 
		predict Index if d`grade'==1 & period==0, score
		replace Index_T3ET15=Index if d`grade'==1 & period==0
		capture drop Index
	}
}
tsset pupilid period
bys pupilid: replace Index_T3ET15=Index_T3ET15[_n-1] if Index_T3ET15==.
bys pupilid: replace Index_T3ET15=Index_T3ET15[_n+1] if Index_T3ET15==.
bys pupilid: replace Index_T3ET15=Index_T3ET15[_n-1] if Index_T3ET15==.
bys pupilid: replace Index_T3ET15=Index_T3ET15[_n+1] if Index_T3ET15==.

bys pupilid: replace Index_T1DG16=Index_T1DG16[_n-1] if Index_T1DG16==.
bys pupilid: replace Index_T1DG16=Index_T1DG16[_n+1] if Index_T1DG16==.
bys pupilid: replace Index_T1DG16=Index_T1DG16[_n-1] if Index_T1DG16==.
bys pupilid: replace Index_T1DG16=Index_T1DG16[_n+1] if Index_T1DG16==.
drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
save "$base_out/Student_Pcentile_NP.dta", replace

*c3 c4 c5 c6 c7
	foreach baseline in Index_T1DG16 Index_T3ET15{
		foreach subject in  $Outcomes_pupils {
				use "$base_out/Student_Pcentile_NP.dta", clear
				reg `subject'_Z  (i.gradename2 $controls )##c.(term*) formerprovince2##stratascore  if  pupils==1 , vce(cluster academyid)
				predict Resid, resid
				reg `baseline'  (i.gradename2 $controls )##c.(term*) formerprovince2##stratascore if  pupils==1 , vce(cluster academyid)
				predict `baseline'Resid, resid 
				compress
				save "$base_out/temp/TempBoot_Pass", replace

				qui egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1
				mkmat kernel_range if kernel_range != .
				matrix diff = kernel_range
				matrix x = kernel_range

				quietly forvalues j = 1(1)`it' {
				use "$base_out/temp/TempBoot_Pass", clear

				bsample, strata(treatment formerprovince2) cluster(academyid)

				bysort treatment  academyid: egen rank = rank(`baseline'Resid), unique
				bysort treatment  academyid: egen max_rank = max(rank)
				bysort treatment  academyid: gen PctileLag = rank/max_rank 



				egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1

				*regressing endline scores on percentile rankings
				lpoly Resid PctileLag if treatment==0 , gen(xcon pred_con) at (kernel_range) nograph
				lpoly Resid PctileLag if treatment==1 , gen(xtre pred_tre) at (kernel_range) nograph
					
					
				mkmat pred_tre if pred_tre != . 
				mkmat pred_con if pred_con != . 
				matrix diff = diff, pred_tre - pred_con

				}

				matrix diff = diff'

				*each variable is a percentile that is being estimated (can sort by column to get 2.5th and 97.5th confidence interval)
				svmat diff
				keep diff* 

				matrix conf_int = J(100, 2, 100)
				qui drop if _n == 1

				*sort each column (percentile) and saving 25th and 975th place in a matrix
				forvalues i = 1(1)100{
				sort diff`i'
				matrix conf_int[`i', 1] = diff`i'[0.025*`it']
				matrix conf_int[`i', 2] = diff`i'[0.975*`it']	
				}

				*******************Graphs for control, treatment, and difference using actual data (BASELINE)*************************************
				use "$base_out/temp/TempBoot_Pass", clear
				 
				bysort treatment  academyid: egen rank = rank(`baseline'Resid), unique
				bysort treatment  academyid: egen max_rank = max(rank)
				bysort treatment  academyid: gen PctileLag = rank/max_rank 

				 
				egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1

				lpoly Resid PctileLag if treatment==0 , gen(xcon pred_con) at (kernel_range) nograph
				lpoly Resid PctileLag if treatment==1 , gen(xtre pred_tre) at (kernel_range) nograph

				gen diff = pred_tre - pred_con

				*variables for confidence interval bands
				svmat conf_int



				save "$base_out/Lowess_Percentile`baseline'_`subject'_withinclass.dta", replace

				graph twoway (line pred_con xcon, lcolor(blue) lpattern("--.....") legend(lab(1 "Control"))) ///
				(line pred_tre xtre, lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
				(line diff xcon, lcolor(black) lpattern(solid) legend(lab(3 "Difference"))) ///
				(line conf_int1 xcon, lcolor(black) lpattern(shortdash) legend(lab(4 "95% Confidence Band"))) ///
				(line conf_int2 xcon, lcolor(black) lpattern(shortdash) legend(lab(5 "95% Confidence Band"))) ///
				,yline(0, lcolor(gs10)) xtitle(Percentile of Baseline Score) ytitle(`subject' Endline Score) legend(order(1 2 3 4)) ///
				title("Treatment effect by percentile at baseline for `subject'", size(vsmall))
				graph export "$results/Graphs/Lowess_Percentile`baseline'_`subject'_withinclass.pdf", as(pdf) replace
			}/*close if*/

		}

	}
}


