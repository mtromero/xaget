\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Design}{3}{0}{1}
\beamer@sectionintoc {2}{Treatment effect}{10}{0}{2}
\beamer@sectionintoc {3}{Heterogeneity by grade}{30}{0}{3}
\beamer@sectionintoc {4}{Heterogeneity by term}{37}{0}{4}
\beamer@sectionintoc {5}{Closing remarks}{44}{0}{5}
\contentsline {section}{}
