%\input{tcilatex}
\documentclass{article}
\usepackage[cm]{fullpage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{footnote}
\usepackage{threeparttable}
\usepackage{fullpage}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{tabularx}
\usepackage{tablefootnote}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}
\usepackage[table,xcdraw]{xcolor}

\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\singlespacing
\title{Cross-Age Tutoring: Experimental Evidence from Kenya\thanks{Corresponding author: Mauricio Romero. E-mail: \href{mailto:mtromero@ucsd.edu}{mtromero@ucsd.edu}}}
 
\newcommand*\samethanks[1][\value{footnote}]{\footnotemark[#1]}
% **** --------------------------------------------------------------------------------
\author{Mauricio Romero\thanks{University of California - San Diego.}   \and Lisa Chen\thanks{Bridge International Academies} \and Noriko Magari\samethanks[2]}%

\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.
\maketitle


\section{Introduction}


Over the past three decades access to primary school has increased dramatically in low- and middle-income counties \cite{Economic2015}. However, learning outcomes remain low despite increases in enrollment.\footnote{For example, despite enrollment rates of over 90\%, less than 50\% of children in Argentina, Colombia, Moroco, Uganda, Namibia, and Malawi attain ``minimum literacy standards'' \cite{WB2007}. Estimates from Mexico and Brazil estimate than more than 50\% of children lack minimally competencies in mathematics \cite{Filmer2006}. \citeA{Jones2014} shows that the majority of children in Grade 3 cannot recognise a word in the medium of instruction.} This is specially worrisome as evidence suggest that the quality of education, not the quanity,  is what matters for growth \cite{Hanushek2000,Hanushek2007}.

There is an increasing wealth of evidence showing that teaching at the right level can improve poor learning outcomes in low-income countries.\footnote{For example, see \citeA{Banerjee2007,Duflo2011,Duflo2015}} However, teachers often lack the time (or incentives) to give each child personalized instruction tailored to its needs and providing schools with extra teachers to do so is expensive. Cross-age tutoring, where older students tutor younger students, is an inexpensive alternative to provide personalized instruction to younger students by exchanging a trained instructor (the teacher) for an untrained one (the older student) at the cost of the older student's time. However, to the extent that tutoring can also provide benefits to tutors (e.g., mastering knowledge and increased social skills), cross-age tutoring could result in an overall welfare improvement. 


In this article, we present the results from a large RCT (over 194 schools, 10,000 tutees, and 13,000 tutors) in Kenya, in which schools are randomly selected to implement a tutoring program. In treated schools, once a day, every day, older students tutor younger students in math for 40 minutes (a total of 3.3 hours per week). 

Cross-age tutoring has been used since at least 95 CE \cite{Quintilianus1869} and is now widely used across the world. In the typical setup, the tutor is a few years older than the tutee and works with him on whatever problems he might have in a specific subject. Cross-age tutoring is often thought to have positive effects for both tutors and tutees in academic achievement and social-emotional outcomes \cite{Cohen1982}. However, the evidence on the subject is mixed, based on small-scale experiments or observational data and mostly based in developed countries. An early review of the literature showed that cross-age tutoring had a positive effect (both in terms of academic performance and attitudes) on both tutees and tutors \cite{Cohen1982}, while a more recent review looking only at randomized control trials came to the conclusion that cross-age tutoring tutoring has non-significative effects on math test score (albeit, they conclude that there is a small, but positive effect on reading) \cite{Shenderovich2015}. 

Our results speak directly to three strands in the literature. First, to the development literature that tries to undestand the underlying production function for cognitive achievement \cite{Todd2003} and studies the impact of different education policies and programs (see \cite{Glewwe2015} for a recent review of the literature). We present a novel approach to improve the amount of personalized instruction at a low cost. Second, to the literature on peer effects and their effect on learning \cite{Sacerdote2001,Zimmerman2003,Munley2010}. We are able to explore how the quality of different tutors (peers at the school level) affect the outcomes of the program. Finally, we communicate with the literature on peer-learning programs. Only two of the studies reviewed by \citeA{Shenderovich2015} had other elementary school students (as opposed to adults, community volunteers, or university students) as tutors and both tutoring programs focus on reading. None of the studies had been done in a low- or middle-income country. To the extent of our knowledge ours is the first large RCT implemented on cross-age tutoring in which tutors are still students in the same school as tutees. Furthermore, its the first study of this kind in a low-income country.  


\section{Experimental Design}
\subsection{Context}

Bridge International Academies (Bridge) is a network of low-cost private schools that takes advantages of economies of scale in school management, teacher training, and teaching guides to deliver high-quality education at a low cost.\footnote{For example, in each Bridge academy, unlike in ``mom-and-pop'' low-cost private schools, there is only one employee involved in management.  This is because the vast majority of non-instructional activities that the Bridge ``Academy Manager'' would normally have to deal with (billing, payments, expense management, payroll processing, and more) are automated and centralized. Similarly, Bridge hires experts to develop comprehensive teacher guidelines and training programs, that are then used in all of their schools. Schools charge on average a fee of \$USD 6  (enough to be financially sustainable) and cater to families living on \$USD 2 a day per person or less.} All schools have English as the medium of instruction and are located across East Africa, but mainly in Kenya. Bridge relies on technology heavily to keep a constant feedback loop with teachers.\footnote{Bridge follows the 8-4-4 curriculum framework mandated by the national government, but provides detailed teacher guides for each lesson that teachers across the network follow.  These guides are created in Bridge headquarter offices in both Boston, MA and Nairobi, KE, and are then streamed to teachers' personal tablets.  Teachers use tablets to upload students' information (e.g., test scores) to Bridge country headquarter offices as well.}


\subsection{Sampling}


Bridge has a network of over 400 schools across Kenya. However, only 194 schools were eligible to participate in the trial.\footnote{Schools without Standard 8 in 2015 and schools on which a small pilot of the program was tested were excluded.}. Randomization was stratified at the province level so that 15\% of schools within each province were assigned to the control group. In total, 166 schools are treated and 28 serve as control. All of our estimations take into account the stratification used during randomization, and the fact that treatment was at the school level (and not at the student level). 


\subsection{Intervention}


At the end of the day, every day, older students tutor younger students in math for 40 minutes (4:20 pm - 5:00 pm). Students in Standard 6 (C6) tutor students in Standard 1 (C1), while students in Standard 5 (C5) tutor students in Pre-Unit (PU). Tutoring replaced the end of day independent study period for C5/6 students.  For C1 students, tutoring replaced an independent study (2 times a week) and a writing period (3 times a week). For PU students, the timetable was compressed to make room for tutoring.\footnote{This was done by cutting: two toileting breaks by 5 minutes each (10 minutes total), the snack break by 5 minutes,the lunch break by 5 minutes and the outdoors activities by 10. The end of day class meeting was eliminated (10 minutes).} The content covered in treatment schools is the same as the content covered in control schools. However, since students in C1 often read during the independent study period and had a writing period eliminated we test for any spillovers in literacy (English). Students in C5/C6 in control schools were given a new reading pilot program where they read approximately 1 book per month.\footnote{Books were adapted public domain classics such as the Jungle Book by Rudyard Kipling.} The intervention lasted a full academic year (2015). 


\subsection{Data}

Students are tested seven times per academic year. Each academic year has three terms, and each term has a midterm and an endterm exam. Additionally, at the beginning of the academic year students did a diagnostic exam.\footnote{PU students were not tested at the beginning of 2015.} In total we have information for 8 exams per students.\footnote{We have an additional examfrom the previous school year.} Two exams  were taken by students before tutoring began, and 6 exams were taken after. Students in Standard 1 and Pre-Unit are tested in mathematics and languages (reading in English and in Swahili). Students in Standard 5 and Standard 6 are tested in mathematics, languages (reading in English and in Swahili), science, and social sciences. 

\subsection{Empirical strategy}


In order to estimate the effect of tutoring on test scores we use the following specification

\begin{equation}
Y_{isgd,t}=\alpha_0+\beta T_s+ \alpha_1 Y_{isgd,t=0}+\gamma_g+\gamma_t+ \gamma_d+\alpha_2 X_i+\alpha_3 X_s+\varepsilon_{isd,t}
\end{equation}

where $Y_{isgd,t}$ is the test score of student $i$ in grade $g$ at school $s$ located in province $d$ at time $t$ (and $Y_{isgd,t=0}$ is his test score before treatment). $\gamma_d$ is a set of province fixed effects, $\gamma_t$ are time fixed effects,  and $\gamma_g$ are grade fixed effects. $X_i$ is a set of student time-invariant characteristics (month of birth and gender), and $X_s$ are school characteristics at baseline (pupil teacher ratio, monthly school fees and teachers' wages). Standard errors are clustered at the school level.

\bibliographystyle{apacite}
\bibliography{CrossAgeBib}








\end{document}


