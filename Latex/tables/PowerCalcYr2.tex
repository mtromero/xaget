%\input{tcilatex}
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{amsmath, amsthm, amssymb}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{geometry}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{float}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage{harvard}
\usepackage{ctable}
\usepackage[all]{hypcap}
\usepackage [autostyle, english = american]{csquotes}
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
\usepackage{setspace}
\usepackage{apacite}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{thm}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\newtheorem{summary}{Summary}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newcommand\myVSpace[1][1pt]{\rule[\normalbaselineskip]{0pt}{#1}}
                            % The preamble begins here.
\title{Power calculations for year 2}  % Declares the document's title.
  % Declares the author's name.
%\date     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
\renewcommand{\arraystretch}{0.5}
\onehalfspacing
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\begin{document}             % End of preamble and beginning of text.
\maketitle                   % Produces the title.
 

\section{Regression analysis and power calculations}

In total we have 178 schools from RCT 1 (after taking into account that some are ``bad'' at uploading data) and 194 schools schools that are yet to be incorporated into the study (that is 405 schools, minus 194 from the first RCt, minus 17 that are ``bad'' at uploading data). From the 178 schools in RCT 1 we have 26 controls and 152 treatments.


We will do the following randomization. Some schools will be fully treated, some will only be treated in ``C1/C6+C2/C7+PU/C5'', some will be treated in ``C1/C6+C2/C7'', some will be treated in ``BA/C3+NU/C4+PU/C5'', and some will be treated in ``BA/C3+NU/C4.'' The following restrictions apply:

\begin{itemize}
\item No more than 60 schools will be untreated in each grade pair
\item The number of schools in ``BA/C3+NU/C4+PU/C5'' treatment is the same as in ``BA/C3+NU/C4'' treatment
\item The number of schools in ``C1/C6+C2/C7+PU/C5'' treatment is the same as in ``C1/C6+C2/C7'' treatment
\item The number of control schools for each grade/pair is the same
\item The number of treatment schools from RCT 1 that have $ C1/C6+C2/C7$ to be as big as possible, and the number of control schools in RCT 1 wihtout $ C1/C6+C2/C7$ to be as small as possible. 
\item We get a 100 observations per schools (suming over all terms)
\end{itemize}


For schools in last year's RCT:

\begin{enumerate}
\item  Let $p_0$ be the probability that treated schools are treated in all grades
\item  Let $p_1$ be the probability that treated schools are treated in grades C1/C6+C2/C7+PU/C5
\item  Let $p_1$ be the probability that treated schools are treated in grades C1/C6+C2/C7
\item  Let $p_1$ be the probability that treated schools are treated in grades  BA/C3+NU/C4+PU/C5
\item  Let $p_1$ be the probability that treated schools are treated in grades  BA/C3+NU/C4
\end{enumerate}

Notice that $p_0+4p_1=1$.

\begin{enumerate}
\item  Let $q_0$ be the probability that control schools are treated in all grades
\item  Let $q_1$ be the probability that control schools are treated in grades C1/C6+C2/C7+PU/C5
\item  Let $q_1$ be the probability that control schools are treated in grades C1/C6+C2/C7
\item  Let $q_1$ be the probability that control schools are treated in grades  BA/C3+NU/C4+PU/C5
\item  Let $q_1$ be the probability that control schools are treated in grades  BA/C3+NU/C4
\end{enumerate}

Notice that $q_0+4q_1=1$.

For schools NOT in previous RCT:

\begin{enumerate}
\item  Let $r_0$ be the probability that schools are treated in all grades
\item  Let $r_1$ be the probability that schools are treated in grades C1/C6+C2/C7+PU/C5
\item  Let $r_1$ be the probability that schools are treated in grades C1/C6+C2/C7
\item  Let $r_1$ be the probability that schools are treated in grades  BA/C3+NU/C4+PU/C5
\item  Let $r_1$ be the probability that schools are treated in grades  BA/C3+NU/C4
\end{enumerate}

Notice that $r_0+4r_1=1$.

Base on the original restrictions we need only 60 schools without treatment in each grade: 
\begin{eqnarray}
152(2p_1)+26(2q_1)+194(2r_1) &=& 60 \\
304 p_1+52 q_1+388 r_1 &=& 60 \\
76 p_1+13 q_1+97 r_1 &=& 15 \\
\end{eqnarray}


Time to do some power calculations...



We would have $152(p_0+2p_1)$ treatment schools and $26(2q_1)$ control schools (and a total of $152(p_0+2p_1)+26(2q_1)$). Assuming that the variance of covariates removes 30\% of the variation in test scores, and that the ICC is 0.1 after this (this are all optimistic calculations), and that we set $q_1$ maximum at $0.25$ (otherwise we need to set all controls as ``something''), we are still under-powered...

Therefore...change of plans!

We left all control schools as controls for the long term study! We treat them at ``BA/C3+NU/C4+PU/C5'' grades. And we forget about them for the RCT since their assignment is non-stochastic...

Now, what can we do about $p_0$ and $p_1$. Since we have 26 controls, what is the minimum number of treatments to be `fine'. Again, assuming the variance of covariates removes 30\% of the variation in test scores, and that the ICC is 0.1 after this (higher ICC make this exercise unfeasible).




Therefore we have 194 schools untouched, and 152 from the first RCT to play with.



\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrrr}
    \toprule
          &       & Last 2 years &       &       &  \\
    \midrule
          &       & COD   & Gains & Control & Row Total \\
    First 2 years & COD   & 40    & 20    & 10    & 70 \\
          & Combo & 10    & 30    & 30    & 70 \\
          & Control & 10    & 10    & 20    & 40 \\
          & Column Total & 60    & 60    & 60    & 180 \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%

If we think that the first 2 years and the second two years have no ``interaction'' effects, then we have that the treatment for each group would be

\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrr}
    \toprule
          &       & Last 2 years &       &         \\
    \midrule
          &       & COD   & Gains & Control  \\
    First 2 years & COD   & $\alpha_0+\alpha_1+\alpha_3$   & $\alpha_0+\alpha_3+\alpha_2$    & $\alpha_0+\alpha_3$     \\
          & Combo & $\alpha_0+\alpha_1+\alpha_4$    & $\alpha_0+\alpha_4+\alpha_2$    & $\alpha_0+\alpha_4$   \\
          & Control & $\alpha_0+\alpha_1$     &  $\alpha_0+\alpha_2$    & $\alpha_0$    \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%

And to estimate the treatment effect we would estimate the following equation

$$Y_{i}=\alpha_0+ \alpha_1 T_{COD,i}^2+\alpha_2 T_{Gains,i}^2+\alpha_3 T_{COD,i}^1+\alpha_4 T_{Combo,i}^1+\alpha_g+\varepsilon_{i,g}$$


In matrix notation we would have

$$Y_{i}= X\beta+\varepsilon_i$$

where $$X=\begin{pmatrix}
1 & T_{COD,1}^2 & T_{Gains,1}^2 & T_{COD,1}^1 & T_{Combo,i}^1 \\
\vdots &  \vdots & \vdots & \vdots & \vdots  \\
1 & T_{COD,n}^2 & T_{Gains,n}^2 & T_{COD,n}^1 & T_{Combo,n}^1 \\
\end{pmatrix}$$

$$\beta=\begin{pmatrix}
\alpha_0 \\
\alpha_1 \\
\alpha_2 \\
\alpha_3 \\
\alpha_4 \\
\end{pmatrix}$$

And therefore the variance-covariance matrix of $\beta$ would be

$$V(\hat{\beta})=(X' \Omega^{-1} X)^{-1} $$

where $\Omega=Diag(\Omega_g)$ and 
$$\Omega_g=\begin{pmatrix} \sigma_{\alpha}^2+\sigma_{\varepsilon}^2 & \sigma_{\alpha}^2 & \cdots & \sigma_{\alpha}^2 \\
\sigma_{\alpha}^2 & \sigma_{\alpha}^2+\sigma_{\varepsilon}^2  & \cdots &  \sigma_{\alpha}^2 \\
\vdots & \ddots &  \ddots & \vdots\\
\sigma_{\alpha}^2 & \cdots & \sigma_{\alpha}^2 & \sigma_{\alpha}^2+\sigma_{\varepsilon}^2 
\end{pmatrix}$$

\begin{eqnarray*}
V(\hat{\beta}) &=& (X' (I_g \otimes\Omega_g )^{-1} X)^{-1} \\
&=& \left( (X_1',X_2',...,X_J') \begin{pmatrix}
\Omega_g^{-1} & 0 &...& 0\\
0 & \Omega_g^{-1} &...& 0\\
\vdots & \ddots  & \ddots & \vdots\\
0 & 0 & ... & \Omega_g^{-1}\\
\end{pmatrix}  \begin{pmatrix}
X_1 \\
X_2 \\
\vdots\\
X_J \\
\end{pmatrix} \right)^{-1} \\
&=& \left( \sum_{j=1}^J X_j' \Omega_g^{-1}X_j  \right)^{-1} \\
\end{eqnarray*}

Now its important to note that there are 9 types of school's (the subindex used to indicate each type is shown below) and let $A^k=X_j^{k'} \Omega_g^{-1}X_j^k$ for schools of type $k$.


\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrr}
    \toprule
          &       & Last 2 years &       &         \\
    \midrule
          &       & COD   & Gains & Control  \\
    First 2 years & COD   & 1   & 2   & 3    \\
          & Combo & 4    & 5  & 6  \\
          & Control & 7     &  8   & 9   \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%

Then we have that:


$$A^1=X_j^{1'} \Omega_g^{-1}X_j^1=\begin{pmatrix} 
n_g \Sigma  & n_g\Sigma  & 0 & n_g\Sigma &0 \\
n_g \Sigma  & n_g\Sigma  & 0 & n_g\Sigma &0 \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma  & n_g\Sigma  & 0 & n_g\Sigma &0 \\
0 & 0 & 0 & 0 &0 \\
\end{pmatrix}$$

$$A^2=X_j^{2'} \Omega_g^{-1}X_j^2=\begin{pmatrix} 
n_g \Sigma    & 0 & n_g\Sigma & n_g\Sigma &0 \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma  & 0  & n_g\Sigma  & n_g\Sigma &0 \\
n_g \Sigma  & 0  & n_g\Sigma  & n_g\Sigma &0 \\
0 & 0 & 0 & 0 &0 \\
\end{pmatrix}$$

$$A^3=X_j^{3'} \Omega_g^{-1}X_j^3=\begin{pmatrix} 
n_g \Sigma    & 0 & 0 & n_g\Sigma &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma  & 0  & 0  & n_g\Sigma &0 \\
0 & 0 & 0 & 0 &0 \\
\end{pmatrix}$$

$$A^4=X_j^{4'} \Omega_g^{-1}X_j^4=\begin{pmatrix} 
n_g \Sigma  & n_g\Sigma  & 0  &0 & n_g\Sigma \\
n_g \Sigma  & n_g\Sigma  & 0  &0 & n_g\Sigma \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma  & n_g\Sigma  & 0  &0 & n_g\Sigma \\
\end{pmatrix}$$

$$A^5=X_j^{5'} \Omega_g^{-1}X_j^5=\begin{pmatrix} 
n_g \Sigma    & 0 & n_g\Sigma  &0 & n_g\Sigma \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma    & 0 & n_g\Sigma  &0 & n_g\Sigma \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma    & 0 & n_g\Sigma &0 & n_g\Sigma \\
\end{pmatrix}$$

$$A^6=X_j^{6'} \Omega_g^{-1}X_j^6=\begin{pmatrix} 
n_g \Sigma    & 0 & 0  &0 & n_g\Sigma \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma  & 0  & 0  &0 & n_g\Sigma \\
\end{pmatrix}$$

$$A^7=X_j^{7'} \Omega_g^{-1}X_j^7=\begin{pmatrix} 
n_g \Sigma  & n_g\Sigma  & 0 & 0 &0 \\
n_g \Sigma  & n_g\Sigma  & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
\end{pmatrix}$$

$$A^8=X_j^{8'} \Omega_g^{-1}X_j^8=\begin{pmatrix} 
n_g \Sigma    & 0 & n_g\Sigma & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
n_g \Sigma   & 0 & n_g\Sigma  & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
\end{pmatrix}$$


$$A^9=X_j^{9'} \Omega_g^{-1}X_j^9=\begin{pmatrix} 
n_g \Sigma  & 0  & 0  &0 & 0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0 & 0 & 0 &0 \\
0 & 0  & 0  &0 & 0 \\
\end{pmatrix}$$

where $\Sigma$ is the sum of any given row of the inverse of $\Omega_g$ and $n_g$ is the number of students per school we test. Therefore we have:

\begin{eqnarray*}
V(\hat{\beta}) &=& \left( \sum_{j=1}^J X_j' \Omega_g^{-1}X_j  \right)^{-1} \\
&=& \begin{pmatrix} 
J n_g \Sigma  & 60 n_g \Sigma  & 60 n_g \Sigma  &70 n_g \Sigma & 70 n_g \Sigma \\
60 n_g \Sigma  & 60 n_g \Sigma & 0 & 40 n_g \Sigma &10 n_g \Sigma \\
60 n_g \Sigma  & 0 & 60 n_g \Sigma & 20 n_g \Sigma & 30 n_g \Sigma \\
70 n_g \Sigma & 40 n_g \Sigma & 20 n_g \Sigma & 70 n_g \Sigma  &0 \\
70  & 10 n_g \Sigma  & 30 n_g \Sigma  &0 & 70 n_g \Sigma \\
\end{pmatrix} ^{-1} \\
&=& \frac{1}{10 n_g \Sigma} \begin{pmatrix} 
18   & 6  & 6  &7  & 7  \\
6   & 6  & 0 & 4  &1  \\
6   & 0 & 6  & 2  & 3  \\
7  & 4  & 2  & 7   &0 \\
7  & 1   & 3   &0 & 7  \\
\end{pmatrix} ^{-1} \\
&=& \frac{1}{18840 n_g \Sigma} \begin{pmatrix} 
604   & -280  & -252  &-372  & -456  \\
-280   & 763  & 357 & -258  & 18 \\
-252   & 357 & 651  & -138  & -78  \\
-372  & -258  & -138  & 828   &468 \\
-456  & 18   & -78   &468 & 756  \\
\end{pmatrix} \\
\end{eqnarray*}

Now, its easy to prove that 

\begin{eqnarray}
\Omega_g^{-1} &=& \begin{pmatrix} \sigma_{\alpha}^2+\sigma_{\varepsilon}^2 & \sigma_{\alpha}^2 & \cdots & \sigma_{\alpha}^2 \\
\sigma_{\alpha}^2 & \sigma_{\alpha}^2+\sigma_{\varepsilon}^2  & \cdots &  \sigma_{\alpha}^2 \\
\vdots & \ddots &  \ddots & \vdots\\
\sigma_{\alpha}^2 & \cdots & \sigma_{\alpha}^2 & \sigma_{\alpha}^2+\sigma_{\varepsilon}^2 
\end{pmatrix}^{-1}\\
&=& \left(\sigma_{\varepsilon}^2(I_{n_g}-\frac{J_{n_g}}{n_g})+ (n_g\sigma_{\alpha}^2+ \sigma_{\varepsilon}^2)\frac{J_{n_g}}{n_g} \right) ^{-1}\\
\end{eqnarray}

where $I_{n_g}$ is the identity matrix of size $n_g$ and $J_{n_g}$ is an $n_g \times n_g$ matrix with the unit in all its entries. Therefore we have:

\begin{eqnarray}
\Omega_g^{-1} &=& \sigma_{\varepsilon}^{-2}(I_{n_g}-\frac{J_{n_g}}{n_g})+ \frac{1}{n_g\sigma_{\alpha}^2+ \sigma_{\varepsilon}^2}\frac{J_{n_g}}{n_g} \\
\end{eqnarray}

and therefore

\begin{eqnarray}
\Sigma &=& \frac{1}{n_g \sigma_{\alpha}^2+\sigma_{\varepsilon}^2}
\end{eqnarray}
And therefore we have that:

\begin{eqnarray*}
V(\hat{\beta}) &=& \frac{n_g \sigma_{\alpha}^2+\sigma_{\varepsilon}^2}{18840n_g} \begin{pmatrix} 
604   & -280  & -252  &-372  & -456  \\
-280   & 763  & 357 & -258  & 18 \\
-252   & 357 & 651  & -138  & -78  \\
-372  & -258  & -138  & 828   &468 \\
-456  & 18   & -78   &468 & 756  \\
\end{pmatrix} \\
\end{eqnarray*}


Which is just fantastic!! Now without controls, we have that $\sigma_{\varepsilon}^2=1$ and the ICC is $\rho=\frac{\sigma_{\alpha}^2}{\sigma_{\alpha}^2+\sigma_{\varepsilon}^2}=0.3$. Therefore we can detect an effect size bigger than 

% Table generated by Excel2LaTeX from sheet 'Sheet1'
\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrr}
    \toprule
    Size  & Power & MDE COD & MDE Gains & MD Difference \\
    \midrule
    0.5   & 0.8   & 0.340081 & 0.314131 & 0.325738 \\
    0.1   & 0.8   & 0.43329 & 0.400228 & 0.415017 \\
    0.5   & 0.9   & 0.400251 & 0.36971 & 0.383371 \\
    0.1   & 0.9   & 0.493461 & 0.455807 & 0.47265 \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%


Now, the good news is that we have controls variables that explain up to 30\% of the variation and reduces the ICC, i.e. we effectively have $\sigma_{\varepsilon}^2=0.7$ and the ICC is $\rho=\frac{\sigma_{\alpha}^2}{\sigma_{\alpha}^2+\sigma_{\varepsilon}^2}=0.1$. Therefore we can detect an effect size bigger than 

% Table generated by Excel2LaTeX from sheet 'Sheet1'
\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrr}
    \toprule
    Size  & Power & MDE COD & MDE Gains & MD Difference \\
    \midrule
    0.5   & 0.8   & 0.1591129 & 0.1469716 & 0.1524025 \\
    0.1   & 0.8   & 0.2027226 & 0.1872537 & 0.194173 \\
    0.5   & 0.9   & 0.1872646 & 0.1729752 & 0.179367 \\
    0.1   & 0.9   & 0.2308743 & 0.2132573 & 0.2211375 \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%


\end{document}


