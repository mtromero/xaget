\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Design}{2}{0}{1}
\beamer@sectionintoc {2}{Treatment effect}{7}{0}{2}
\beamer@sectionintoc {3}{Heterogeneity by grade}{10}{0}{3}
\beamer@sectionintoc {4}{Heterogeneity by period}{14}{0}{4}
