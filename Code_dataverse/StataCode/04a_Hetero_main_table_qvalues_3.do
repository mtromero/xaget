
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors *tutor

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period
encode academyid, gen(academyid2)
drop if term4==1


label var Pupils_S "Enrollment"
label var Teachers_S "Teachers"
label var PTR_S "PTR"
label var TTR "TTR"
label var mean_T1DG16_tutor "Tutors' avergage test score in T1DG16"
label var mean_T3ET15_tutor "Tutors' avergage test score in T3ET15"
label var median_T1DG16_tutor "Tutors' median test score in T1DG16"
label var median_T3ET15_tutor "Tutors' median test score in T3ET15"
label var Male_tutor "Proportion of male tutors"
label var Age_tutor "Tutors' average age"
label var AgeFirstBridge "Age joined Bridge"
gen Age=2016-year(DateOfBirth)	
	
foreach var of varlist DateOfBirth Male AgeFirstBridge Age_tutor Male_tutor median_T3ET15_tutor  PTR_S TTR Pupils_S {
	sum `var' if treatment==0 & pupils==1
	replace `var'=`var'-r(mean)
}


foreach subject in math English {
	eststo clear
	clear mata
	preserve
	clear
	tempfile temp_pvalues
	save `temp_pvalues', emptyok
	restore 
	foreach var of varlist Age Male AgeFirstBridge Age_tutor Male_tutor median_T3ET15_tutor  PTR_S TTR Pupils_S {	
		capture drop cov
		gen cov=`var'
		eststo e_`var': reghdfe percentscore_Z c.treatment##c.cov $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
		estadd ysumm
		qui tab academyid if e(sample)==1 & treatment==0
		estadd scalar controls=r(r)
		estadd local ProvinceFE "Yes"
		estadd local Studentcontrols "Yes"
		estadd local Schoolscontrols "Yes"
		
		preserve
		parmest, norestore
		drop if strpos(parm,"treatment")==0
		drop if strpos(parm,"cov")==0
		capture append using `"`temp_pvalues'"'
		save `"`temp_pvalues'"', replace
		restore
		
		di "`cov'"
		
	}
	preserve
	use `"`temp_pvalues'"', clear
	qqvalue p in 1/3, method(yekutieli) qvalue(myqval1)
	qqvalue p in 4/6, method(yekutieli) qvalue(myqval2)
	qqvalue p in 7/9, method(yekutieli) qvalue(myqval3)
	egen myqval=rowtotal(myqval1 myqval2 myqval3), missing
	keep myqval
	mkmat myqval, matrix(qvalues)
	mkmat p, matrix(pvalues)
	mkmat estimate, matrix(B)
	restore

	
	local k=9
	foreach var of varlist Age Male AgeFirstBridge Age_tutor Male_tutor median_T3ET15_tutor  PTR_S TTR Pupils_S {	
		estimates restore e_`var'
		* Adding my p values to the estimation results.
		local Ncols=colsof(e(b))
		local col_interest=colnumb(e(b),"c.treatment#c.cov")
		matrix define bpmat = J(1,`Ncols',0)
		local names : colfullnames e(b)
		matrix colnames bpmat = `names'
		matrix bpmat[1,`col_interest']=qvalues[`k',1]
		estadd matrix qval = bpmat
		local k=`k'-1
		
		local tempm=string(p[`k',1], "%9.2gc")
		file open newfile using "$results/LatexCode/`subject'_het_`var'pvalue.tex", write replace
		file write newfile "`tempm'"
		file close newfile
			
		local tempm=string(B[`k',1], "%9.2gc")
		file open newfile using "$results/LatexCode/`subject'_het_`var'coef.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(myqval[`k',1], "%9.2gc")
		file open newfile using "$results/LatexCode/`subject'_het_`var'qvalue.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
	}
		
	label var cov "Covariate"
estout using "$results/LatexCode/het_all_pupils_`subject'_withoutT2ET_qvalues_3.tex", cells(b(star fmt(%9.3f)) se(par fmt(%9.3f)) qval(par(`"["' `"]"') fmt(%9.3f))) ///
 starlevels(* 0.10 ** 0.05 *** 0.01) stats(N r2, fmt(0 3) labels("Observations" "Adjusted \$R^2\$"))  label   ///
style(tex)	nonumber  replace keep(c.treatment#c.cov)   mlabels(none) collabels(none)

}
	
