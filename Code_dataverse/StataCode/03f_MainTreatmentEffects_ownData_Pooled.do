
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 formerprovince d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR
encode academyid, gen(academyid2)


drop if period<=1
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period


*pupils
	foreach cual in core pupils pupils_full {
		
		
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
			
			local tempm=string(B[1,1], "%9.2gc")
			file open newfile using "$results/LatexCode/All_coef_`subject'_pupils.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			test treatment
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$results/LatexCode/All_pvalue_`subject'_pupils.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			
		}
					
		esttab  using "$results/LatexCode/Pooled_all_pupils_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
		
	}
	
	**tutors		
	*tutors
	foreach cual in core tutors tutors_full {
				eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls_old $controls_prescores  if  tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
		}
					
		esttab  using "$results/LatexCode/Pooled_all_tutors_control_`cual'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
	}
