
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear
set graphics off
bys treatment period Subject gradename2: cumul percentscore, gen(cum_percentscore) eq
bys treatment period Subject gradename2: cumul percentscore_Z, gen(cum_percentscore_Z) eq
		
	
				
gen Age=2016-year(DateOfBirth)


drop if period!=2
	twoway (kdensity Age if GradeName2016=="Baby Class" & period==2, bwidth(1) lwidth(medthick)  legend(lab(1 "Baby Class"))) ///
	(kdensity Age if GradeName2016=="Nursery" & period==2, bwidth(1) lwidth(medthick)  legend(lab(2 "Nursery"))) ///
	(kdensity Age if GradeName2016=="PREUNIT" & period==2, bwidth(1) lwidth(medthick)  legend(lab(3 "PREUNIT"))) ///
	(kdensity Age if GradeName2016=="STD 1" & period==2, bwidth(1) lwidth(medthick)  legend(lab(4 "STD 1"))) ///
	(kdensity Age if GradeName2016=="STD 2" & period==2, bwidth(1) lwidth(medthick)  legend(lab(5 "STD 2"))) ///
	(kdensity Age if GradeName2016=="STD 3" & period==2, bwidth(1) lwidth(medthick)  legend(lab(6 "STD 3"))) ///
	(kdensity Age if GradeName2016=="STD 4" & period==2, bwidth(1) lwidth(medthick)  legend(lab(7 "STD 4"))) ///
	(kdensity Age if GradeName2016=="STD 5" & period==2, bwidth(1) lwidth(medthick)  legend(lab(8 "STD 5"))) ///
	(kdensity Age if GradeName2016=="STD 6" & period==2, bwidth(1) lwidth(medthick)  legend(lab(9 "STD 6"))) ///
	(kdensity Age if GradeName2016=="STD 7" & period==2, bwidth(1) lwidth(medthick)  legend(lab(10 "STD 7"))) ///
	 ,xtitle("Age") ytitle(Density) graphregion(color(white)) xscale(range(3 18))
	
	
	graph export "$results/Graphs/Dist_age.pdf", as(pdf) replace

