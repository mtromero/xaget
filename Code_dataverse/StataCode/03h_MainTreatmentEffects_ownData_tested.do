
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR

gen Tested=!missing(percentscore_Z)


drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period


encode academyid, gen(academyid2)

*pupils
	foreach cual in core pupils {
		eststo clear
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe Tested treatment  $controls_prescores  if  pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			sum Tested if pupils==1 & Subject=="`subject'" & treatment==0
			estadd scalar ymean2=r(mean)
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "No"
		}
					
		esttab  using "$results/LatexCode/Pooled_Tested_pupils_`cual'.tex", se fragment ar2 booktabs label b(a2) se(a2) nocon nonumber ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(${Outcomes_`cual'_labels}) ///
		replace keep(treatment) stats(ymean2 N N_clust , labels("Mean English" "N. of obs." "Number of schools")) ///
		nonotes 

	
	}

			
	