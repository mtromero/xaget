
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR
/*
gen post=(period>=2)

reghdfe percentscore_Z c.treatment#c.post  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab(pupilid period)
*/
gen Tested=!missing(percentscore_Z)


drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)


label var term1 T1MT16
label var term2 T1ET16
label var term3 T2MT16
label var term4 T2ET16
label var term5 T3MT16
label var term6 T3ET16
keep pupilid period Tested Subject
reshape wide Tested, i(pupilid period) j(Subject) string

label var Testedmath "Math"
label var TestedEngCompReading "English (Reading)"
label var TestedEngLanguage "English (Writing)"

eststo clear
estpost tabstat Testedmath TestedEngLanguage TestedEngCompReading , by(period)  statistics(mean sd) columns(statistics)
esttab using "$results/LatexCode/Evolv_Tested.tex", main(mean) aux(sd) nostar unstack  nonote nomtitle nonumber  replace label booktabs
