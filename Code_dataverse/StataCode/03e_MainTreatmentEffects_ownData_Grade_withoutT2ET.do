
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop period
encode academyid, gen(academyid2)
drop if term4==1
/*

gen term5=(term=="T3MT16")
gen term6=(term=="T3ET16")
*/

foreach var in dPU  dNU dBC dc1  dc2{
    gen percentscore_Z`var'=percentscore_Z if `var'==1
}	


	foreach subject in  math English{
		local label : var label percentscore_Z
		
		
		reghdfe percentscore_Z c.treatment#c.(dPU  dNU dBC dc1  dc2) $control  $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))
		test (_b[treatment#c.dPU]=_b[treatment#c.dNU]= _b[treatment#c.dBC]= _b[treatment#c.dc1]= _b[treatment#c.dc2])
		local p_all=string(r(p), "%9.2gc")
		
		
		preserve
		parmest, norestore
		drop if strpos(parm,"treatment")==0
		qqvalue p, method(yekutieli) qvalue(myqval)
		keep p myqval
		mkmat p myqval, matrix(A)
		forvalues i=1/5{
			local tempm=string(A[`i',1], "%9.2gc")
			file open newfile using "$results/LatexCode/`subject'_pvalue_grade`i'_withoutT2ET.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			local tempm=string(A[`i',2], "%9.2gc")
			file open newfile using "$results/LatexCode/`subject'_qvalue_grade`i'_withoutT2ET.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		restore
		
		
		coefplot, graphregion(color(white)) baselevels keep(c.treatment*) ci levels(95 90) ciopts(lwidth(medium thick)) rename(c.treatment#c.dBC="BC" c.treatment#c.dNU="NU" c.treatment#c.dPU="PU" c.treatment#c.dc1="Grd 1" c.treatment#c.dc2="Grd 2")   vertical yline(0) xtitle("Grade",size(medlarge)) ytitle("Math tutoring treatment effect",size(medlarge)) ///
		note("p-value(H{sub:0}:{&beta}{sub:PU}={&beta}{sub:NU}={&beta}{sub:BC}={&beta}{sub:Grd 1}={&beta}{sub:Grd 2})= `p_all'",size(medlarge))
		graph export "$results/Graphs/Grade_`subject'_Pupils_control_withoutT2ET.pdf", replace
		
		capture rwolf percentscore_ZdPU percentscore_ZdNU percentscore_ZdBC  percentscore_Zdc1 percentscore_Zdc2 if pupils==1 & Subject=="`subject'", indepvar(treatment) method(reghdfe)	vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)#c.(term*))	controls($control $controls_prescores ) seed(845894) reps(1000)
		foreach var in dPU  dNU dBC dc1  dc2{
			local tempm=string(e(rw_percentscore_Z`var'), "%9.2gc")
			file open newfile using "$results/LatexCode/`subject'_rwolf_grade`var'_withoutT2ET.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		
	}	

	

	
