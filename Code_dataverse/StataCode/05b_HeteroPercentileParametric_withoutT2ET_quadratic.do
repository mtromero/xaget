use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 GradeName2016 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors *tutor

drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
gen term6=(period=="T3ET16":periodl)
drop if term4==1
encode academyid, gen(academyid2)



reghdfe percentscore_Z treatment##(c.math_T3ET15_Z_mod##c.math_T3ET15_Z_mod##c.math_T3ET15_Z_mod##c.math_T3ET15_Z_mod) $controls $controls_prescores_nomath tested_math_T3ET15_Z  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))


margins, dydx(treatment) at(math_T3ET15_Z_mod=(-2(0.5)2))  subpop(if tested_math_T3ET15_Z==1)
marginsplot, yline(0) graphregion(color(white))  xtitle("Math test score in T3ET15") ytitle("Math tutoring treatment effect") title("") level(90)
		graph export "$graphs/HeteroBaseline_quadratic.pdf", replace
	
	

reghdfe percentscore_Z treatment##(c.math_T3ET15_Z##c.math_T3ET15_Z##c.math_T3ET15_Z##c.math_T3ET15_Z) $controls $controls_prescores_nomath   if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))

margins, dydx(treatment) at(math_T3ET15_Z=(-2(0.5)2)) 
marginsplot, yline(0) graphregion(color(white))  xtitle("Math test score in T3ET15") ytitle("Math tutoring treatment effect") title("") level(90)
		graph export "$graphs/HeteroBaseline_quadratic_v2.pdf", replace