
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR




drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
drop if period=="T2ET16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
drop period


encode academyid, gen(academyid2)

global controls_school c.wagecat_D2 c.wagecat_D3 c.DaysOpen  c.Pupils_S c.Teachers_S c.PTR_S c.Male_tutor c.Age_tutor c.TTR

global controls_student  c.DateOfBirth c.Male c.AgeFirstBridge



eststo clear

eststo: reghdfe percentscore_Z treatment  $controls_prescores if pupils==1 & Subject=="math", vce(cluster academyid2) ab(formerprovince2#stratascore)
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)

eststo: reghdfe percentscore_Z treatment  $controls_prescores $controls_student if pupils==1 & Subject=="math", vce(cluster academyid2) ab(formerprovince2#stratascore)
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)

eststo: reghdfe percentscore_Z treatment  $controls_prescores $controls_student  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)

eststo: reghdfe percentscore_Z treatment  $controls_school $controls_prescores $controls_student  if pupils==1 & Subject=="math", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)

eststo: reghdfe percentscore_Z treatment  $controls_school $controls_prescores $controls_student  if pupils==1 & Subject=="math", vce(cluster academyid2) ab(formerprovince2#stratascore)
estadd ysumm
qui tab academyid if e(sample)==1 & treatment==0
estadd scalar controls=r(r)

esttab  using "$results/LatexCode/Specification_Pupils_Robust.tex", se ar2 booktabs label b(a2) se(a2) nocon fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		nomtitle ///
		replace keep(treatment) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
		
		