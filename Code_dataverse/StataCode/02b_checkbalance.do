use "$base_out/SchoolLeveLData.dta", replace


label var wagecat_D1 "Monthly teacher wage of 11,250 KSH"
label var wagecat_D2 "Monthly teacher wage of 10,400 KSH"
label var wagecat_D3 "Monthly teacher wage of 7,970 KSH"


label var DaysOpen "Days since launch (as of Jan. 1, 2016)"

label var Teachers_S "Teachers"
label var Pupils_S "Enrollment (beginning of the school year)"
label var PTR_S "PTR"
* across all grades at the school at the beginning of the school year.
 


encode academyid, gen(academyid2)
encode formerprovince, gen(formerprovince2)

xi: my_ptest  DaysOpen wagecat_D1 wagecat_D2 wagecat_D3 Teachers_S Pupils_S PTR_S, by(treatment) clus_id(academyid2) strat_id(formerprovince2 stratascore)

esttab using "$results/LatexCode/BalanceTable_SchoolLevel.tex", label replace ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

esttab using "$results/LatexCode/BalanceTable_SchoolLevel.csv", label replace ///
nomtitle nonumbers nodep noobs nodep star(* 0.10 ** 0.05 *** 0.01)   collabels("English Tutoring" "Math Tutoring"  "Difference" "Difference (F.E)")  ///
cells("mu_1(fmt(3)) mu_2(fmt(3)) mu_3(fmt(3) star pvalue(d_p)) mu_4(fmt(3) star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par)") 

