clear all
set more off
* Central Path
global mipath "C:/Users/Mauricio/Dropbox/Learning Lab/XageT"


set matsize 5000
* Path Tree
global basein 	"$mipath/RawData"
global base_out   "$mipath/CreatedData"
global dir_do     "$mipath/Code/StataCode/Yr2"
global results    "$mipath/ResultsYr2"
global exceltables  "$mipath/ResultsYr2/ExcelTables"
global graphs     "$mipath/ResultsYr2/Graphs"
global latexcodes     "$mipath/ResultsYr2/LatexCodes"
  
  
global controls_T1DG16_BC
global controls_T1DG16_NU 
global controls_T1DG16_PU 
global controls_T1DG16_c1  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3
global controls_T1DG16_c2  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3
global controls_T1DG16_c3  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3
global controls_T1DG16_c4  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3
global controls_T1DG16_c5  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3
global controls_T1DG16_c6  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3
global controls_T1DG16_c7  lang_Z math_Z sci_Z ss_T1DG16_Z 2 lang_Z2 math_Z2 sci_Z2 ss_T1DG16_Z2 3 lang_Z3 math_Z3 sci_Z3 ss_T1DG16_Z3


global controls_T1DG16_BC_simple
global controls_T1DG16_NU_simple 
global controls_T1DG16_PU_simple 
global controls_T1DG16_c1_simple  lang_Z math_Z sci_Z ss_Z 
global controls_T1DG16_c2_simple  lang_Z math_Z sci_Z ss_Z 
global controls_T1DG16_c3_simple  lang_Z math_Z sci_Z ss_Z 
global controls_T1DG16_c4_simple  lang_Z math_Z sci_Z ss_Z 
global controls_T1DG16_c5_simple  lang_Z math_Z sci_Z ss_Z 
global controls_T1DG16_c6_simple  lang_Z math_Z sci_Z ss_Z 
global controls_T1DG16_c7_simple  lang_Z math_Z sci_Z ss_Z 

global controls_T3ET15_BC 
global controls_T3ET15_NU dev_Z env_T3ET15_Z EngLanguage_T3ET15_Z math_T3ET15_Z dev_Z2 env_T3ET15_Z2 EngLanguage_T3ET15_Z2 math_T3ET15_Z2 dev_Z3 env_T3ET15_Z3 EngLanguage_T3ET15_Z3 math_T3ET15_Z3
global controls_T3ET15_PU dev_Z env_T3ET15_Z EngLanguage_T3ET15_Z math_T3ET15_Z dev_Z2 env_T3ET15_Z2 EngLanguage_T3ET15_Z2 math_T3ET15_Z2 dev_Z3 env_T3ET15_Z3 EngLanguage_T3ET15_Z3 math_T3ET15_Z3
global controls_T3ET15_c1 env_T3ET15_Z  EngLanguage_T3ET15_Z math_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z SwaLanguage_T3ET15_Z EngCompReading_T3ET15_Z  env_T3ET15_Z2  EngLanguage_T3ET15_Z2 math_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 SwaLanguage_T3ET15_Z2 EngCompReading_T3ET15_Z2 2 env_T3ET15_Z3  EngLanguage_T3ET15_Z3 math_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 SwaLanguage_T3ET15_Z3 EngCompReading_T3ET15_Z3 3
global controls_T3ET15_c2 EngLanguage_T3ET15_Z math_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z SwaLanguage_T3ET15_Z  EngCompReading_T3ET15_Z sci_T3ET15_Z ss_T3ET15_Z EngLanguage_T3ET15_Z2 math_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 SwaLanguage_T3ET15_Z2 2 EngCompReading_T3ET15_Z2 sci_T3ET15_Z2 ss_T3ET15_Z2 EngLanguage_T3ET15_Z3 math_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 SwaLanguage_T3ET15_Z3 3 EngCompReading_T3ET15_Z3 sci_T3ET15_Z3 ss_T3ET15_Z3
global controls_T3ET15_c3 EngLanguage_T3ET15_Z math_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z SwaLanguage_T3ET15_Z  EngCompReading_T3ET15_Z sci_T3ET15_Z ss_T3ET15_Z EngLanguage_T3ET15_Z2 math_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 SwaLanguage_T3ET15_Z2 2 EngCompReading_T3ET15_Z2 sci_T3ET15_Z2 ss_T3ET15_Z2 EngLanguage_T3ET15_Z3 math_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 SwaLanguage_T3ET15_Z3 3 EngCompReading_T3ET15_Z3 sci_T3ET15_Z3 ss_T3ET15_Z3
global controls_T3ET15_c4 EngLanguage_T3ET15_Z math_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z SwaLanguage_T3ET15_Z  EngCompReading_T3ET15_Z sci_T3ET15_Z ss_T3ET15_Z EngLanguage_T3ET15_Z2 math_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 SwaLanguage_T3ET15_Z2 2 EngCompReading_T3ET15_Z2 sci_T3ET15_Z2 ss_T3ET15_Z2 EngLanguage_T3ET15_Z3 math_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 SwaLanguage_T3ET15_Z3 3 EngCompReading_T3ET15_Z3 sci_T3ET15_Z3 ss_T3ET15_Z3
global controls_T3ET15_c5 EngLanguage_T3ET15_Z math_T3ET15_Z  sci_T3ET15_Z ss_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z comp_T3ET15_Z SwaLanguage_T3ET15_Z EngLanguage_T3ET15_Z2 math_T3ET15_Z2 2 sci_T3ET15_Z2 ss_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 comp_T3ET15_Z2 SwaLanguage_T3ET15_Z2 EngLanguage_T3ET15_Z3 math_T3ET15_Z3 3 sci_T3ET15_Z3 ss_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 comp_T3ET15_Z3 SwaLanguage_T3ET15_Z3
global controls_T3ET15_c6 EngLanguage_T3ET15_Z math_T3ET15_Z  sci_T3ET15_Z ss_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z comp_T3ET15_Z SwaLanguage_T3ET15_Z EngLanguage_T3ET15_Z2 math_T3ET15_Z2 2 sci_T3ET15_Z2 ss_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 comp_T3ET15_Z2 SwaLanguage_T3ET15_Z2 EngLanguage_T3ET15_Z3 math_T3ET15_Z3 3 sci_T3ET15_Z3 ss_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 comp_T3ET15_Z3 SwaLanguage_T3ET15_Z3
global controls_T3ET15_c7 EngLanguage_T3ET15_Z math_T3ET15_Z  sci_T3ET15_Z ss_T3ET15_Z SwaCompEngCompReadinging_T3ET15_Z comp_T3ET15_Z SwaLanguage_T3ET15_Z EngLanguage_T3ET15_Z2 math_T3ET15_Z2 2 sci_T3ET15_Z2 ss_T3ET15_Z2 SwaCompEngCompReadinging_T3ET15_Z2 comp_T3ET15_Z2 SwaLanguage_T3ET15_Z2 EngLanguage_T3ET15_Z3 math_T3ET15_Z3 3 sci_T3ET15_Z3 ss_T3ET15_Z3 SwaCompEngCompReadinging_T3ET15_Z3 comp_T3ET15_Z3 SwaLanguage_T3ET15_Z3


global controls_T3ET15_BC_simple 
global controls_T3ET15_NU_simple dev_Z env_Z EngLanguage_Z math_Z 
global controls_T3ET15_PU_simple dev_Z env_Z EngLanguage_Z math_Z 
global controls_T3ET15_c1_simple env_Z  EngLanguage_Z math_Z SwaCompReading_Z SwaLanguage_Z EngCompReading_Z  
global controls_T3ET15_c2_simple EngLanguage_Z math_Z SwaCompReading_Z SwaLanguage_Z  EngCompReading_Z sci_Z ss_Z 
global controls_T3ET15_c3_simple EngLanguage_Z math_Z SwaCompReading_Z SwaLanguage_Z  EngCompReading_Z sci_Z ss_Z 
global controls_T3ET15_c4_simple EngLanguage_Z math_Z SwaCompReading_Z SwaLanguage_Z  EngCompReading_Z sci_Z ss_Z 
global controls_T3ET15_c5_simple EngLanguage_Z math_Z  sci_Z ss_Z SwaCompReading_Z EngCompReading_Z SwaLanguage_Z 
global controls_T3ET15_c6_simple EngLanguage_Z math_Z  sci_Z ss_Z SwaCompReading_Z EngCompReading_Z SwaLanguage_Z 
global controls_T3ET15_c7_simple EngLanguage_Z math_Z  sci_Z ss_Z SwaCompReading_Z EngCompReading_Z SwaLanguage_Z 



global Outcomes_BC_simple math EngLanguage EngCompReading   dev
global Outcomes_NU_simple math EngLanguage EngCompReading   dev
global Outcomes_PU_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading   ssci 
global Outcomes_c1_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading    sci ss
global Outcomes_c2_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading    sci ss
global Outcomes_pupils_full math EngLanguage EngCompReading SwaLanguage SwaCompReading 
global Outcomes_pupils_full_labels "Math" "English (Writing)" "English (Reading)" "Swahili (Writing)" "Swahili (Reading)" 


global Outcomes_c3_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading  sci ss
global Outcomes_c4_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading  sci ss   
global Outcomes_c5_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading  sci ss   
global Outcomes_c6_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading  sci ss   
global Outcomes_c7_simple math EngLanguage EngCompReading SwaLanguage SwaCompReading  sci ss   
global Outcomes_tutors_full math EngLanguage EngCompReading SwaLanguage SwaCompReading sci ss 
global Outcomes_tutors_full_labels "Math" "English (Writing)" "English (Reading)" "Swahili (Writing)" "Swahili (Reading)" "Science" "Soc. Sci."

global Outcomes_core math English Swahili 
global Outcomes_core_labels "Math" "English" "Swahili"


global Outcomes_pupils math English Swahili 
global Outcomes_pupils_labels "Math" "English" "Swahili"

global Outcomes_tutors math English Swahili 
global Outcomes_tutors_labels "Math" "English" "Swahili"


global controls c.wagecat_D2 c.wagecat_D3 c.DaysOpen c.DateOfBirth c.Male c.AgeFirstBridge c.Pupils_S c.Teachers_S c.PTR_S c.Male_tutor c.Age_tutor c.TTR
global controls2  wagecat_D2 wagecat_D3 DaysOpen DateOfBirth Male AgeFirstBridge Pupils_S Teachers_S PTR_S TTR  Male_tutor Age_tutor 

global controls_old c.wagecat_D2 c.wagecat_D3 c.DaysOpen c.DateOfBirth c.Male c.AgeFirstBridge c.Pupils_S c.Teachers_S c.PTR_S 
global controls_old2  wagecat_D2 wagecat_D3 DaysOpen DateOfBirth Male AgeFirstBridge Pupils_S Teachers_S PTR_S 

global controls_prescores tested_EngCompReading_T3ET15_Z c.(tested_EngCompReading_T3ET15_Z)#c.(EngCompReading_T3ET15_Z_mod EngCompReading_T3ET15_Z2_mod EngCompReading_T3ET15_Z3_mod) ///
tested_EngLanguage_T3ET15_Z c.(tested_EngLanguage_T3ET15_Z)#c.(EngLanguage_T3ET15_Z_mod EngLanguage_T3ET15_Z2_mod EngLanguage_T3ET15_Z3_mod) ///
tested_env_T3ET15_Z c.(tested_env_T3ET15_Z)#c.(env_T3ET15_Z_mod env_T3ET15_Z2_mod env_T3ET15_Z3_mod) ///
tested_SwaCompReading_T3ET15_Z c.(tested_SwaCompReading_T3ET15_Z)#c.(SwaCompReading_T3ET15_Z_mod SwaCompReading_T3ET15_Z2_mod SwaCompReading_T3ET15_Z3_mod) ///
tested_SwaLanguage_T3ET15_Z c.(tested_SwaLanguage_T3ET15_Z)#c.(SwaLanguage_T3ET15_Z_mod SwaLanguage_T3ET15_Z2_mod SwaLanguage_T3ET15_Z3_mod) ///
tested_crere_T3ET15_Z c.(tested_crere_T3ET15_Z)#c.(crere_T3ET15_Z_mod crere_T3ET15_Z2_mod crere_T3ET15_Z3_mod) ///
tested_dev_T3ET15_Z c.(tested_dev_T3ET15_Z)#c.(dev_T3ET15_Z_mod dev_T3ET15_Z2_mod dev_T3ET15_Z3_mod) ///
tested_math_T3ET15_Z c.(tested_math_T3ET15_Z)#c.(math_T3ET15_Z_mod math_T3ET15_Z2_mod math_T3ET15_Z3_mod) ///
tested_sci_T3ET15_Z c.(tested_sci_T3ET15_Z)#c.(sci_T3ET15_Z_mod sci_T3ET15_Z2_mod sci_T3ET15_Z3_mod) ///
tested_ss_T3ET15_Z c.(tested_ss_T3ET15_Z)#c.(ss_T3ET15_Z_mod ss_T3ET15_Z2_mod ss_T3ET15_Z3_mod) ///
tested_lang_T1DG16_Z c.(tested_lang_T1DG16_Z)#c.(lang_T1DG16_Z_mod lang_T1DG16_Z2_mod lang_T1DG16_Z3_mod) ///
tested_math_T1DG16_Z c.(tested_math_T1DG16_Z)#c.(math_T1DG16_Z_mod math_T1DG16_Z2_mod math_T1DG16_Z3_mod) ///
tested_sci_T1DG16_Z c.(tested_sci_T1DG16_Z)#c.(sci_T1DG16_Z_mod sci_T1DG16_Z2_mod sci_T1DG16_Z3_mod) ///
tested_ss_T1DG16_Z c.(tested_ss_T1DG16_Z)#c.(ss_T1DG16_Z_mod ss_T1DG16_Z2_mod ss_T1DG16_Z3_mod) ///
tested_swa_T1DG16_Z c.(tested_swa_T1DG16_Z)#c.(swa_T1DG16_Z_mod swa_T1DG16_Z2_mod swa_T1DG16_Z3_mod) 


global controls_prescores_nomath tested_EngCompReading_T3ET15_Z c.(tested_EngCompReading_T3ET15_Z)#c.(EngCompReading_T3ET15_Z_mod EngCompReading_T3ET15_Z2_mod EngCompReading_T3ET15_Z3_mod) ///
tested_EngLanguage_T3ET15_Z c.(tested_EngLanguage_T3ET15_Z)#c.(EngLanguage_T3ET15_Z_mod EngLanguage_T3ET15_Z2_mod EngLanguage_T3ET15_Z3_mod) ///
tested_env_T3ET15_Z c.(tested_env_T3ET15_Z)#c.(env_T3ET15_Z_mod env_T3ET15_Z2_mod env_T3ET15_Z3_mod) ///
tested_SwaCompReading_T3ET15_Z c.(tested_SwaCompReading_T3ET15_Z)#c.(SwaCompReading_T3ET15_Z_mod SwaCompReading_T3ET15_Z2_mod SwaCompReading_T3ET15_Z3_mod) ///
tested_SwaLanguage_T3ET15_Z c.(tested_SwaLanguage_T3ET15_Z)#c.(SwaLanguage_T3ET15_Z_mod SwaLanguage_T3ET15_Z2_mod SwaLanguage_T3ET15_Z3_mod) ///
tested_crere_T3ET15_Z c.(tested_crere_T3ET15_Z)#c.(crere_T3ET15_Z_mod crere_T3ET15_Z2_mod crere_T3ET15_Z3_mod) ///
tested_dev_T3ET15_Z c.(tested_dev_T3ET15_Z)#c.(dev_T3ET15_Z_mod dev_T3ET15_Z2_mod dev_T3ET15_Z3_mod) ///
tested_sci_T3ET15_Z c.(tested_sci_T3ET15_Z)#c.(sci_T3ET15_Z_mod sci_T3ET15_Z2_mod sci_T3ET15_Z3_mod) ///
tested_ss_T3ET15_Z c.(tested_ss_T3ET15_Z)#c.(ss_T3ET15_Z_mod ss_T3ET15_Z2_mod ss_T3ET15_Z3_mod) ///
tested_lang_T1DG16_Z c.(tested_lang_T1DG16_Z)#c.(lang_T1DG16_Z_mod lang_T1DG16_Z2_mod lang_T1DG16_Z3_mod) ///
tested_sci_T1DG16_Z c.(tested_sci_T1DG16_Z)#c.(sci_T1DG16_Z_mod sci_T1DG16_Z2_mod sci_T1DG16_Z3_mod) ///
tested_ss_T1DG16_Z c.(tested_ss_T1DG16_Z)#c.(ss_T1DG16_Z_mod ss_T1DG16_Z2_mod ss_T1DG16_Z3_mod) ///
tested_swa_T1DG16_Z c.(tested_swa_T1DG16_Z)#c.(swa_T1DG16_Z_mod swa_T1DG16_Z2_mod swa_T1DG16_Z3_mod) 


global controls_prescores_noEnglish tested_env_T3ET15_Z c.(tested_env_T3ET15_Z)#c.(env_T3ET15_Z_mod env_T3ET15_Z2_mod env_T3ET15_Z3_mod) ///
tested_SwaCompReading_T3ET15_Z c.(tested_SwaCompReading_T3ET15_Z)#c.(SwaCompReading_T3ET15_Z_mod SwaCompReading_T3ET15_Z2_mod SwaCompReading_T3ET15_Z3_mod) ///
tested_SwaLanguage_T3ET15_Z c.(tested_SwaLanguage_T3ET15_Z)#c.(SwaLanguage_T3ET15_Z_mod SwaLanguage_T3ET15_Z2_mod SwaLanguage_T3ET15_Z3_mod) ///
tested_crere_T3ET15_Z c.(tested_crere_T3ET15_Z)#c.(crere_T3ET15_Z_mod crere_T3ET15_Z2_mod crere_T3ET15_Z3_mod) ///
tested_dev_T3ET15_Z c.(tested_dev_T3ET15_Z)#c.(dev_T3ET15_Z_mod dev_T3ET15_Z2_mod dev_T3ET15_Z3_mod) ///
tested_math_T3ET15_Z c.(tested_math_T3ET15_Z)#c.(math_T3ET15_Z_mod math_T3ET15_Z2_mod math_T3ET15_Z3_mod) ///
tested_sci_T3ET15_Z c.(tested_sci_T3ET15_Z)#c.(sci_T3ET15_Z_mod sci_T3ET15_Z2_mod sci_T3ET15_Z3_mod) ///
tested_ss_T3ET15_Z c.(tested_ss_T3ET15_Z)#c.(ss_T3ET15_Z_mod ss_T3ET15_Z2_mod ss_T3ET15_Z3_mod) ///
tested_math_T1DG16_Z c.(tested_math_T1DG16_Z)#c.(math_T1DG16_Z_mod math_T1DG16_Z2_mod math_T1DG16_Z3_mod) ///
tested_sci_T1DG16_Z c.(tested_sci_T1DG16_Z)#c.(sci_T1DG16_Z_mod sci_T1DG16_Z2_mod sci_T1DG16_Z3_mod) ///
tested_ss_T1DG16_Z c.(tested_ss_T1DG16_Z)#c.(ss_T1DG16_Z_mod ss_T1DG16_Z2_mod ss_T1DG16_Z3_mod) ///
tested_swa_T1DG16_Z c.(tested_swa_T1DG16_Z)#c.(swa_T1DG16_Z_mod swa_T1DG16_Z2_mod swa_T1DG16_Z3_mod) 


capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
 reg `var'  TD1 TD2   `if', nocons vce(cluster `clus_id')
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p'  = nullmat(`d_p'),r(p)
 matrix A=e(b)
 lincom (TD1-TD2)
 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
 mat `se_3' = nullmat(`se_3'), r(se)
 
 reghdfe `var' TD1 TD2  `if', vce(cluster `clus_id') ab((`strat_id')##(`strat_id'))
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p2'  = nullmat(`d_p2'),r(p)
 lincom (TD1-TD2)
 mat `se_4' = nullmat(`se_4'), r(se)
 matrix A=e(b)
 matrix B=e(V)
 mat `mu_4' = nullmat(`mu_4'), A[1,2]-A[1,1]
 sum `var' if TD1==1 & e(sample)==1
 mat `mu_1' = nullmat(`mu_1'), round(r(mean),0.01)
 mat `se_1' = nullmat(`se_1'), r(sd)
 sum `var' if TD2==1 & e(sample)==1
 mat `mu_2' = nullmat(`mu_2'),round(r(mean),0.01)
 mat `se_2' = nullmat(`se_2'), r(sd)
}
foreach mat in mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2 {
 mat coln ``mat'' = `varlist'
}
 local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2 {
 eret mat `mat' = ``mat''
}
end


	do "$dir_do/01_ReadData"
	do "$dir_do/02a_checkbalance"
	do "$dir_do/02b_BalanceAndAttrition"
	do "$dir_do/02c_MissingBalance"
	
	
	
	do "$dir_do/03a_MainTable"
	do "$dir_do/03b_SpecificationTable"
	do "$dir_do/03c_MainTreatmentEffects_ownData_Evolution"
	do "$dir_do/03d_MainTreatmentEffects_ownData_Grade"
	do "$dir_do/03e_MainTreatmentEffects_ownData_Grade_withoutT2ET"
	do "$dir_do/03f_MainTreatmentEffects_ownData_Pooled"
	do "$dir_do/03g_MainTreatmentEffects_ownData_Pooled_Interpolate"
	do "$dir_do/03h_MainTreatmentEffects_ownData_tested"
	do "$dir_do/03i_MainTreatmentEffects_ownData_tested_withoutT2ET"
	do "$dir_do/03j_MainTreatmentEffects_ownData_tested_simpleevolution"
	do "$dir_do/03k_MainTreatmentEffects_tested_Evolution"
	
	
	do "$dir_do/04a_Hetero_main_table_qvalues_3"
	do "$dir_do/04b_Hetero_main_table_qvalues_3_allPeriods"
	do "$dir_do/04c_TripleInteract"
	
	do "$dir_do/05a_HeteroPercentileParametric_withoutT2ET"
	do "$dir_do/05b_HeteroPercentileParametric_withoutT2ET_quadratic"
	do "$dir_do/05c_HeteroPercentileParametric_withoutT2ET_quintiles"
	do "$dir_do/05d_HeteroPercentileParametric_withoutT2ET_Terciles"
	do "$dir_do/05e_HeteroPercentileParametricTutors_withoutT2ET"

	do "$dir_do/06a_ConteoDeIds"
	do "$dir_do/06b_RawDataDescr"
