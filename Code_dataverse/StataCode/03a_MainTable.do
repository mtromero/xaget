
use "$base_out/2016t1_FullyLinked_mainTE_own.dta", clear

keep $controls2 gradename gradename2 d* academyid formerprovince2 stratascore percentscore_Z *T3ET15* *T1DG16*  treatment pupilid period Subject pupils tutors TTR
/*
gen post=(period>=2)

reghdfe percentscore_Z c.treatment#c.post  if  pupils==1 & Subject=="math", vce(cluster academyid22) ab(pupilid period)
*/



drop if period<=1
drop if period=="T3ET15":periodl
drop if period=="T1DG16":periodl
drop if period=="T2ET16":periodl
compress
gen term1=(period=="T1MT16":periodl)
gen term2=(period=="T1ET16":periodl)
gen term3=(period=="T2MT16":periodl)
gen term4=(period=="T2ET16":periodl)
gen term5=(period=="T3MT16":periodl)
drop period


encode academyid, gen(academyid2)


	eststo clear
	foreach cual in  core {
		
		
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls $controls_prescores  if pupils==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
			matrix B=e(b)
			
			local tempm=string(B[1,1], "%9.2gc")
			file open newfile using "$results/LatexCode/Main_coef_`subject'_pupils.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			test treatment
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$results/LatexCode/Main_pvalue_`subject'_pupils.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			lincom treatment
			
			local tempm=string(r(ub), "%9.2gc")
			file open newfile using "$results/LatexCode/Main_upperbound_`subject'_pupils.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			local tempm=string(abs(r(lb)), "%9.2gc")
			file open newfile using "$results/LatexCode/Main_lowerbound_`subject'_pupils.tex", write replace
			file write newfile "`tempm'"
			file close newfile


		}
					
	}
	
	**tutors		
	*tutors
	*core tutors
	foreach cual in  core{
		

		
		foreach subject in ${Outcomes_`cual'} {
			eststo: reghdfe percentscore_Z treatment  $controls_old $controls_prescores  if  tutors==1 & Subject=="`subject'", vce(cluster academyid2) ab((gradename2#formerprovince2#stratascore)##c.(term*))
			estadd ysumm
			qui tab academyid if e(sample)==1 & treatment==0
			estadd scalar controls=r(r)
			estadd local ProvinceFE "Yes"
			estadd local Studentcontrols "Yes"
			estadd local Schoolscontrols "Yes"
			
			local tempm=string(B[1,1], "%9.2gc")
			file open newfile using "$results/LatexCode/Main_coef_`subject'_tutors.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			test treatment
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$results/LatexCode/Main_pvalue_`subject'_tutors.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			lincom treatment
			
			local tempm=string(r(ub), "%9.2gc")
			file open newfile using "$results/LatexCode/Main_upperbound_`subject'_tutors.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			local tempm=string(abs(r(lb)), "%9.2gc")
			file open newfile using "$results/LatexCode/Main_lowerbound_`subject'_tutors.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
					
	}
	
	esttab  using "$results/LatexCode/Pooled_minusT2ET16_Ambos_control.tex", se ar2 booktabs label b(a2) se(a2) nocon fragment ///
		star(* 0.10 ** 0.05 *** 0.01) ///
		mtitles(Math English Swahili Math English Swahili) ///
		mgroups("Tutees" "Tutors", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
		replace keep(treatment) stats(N N_clust , labels("N. of obs." "Number of schools")) ///
		nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
